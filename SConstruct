import platform

# build bpscript compiler
# scons --compiler
AddOption('--compiler',
          dest='build_compiler',
          action='store_true',
          default=False,
          help='Build BPScript compiler.')

# build bpscript assembler
# scons --asm
AddOption('--asm',
          dest='build_asm',
          action='store_true',
          default=False,
          help='Build BPScript assembler.')

# build bpscript net compiler
# scons --net
AddOption('--net',
          dest='build_net',
          action='store_true',
          default=False,
          help='Build BPScript net compiler.')

# build samples
# scons --samples
AddOption('--samples',
          dest='build_samples',
          action='store_true',
          default=False,
          help='Builds samples.')

# build all tests
# scons --tests
AddOption('--tests',
          dest='build_all_tests',
          action='store_true',
          default=False,
          help='Builds all tests.')

# build compiler tests
# scons --compiler-tests
AddOption('--compiler-tests',
          dest='build_compiler_tests',
          action='store_true',
          default=False,
          help='Builds compiler tests.')

# build and run selected tests
# scons --run
AddOption('--run',
          dest='run_tests',
          action='store_true',
          default=False,
          help='Builds and then runs selected tests.')


opt_build_asm = GetOption('build_asm')
opt_build_net = GetOption('build_net')
opt_build_samples = GetOption('build_samples')

opt_build_compiler = GetOption('build_compiler')
opt_build_compiler_tests = GetOption('build_compiler_tests')

opt_build_all_tests = GetOption('build_all_tests')
opt_run_tests = GetOption('run_tests')

if opt_build_all_tests:
    opt_build_compiler_tests = True
    # opt_build_asm_tests = True




######################################################
#
#
#
######################################################

def detect_msvc(env):
    if 'msvc_version' in env:
        env['MSVC_VERSION'] = env['msvc_version']

    msvc_version = env.get('MSVC_VERSION', '')
    if msvc_version:
        env.msvc = True

    # FIXME: using mingw instead of msvc 12.0
    # if msvc_version == '12.0':
    #     env.msvc = False
    #     env.PrependENVPath('PATH', 'C:\\Applications\\mingw32\\bin')
    #     env.Tool('mingw', toolpath='C:\\Applications\\mingw32\\bin')
    #     env.Tool('gnulink', toolpath='C:\\Applications\\mingw32\\bin')
    #     env.Replace(CC = 'C:\\Applications\\mingw32\\bin\\gcc.exe')
    #     env.Replace(CCFLAGS = '') # to remove /nologo flag meant for msvc


######################################################
#
#
#
######################################################

env = Environment()

# i.e. cc_cxx_values is ['gcc', 'g++', '7.3.0', '7.3.0']
cc_cxx_values = env.Dictionary('CC', 'CXX', 'CCVERSION', 'CXXVERSION')
cc_name = cc_cxx_values[0] # 'CC'
cxx_name = cc_cxx_values[1] # 'CXX'
cc_version = cc_cxx_values[2] # 'CCVERSION'
cxx_version = cc_cxx_values[3] # 'CXXVERSION'

env.msvc = False
detect_msvc(env)


## DEBUG build
if env.msvc:
    # Compiler Options: https://msdn.microsoft.com/en-us/library/fwkeyyhe.aspx
    env.Append(CXXFLAGS = ['/EHsc', '/Z7', '/Od'])
    env.Append(LINKFLAGS=['/DEBUG'])
else:
    env.Append(CXXFLAGS = ['-std=c++11', '-g', '-O0', '-Wall', '-Wextra'])
    env.Append(CFLAGS = ['-std=c99', '-g', '-O0', '-Wall', '-Wextra'])
    if platform.system() == "Linux":
        # they say this is needed by cpp-peglib on Ubuntu
        # https://github.com/yhirose/cpp-peglib/issues/23#issuecomment-261126127
        env.Append(LINKFLAGS=['-pthread'])

## RELEASE build
    # -Wall -Wextra -O3 -march=native ...
    # -Wall -Wextra -O3 -march=armv7 ...
    # -Wall -Wextra -Os -march=armv7 ...

if cxx_name == 'g++':
    # turn on 
    env.Append(CXXFLAGS = ['-Wmissing-field-initializers', '-g'])


SConscript( 'assembler/SConscript', 'env opt_build_asm opt_build_net opt_build_samples' )
SConscript( 'compiler/SConscript', 'env opt_build_compiler opt_build_compiler_tests opt_run_tests' )

