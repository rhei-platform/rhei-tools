
## Installs

- Python 3
- pip install
    - scons
    - cefpython3
    - sphinx
    - sphinx-rtd-theme
    - pytest
    - pyinstaller
- gcc or clang


## BPScript assembler

The assembler is coded in Python and does not need a build.

What is included here is `scons` script that processes few sample networks and creates C source for them. Generated code is then compiled into executables that run the network and print out the result.

To build sample apps, from the root folder (where the `SConstruct` file is), run:

    scons --asm

This will generate C source files in `build/output` folder as well as three binaries: `main`, `main_native` and `main_network`.

### `main` sample

This sample app uses transpiled C header files for two components to get component instances. It then builds a network using these components, sets up input values, runs the network on VM and prints out output result.

Run with:

    ./build/output/main

Sample output:

    BPScript VM sample
    OUT = - ( A + B + C )
    valueA = 55
    valueB = 44
    valueC = 33
    result = -132


### `main_native` sample

This sample app uses transpiled C header files for two components to get component instances. It then builds a network using these components, connects native functions for handling network I/Os, sets up input value and runs the network on VM. Native function set to handle the network output will print out processing result as soon as it becomes available.

Run with:

    ./build/output/main_native

Sample output:

    BPScript VM transpiled network sample
    OUT = - ( A + B + C )
    valueA = 55
    valueB = 44
    valueC = 33
    result = -132


### `main_network` sample

This sample app uses transpiled sample network C code. It sets up connections to the network I/Os, sets up input values, runs VM on the network and prints out output result.


Run with:

    ./build/output/main_network

Sample output:

    BPScript VM sample with native functions
    OUT = - A
    fnInputValue = 42
    fnOutputValue = -42



To run natwork specification compiler on a sample net definiton:

    python \
        assembler/src/bpnet.py \
            assembler/samples/sample.bpnet \
            --component-path \
            assembler/samples \
            --output-folder \
            build/output

This will create files:

- `summator.h`
- `negator.h`
- `network.c`

in `build/output` folder.

To run assembler on a sample component specification code:

    python \
        assembler/src/bpasm.py \
        assembler/tests/sources/components/offsets.bpasm \
        build/output

This will save generated C code and binary file to `build/output` folder as `build/output/offsets.h` and `build/output/offsets.bin` respectively.
