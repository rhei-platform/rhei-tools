#!/bin/bash

gcc -c main.c -o main.o -I../../vm/src -I../../vm/src/core -I../../vm/src/debug -I../../vm/src/interpreter -I../../vm/src/lib -I../../vm/src/meta -I../../vm/src/util -I.
gcc -o main main.o -lbpsvm -L..
./main 

# BPScript VM sample
# OUT = - ( A + B + C )
# valueA = 55
# valueB = 44
# valueC = 33
# result = -132


gcc -c main_native.c -o main_native.o -I../../vm/src -I../../vm/src/core -I../../vm/src/debug -I../../vm/src/interpreter -I../../vm/src/lib -I../../vm/src/meta -I../../vm/src/util -I.
gcc -o main_native main_native.o -lbpsvm -L..
./main_native 

# BPScript VM sample with native functions
# OUT = - A
# fnInputValue = 42
# fnOutputValue = -42

gcc -c main_network.c -o main_network.o -I../../vm/src -I../../vm/src/core -I../../vm/src/debug -I../../vm/src/interpreter -I../../vm/src/lib -I../../vm/src/meta -I../../vm/src/util -I.
gcc -c sample_network.c -o sample_network.o -I../../vm/src -I../../vm/src/core -I../../vm/src/debug -I../../vm/src/interpreter -I../../vm/src/lib -I../../vm/src/meta -I../../vm/src/util -I.
gcc -o main_network main_network.o sample_network.o -lbpsvm -L..
./main_network

# BPScript VM sample
# OUT = - ( A + B + C )
# valueA = 55
# valueB = 44
# valueC = 33
# result = -132
