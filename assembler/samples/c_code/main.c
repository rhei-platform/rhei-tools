#include "vm/util/network_builder.h"

#include "summator.h"
#include "negator.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/debug/vm_debug.h"

#include <stdio.h>
#include <assert.h>
#define REQUIRE assert

static void dummyFn( FBNativeContextHandle handle ) {
    (void)handle;
}


int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    printf("BPScript VM sample\n");
    printf("OUT = - ( A + B + C )\n");

    FBComponent* sumamtor = fb_bpasm_get_summator();
    FBComponent* negator = fb_bpasm_get_negator();
/*
OUT = - ( A + B + C )

    +sum1+
A--|0   |   +sum2+
B--|1  0|---|0   |   +neg+
    +----+   |   0|---|0 0|---OUT
C-----------|1   |   +---+
            +----+

*/

    FBNetworkBuilderData nbdata;

    fbNetworkBuilder_init( &nbdata );

    index_t ctxSum1idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
    index_t ctxSum2idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
    index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );

    fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum1idx, 0, ctxSum2idx, 0, 1 );
    fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum2idx, 0, ctxNegidx, 0, 1 );

    fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 0 );
    fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 1 );
    fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum2idx, 1 );
    fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );


    // the easiest way to connect network I/Os is to create native ports with dummy native function
    // network builder will then create connections to network ports and do all the dirty work for us
    // NOTE: nativeFn can also be NULL, no need to declare dummyFn
    FBPacketHandlerFn nativeFn = dummyFn;

    index_t nativeInAIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, NULL, 0 );
    fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInAIdx, ctxSum1idx, 0, 1 );
    index_t nativeInBIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, NULL, 0 );
    fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInBIdx, ctxSum1idx, 1, 1 );
    index_t nativeInCIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, NULL, 0 );
    fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInCIdx, ctxSum2idx, 1, 1 );
    index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, nativeFn, NULL, 0 );
    fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxNegidx, 0, nativeOutIdx, 1 );



    FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );

    fbNetworkBuilder_free( &nbdata );

    // at this point we can either get native I/Os or regular network I/Os
    // we just need access to I/O connections so we can write/read to them, and
    // either of those (native or regular I/Os) contains the same connection reference we need
    FBNetworkNativeIO *nativeIOInputA = fbNetwork_getNativeInput( networkContext, nativeInAIdx );
    FBNetworkNativeIO *nativeIOInputB = fbNetwork_getNativeInput( networkContext, nativeInBIdx );
    FBNetworkNativeIO *nativeIOInputC = fbNetwork_getNativeInput( networkContext, nativeInCIdx );
    FBNetworkNativeIO *nativeIOOutput = fbNetwork_getNativeOutput( networkContext, nativeOutIdx );
    // using regular network I/Os
    // FBInputOutput *IOInputA = fbNetwork_getInput( networkContext, nativeInAIdx );
    // FBInputOutput *IOInputB = fbNetwork_getInput( networkContext, nativeInBIdx );
    // FBInputOutput *IOInputC = fbNetwork_getInput( networkContext, nativeInCIdx );
    // FBInputOutput *IOOutput = fbNetwork_getOutput( networkContext, nativeOutIdx );

    // DEBUG INFO
    // fbDebug_printNetworkContext( networkContext );


    FBInformationPacketTuple packetA = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    FBInformationPacketTuple packetB = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    FBInformationPacketTuple packetC = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    FBInformationPacketHandle outputPacket;

    int32_t valueA = 55;
    int32_t valueB = 44;
    int32_t valueC = 33;

    // set data
    fbData_set_int32( packetA.buffer, valueA );
    fbData_set_int32( packetB.buffer, valueB );
    fbData_set_int32( packetC.buffer, valueC );


    //---------------------------------------------------------------
    // Send information packets into the sample network
    //---------------------------------------------------------------
    // we use native inputs to access underlying connection to the network inputs
    // we could've instead used network inputs, the connection reference is the same
    fbConnection_write( nativeIOInputA->connection, packetA.handle );
    fbConnection_write( nativeIOInputB->connection, packetB.handle );
    fbConnection_write( nativeIOInputC->connection, packetC.handle );
    // using regular network I/Os
    // fbConnection_write( IOInputA->connection, packetA.handle );
    // fbConnection_write( IOInputB->connection, packetB.handle );
    // fbConnection_write( IOInputC->connection, packetC.handle );


    //---------------------------------------------------------------
    // Run the network
    //---------------------------------------------------------------
    while( fbScheduler_mainLoop( networkContext ) ) {
        // the network will run until component terminates
        // by reaching OP_TERMINATE opcode at which point
        // scheduler will terminate both input connections
        // and close all ports on them and then close
        // component's output port.
        // output connection with result in it will still
        // be alive and our portResult will still be open

        // DEBUG INFO
        // fbDebug_printNetworkContextState( networkContext );
    }

    // we use native outputs to access underlying connection to the network output
    // we could've instead used network output, the connection reference is the same
    outputPacket = fbConnection_read( nativeIOOutput->connection );
    // using regular network I/Os
    // outputPacket = fbConnection_read( IOOutput->connection );
    REQUIRE( outputPacket != FB_INVALID_PACKET_HANDLE );

    byte_t *outputData = fbInformationPacket_getDataBuffer( outputPacket );
    int32_t result = fbData_get_int32( outputData );

    REQUIRE( result == - ( valueA + valueB + valueC ) );


    fbInformationPacket_drop( outputPacket );
    fbNetwork_destroy( networkContext );

    printf("valueA = %d\n", valueA);
    printf("valueB = %d\n", valueB);
    printf("valueC = %d\n", valueC);
    printf("result = %d\n", result);

    return 0;
}
