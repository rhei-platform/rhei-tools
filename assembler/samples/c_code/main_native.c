#include "vm/util/network_builder.h"

#include "negator.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/debug/vm_debug.h"

#include "vm/native.h"

#include <stdio.h>


static uint32_t fnInputValue;
static uint32_t fnOutputValue;

static void inputFn( FBNativeContextHandle handle ) {

    enum inputState {
        IFN_STATE_WRITE,
        IFN_STATE_TERMINATE,
        IFN_STATE_DONE
    };

    static FBInformationPacketHandle packet = FB_INVALID_PACKET_HANDLE;
    static enum inputState state = IFN_STATE_WRITE;

    switch( state )
    {
        case IFN_STATE_WRITE: {

            FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( sizeof( int32_t ) );
            packet = tuple.handle;
            fbData_set_int32( tuple.buffer, fnInputValue );

            bool writeSuccess = fbNativeConnection_write( handle, packet );
            if( writeSuccess ) {
                packet = FB_INVALID_PACKET_HANDLE;
                state = IFN_STATE_TERMINATE;
            }
        }
            break;

        case IFN_STATE_TERMINATE: {

            fbNativeConnection_close( handle );
            state = IFN_STATE_DONE;
        }
            break;

        default:
            break;
    }
}

static void outputFn( FBNativeContextHandle handle ) {

    FBInformationPacketHandle ip = fbNativeConnection_read( handle );

    if( ip != FB_INVALID_PACKET_HANDLE ) {

        byte_t* buffer = fbInformationPacket_getDataBuffer( ip );

        fnOutputValue = fbData_get_int32( buffer );

        fbInformationPacket_drop( ip );
    }
}


int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    printf("BPScript VM sample with native functions\n");
    printf("OUT = - A\n");

    FBComponent* negator = fb_bpasm_get_negator();

    FBNetworkBuilderData nbdata;

    fbNetworkBuilder_init( &nbdata );

    index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );

    fbNetworkBuilder_setNetworkInput( &nbdata, ctxNegidx, 0 );
    fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );

    index_t nativeInIdx = fbNetworkBuilder_setNativeInput( &nbdata, inputFn, NULL, 0 );
    fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInIdx, ctxNegidx, 0, 1 );
    index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, outputFn, NULL, 0 );
    fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxNegidx, 0, nativeOutIdx, 1 );


    FBNetworkContext *networkContext;
    networkContext = fbNetworkBuilder_build( &nbdata );

    fbNetworkBuilder_free( &nbdata );


    FBContext *ctxNeg;

    ctxNeg = fbNetwork_getContext( networkContext, ctxNegidx );


    fbContext_getInputPort( ctxNeg, 0 );
    fbContext_getOutputPort( ctxNeg, 0 );


    fbNetwork_getNativeInput( networkContext, nativeInIdx );
    fbNetwork_getNativeOutput( networkContext, nativeOutIdx );


    fnInputValue = 42;
    //---------------------------------------------------------------
    // Run the network
    //---------------------------------------------------------------
    while( fbScheduler_mainLoop( networkContext ) ) {
        // the network will run until component terminates
        // by reaching OP_TERMINATE opcode at which point
        // scheduler will terminate both input connections
        // and close all ports on them and then close
        // component's output port.
        // output connection with result in it will still
        // be alive and our portResult will still be open

        // DEBUG INFO
        // fbDebug_printNetworkContextState( networkContext );
    }

    fbNetwork_destroy( networkContext );

    printf("fnInputValue = %d\n", fnInputValue);
    printf("fnOutputValue = %d\n", fnOutputValue);

    return 0;
}
