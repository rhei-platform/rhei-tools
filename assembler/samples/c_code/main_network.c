#include "vm/util/network_builder.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/debug/vm_debug.h"

#include <stdio.h>
#include <assert.h>
#define REQUIRE assert


extern FBNetworkContext *create_network();

int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    printf("BPScript VM transpiled network sample\n");
    printf("OUT = - ( A + B + C )\n");

/*
OUT = - ( A + B + C )

    +sum1+
A--|0   |   +sum2+
B--|1  0|---|0   |   +neg+
    +----+   |   0|---|0 0|---OUT
C-----------|1   |   +---+
            +----+

*/



    FBNetworkContext *networkContext = create_network();


    FBInputOutput *IOInputA = fbNetwork_getInput( networkContext, 0 );
    FBInputOutput *IOInputB = fbNetwork_getInput( networkContext, 1 );
    FBInputOutput *IOInputC = fbNetwork_getInput( networkContext, 2 );
    FBInputOutput *IOOutput = fbNetwork_getOutput( networkContext, 0 );

    FBConnection connectionA;
    FBInputOutput portA;
    fbConnection_init( &connectionA, &portA, IOInputA );
    FBConnection connectionB;
    FBInputOutput portB;
    fbConnection_init( &connectionB, &portB, IOInputB );
    FBConnection connectionC;
    FBInputOutput portC;
    fbConnection_init( &connectionC, &portC, IOInputC );
    FBConnection connectionOut;
    FBInputOutput portOut;
    fbConnection_init( &connectionOut, IOOutput, &portOut );

    // DEBUG INFO
    // fbDebug_printNetworkContext( networkContext );


    FBInformationPacketTuple packetA = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    FBInformationPacketTuple packetB = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    FBInformationPacketTuple packetC = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    FBInformationPacketHandle outputPacket;

    int32_t valueA = 55;
    int32_t valueB = 44;
    int32_t valueC = 33;

    // set data
    fbData_set_int32( packetA.buffer, valueA );
    fbData_set_int32( packetB.buffer, valueB );
    fbData_set_int32( packetC.buffer, valueC );


    //---------------------------------------------------------------
    // Send information packets into the sample network
    //---------------------------------------------------------------
    REQUIRE( fbConnection_write( &connectionA, packetA.handle ) );
    REQUIRE( fbConnection_write( &connectionB, packetB.handle ) );
    REQUIRE( fbConnection_write( &connectionC, packetC.handle ) );


    //---------------------------------------------------------------
    // Run the network
    //---------------------------------------------------------------
    int cntr = 7;
    while( cntr > 0 && fbScheduler_mainLoop( networkContext ) ) {
        // the network will run until component terminates
        // by reaching OP_TERMINATE opcode at which point
        // scheduler will terminate both input connections
        // and close all ports on them and then close
        // component's output port.
        // output connection with result in it will still
        // be alive and our portResult will still be open

        // DEBUG INFO
        // fbDebug_printNetworkContextState( networkContext );
        cntr--;
    }

    outputPacket = fbConnection_read( &connectionOut );
    REQUIRE( outputPacket != FB_INVALID_PACKET_HANDLE );

    byte_t *outputData = fbInformationPacket_getDataBuffer( outputPacket );
    int32_t result = fbData_get_int32( outputData );

    REQUIRE( result == - ( valueA + valueB + valueC ) );


    fbInformationPacket_drop( outputPacket );
    fbNetwork_free( networkContext );

    printf("valueA = %d\n", valueA);
    printf("valueB = %d\n", valueB);
    printf("valueC = %d\n", valueC);
    printf("result = %d\n", result);

    return 0;
}
