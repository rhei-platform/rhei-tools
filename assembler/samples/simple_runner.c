/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/scheduler.h"
#include "vm/debug/vm_debug.h"


extern FBNetworkContext *create_network();

int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    FBNetworkContext *networkContext = create_network();


    //---------------------------------------------------------------
    // Run the network
    //---------------------------------------------------------------
    do {
        // DEBUG INFO
        // uncomment this line to see network context state printouts
        // for each iteration
        // fbDebug_printNetworkContextState( networkContext );
    }
    while( fbScheduler_mainLoop( networkContext ) );

    fbNetwork_destroy( networkContext );

    return 0;
}
