# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re
import sys
import os

import argparse

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile
from bpasmlib.bpasmccode import generate_c_code

from bptools.bpasmtools import generate_binary


parser = argparse.ArgumentParser(description='BPAsm compiler.')
parser.add_argument('file_name', type=str, help='component source file to compile')
parser.add_argument('-of', '--output-folder', type=str, help='path to folder that all output files will be placed to')

args = parser.parse_args()


file_name = args.file_name
output_folder_path = args.output_folder if args.output_folder else "./"
output_folder_path = os.path.normpath( output_folder_path )

if not os.path.isdir( output_folder_path ):
    print( "path: '", output_folder_path, "' does not exist or is not a folder" )
    sys.exit(1)


source = None

if file_name == '-':
    source = BPAsmSource( sys.stdin.readlines() )

else:
    file_name = os.path.normpath( file_name )
    if not os.path.isfile( file_name ):
        print( "file: '", file_name, "' is not a file" )
        sys.exit(1)

    source = BPAsmSourceFile(file_name)

output_file_name_base = os.path.splitext( os.path.basename( file_name ) )[0]


#========================================================================
#
# Write component C-code
#
#========================================================================

component = parse_component(source)

# returns ( generated function name, C source ) tuple
_, generated_source = generate_c_code( component )

output_source_file_name = os.path.normpath( os.path.join( output_folder_path, output_file_name_base + '.h' ) )

print( 'Writing', output_source_file_name )
with open( output_source_file_name, 'w' ) as source_file:
    source_file.write( generated_source )


#========================================================================
#
# Write component binary
#
#========================================================================

binary = generate_binary( component )

output_binary_file_name = os.path.normpath( os.path.join( output_folder_path, output_file_name_base + '.bin' ) )

print( 'Writing', output_binary_file_name )
with open( output_binary_file_name, 'wb' ) as source_file:
    source_file.write( binary )


# from bptools.hexdump import hexdump, print_hexdump_header

# print( "" )
# print_hexdump_header()
# hexdump( list( binary ) )

print( 'Done.' )
