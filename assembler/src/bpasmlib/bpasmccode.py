# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re

_generated_function_prefix = "fb_bpasm_get_"

def generate_c_code(component):

    source_output_top = \
    """
/************************************************
 *
 * BPScript assembler
 *
 * !!! AUTO-GENERATED FILE !!!
 *
 ************************************************/
    """

    generated_source = []

    generated_source.append( source_output_top )


#========================================================================
#
# Component meta-data in C-comments
#
#========================================================================

    if component.doc:
        generated_source.append( "/*" )
        generated_source.append( " * Component DOC:" )
        generated_source.append( " *" )
        generated_source.append( " *" + " *".join( component.doc ).rstrip() )
        generated_source.append( " *" )
        generated_source.append( " */" )

    for rec in component.records.values():
        rr = str(rec).splitlines()
        for r in rr:
            generated_source.append( "// " + r )
        generated_source.append( "" )

    generated_source.append( "// NAME = " + component.name + ";" )
    generated_source.append( "// " + "".join([ ".inputCount = ", component.inputs, ";" ]) )
    generated_source.append( "// " + "".join([ ".outputCount = ", component.outputs, ";" ]) )
    generated_source.append( "// " + "".join([ ".stackSize = ", component.stack, ";" ]) )
    generated_source.append( "// " + "".join([ ".informationPacketCount = ", component.packets, ";" ]) )
    generated_source.append( "// " + "".join([ ".dataSize = ", str(component.data.size()), ";" ]) )


#========================================================================
#
# Includes
#
#========================================================================

    generated_source.append( """
#include "vm/interpreter/types.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/context.h"
#include "vm/core/component.h"

    """ )

#========================================================================
#
# Prologue
#
#========================================================================

    fn_name = _generated_function_prefix + re.sub(r'[^a-zA-Z0-9]', '_', component.name)
    fn_name_line = [ "static FBComponent* ", fn_name, "() {" ]
    generated_source.append( "".join(fn_name_line) )
    generated_source.append( "" )

#========================================================================
#
# Constant pool
#
#========================================================================

    generated_source.append( "    static byte_t constant_pool [] = {" )

    for cp_item in component.constant_pool_data:
        parsed_line, code = cp_item        
        generated_source.append( "    " + "    " + "// " + parsed_line )
        code = ", ".join( map( str, code ) ) + ","
        generated_source.append( "    " + "    " + code )

    generated_source.append( "    };" )

    generated_source.append( "" )


#========================================================================
#
# Code generator helpers
#
#========================================================================

    def sourcerize_op(op):
        source_line, code, code_label = op
        return ( "// " + source_line, ", ".join( map( str, code ) ) + "," + (" // " + str(code_label) if code_label else "") )

    def code_to_source(code_ops):
        code_source = []

        code_ops = map( sourcerize_op, code_ops )
        for code_op in code_ops:
            comment, op = code_op
            code_source.append( comment )
            code_source.append( op )

        return code_source


#========================================================================
#
# Setup code
#
#========================================================================

    generated_source.append( "    static byte_t setup_code [] = {" )
    setup_code = code_to_source( component.setup_code )
    for code in setup_code:
        generated_source.append( "    " + "    " + code )
    generated_source.append( "    };" )

    generated_source.append( "" )


#========================================================================
#
# Loop code
#
#========================================================================

    generated_source.append( "    static byte_t loop_code [] = {" )
    loop_code = code_to_source( component.loop_code )
    for code in loop_code:
        generated_source.append( "    " + "    " + code )
    generated_source.append( "    };" )

    generated_source.append( "" )


#========================================================================
#
# Component struct
#
#========================================================================

    generated_source.append( """
    static FBComponent component = {
        // byte_t *setupCode;
        // index_t setupCodeSize;
        &setup_code[0],
        sizeof( setup_code ),
        // byte_t *loopCode;
        // index_t loopCodeSize;
        &loop_code[0],
        sizeof( loop_code ),

        // byte_t *constantPool;
        // index_t constantPoolSize;
        &constant_pool[0],
        sizeof( constant_pool ),
    """ )
    generated_source.append( "        // index_t dataSize;" )
    generated_source.append( "        " + str(component.data.size()) + "," )
    generated_source.append( "        // index_t stackSize;" )
    generated_source.append( "        " + str(component.stack) + "," )
    generated_source.append( "        // index_t informationPacketCount;" )
    generated_source.append( "        " + str(component.packets) + "," )
    generated_source.append( "        // index_t inputCount;" )
    generated_source.append( "        " + str(component.inputs) + "," )
    generated_source.append( "        // index_t outputCount;" )
    generated_source.append( "        " + str(component.outputs) + "," )
    generated_source.append( "    };" )


#========================================================================
#
# Epilogue
#
#========================================================================

    generated_source.append( """
    return &component;
}
    """ )

    return ( fn_name, "\n".join(generated_source) )
