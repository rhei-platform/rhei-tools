# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re

from .bpasmparsers import *
from .bpasmrec import RecordDef
from .bpasmrec import parse_record, parse_data_section

#========================================================================
#
# Component parser
#
#========================================================================

class Component:
    def __init__(self):
        self.name = '<component>'
        self.doc = ['']
        self.inputs = 0
        self.outputs = 0
        self.stack = 0
        self.packets = 0
        self.data = None

        # record name -> RecordDef
        self.records = {}
        # array of tuples ( parsed_line, code ) where:
        #   parsed_line -> string containing CP item 'declaration'
        #   code -> array of strings where:
        #       code[ 0 ] -> 'OP_*' string representing opcode from FBVMInstruction enum in opcodes.h
        #       code[ 1: ] -> string representations of bytes of this opcode's immediate parameters, if any
        self.constant_pool_data = []
        # array of 'op_items' where 'op_items' is a tuple of:
            # op_items[ 0 ] -> parsed source line
            # op_items[ 1 ] -> array of compiled bytecodes (as strings)
            # op_items[ 2 ] -> code label ref or None
        self.setup_code = []
        # same structur as setup_code
        self.loop_code = []



def parse_component(source):
    
    parsed_records = {}
    parsed_name = None
    parsed_doc = None
    parsed_inputs = None
    parsed_outputs = None
    parsed_stack = None
    parsed_packets = None
    parsed_data_record = None
    constant_pool_data = None
    setup_code_ops = None
    loop_code_ops = None


    parsed_data = parse_record(source)
    while parsed_data:
        if parsed_data.name in parsed_records.keys():
            raise SyntaxError( "repeated definition of record with name '" + parsed_data.name + "'." )
        
        parsed_records[ parsed_data.name ] = parsed_data

        parsed_data = parse_record(source)

    parsed_data = parse_name(source)
    if parsed_data:
        parsed_name = parsed_data

    parsed_data = parse_doc_section(source)
    if parsed_data:
        parsed_doc = parsed_data

    parsed_data = parse_inputs(source)
    if parsed_data:
        parsed_inputs = parsed_data

    parsed_data = parse_outputs(source)
    if parsed_data:
        parsed_outputs = parsed_data

    parsed_data = parse_stack(source)
    if parsed_data:
        parsed_stack = parsed_data

    parsed_data = parse_packets(source)
    if parsed_data:
        parsed_packets = parsed_data

    parsed_data = parse_data_section(source)
    if parsed_data:
        parsed_data_record = parsed_data
        parsed_records[ parsed_data_record.name ] = parsed_data_record

    parsed_data = parse_constant_pool(source)
    if parsed_data:
        constant_pool_data = parsed_data
        constant_pool_data, cp_record = constant_pool_data
        parsed_records[ cp_record.name ] = cp_record

    parsed_data = parse_setup_code(source, parsed_records)
    if parsed_data:
        setup_code_ops = parsed_data

    parsed_data = parse_loop_code(source, parsed_records)
    if parsed_data:
        loop_code_ops = parsed_data


    if not source.end_of_source():
        raise SyntaxError( "Source not fully parsed. Stopped at line {0}: '{1}'".format( source.current_line_number(), source.current_line() ) )


    if parsed_name is None:
        raise SyntaxError( "NAME is missing" )

    # if parsed_doc is None:
    #     raise SyntaxError( "DOC is missing" )

    if parsed_inputs is None:
        raise SyntaxError( "INPUTS is missing" )

    if parsed_outputs is None:
        raise SyntaxError( "OUTPUTS is missing" )

    if parsed_stack is None:
        raise SyntaxError( "STACK is missing" )

    if parsed_packets is None:
        raise SyntaxError( "PACKETS is missing" )

    if parsed_data_record is None:
        raise SyntaxError( "DATA is missing" )

    if constant_pool_data is None:
        raise SyntaxError( "CONSTANT POOL is missing" )

    if setup_code_ops is None:
        raise SyntaxError( "SETUP is missing" )

    if loop_code_ops is None:
        raise SyntaxError( "LOOP is missing" )


    component = Component()

    component.records = parsed_records

    component.name = parsed_name
    component.doc = parsed_doc
    component.inputs = parsed_inputs
    component.outputs = parsed_outputs
    component.stack = parsed_stack
    component.packets = parsed_packets

    component.data = parsed_data_record
    component.constant_pool_data = constant_pool_data

    component.setup_code = setup_code_ops
    component.loop_code = loop_code_ops


    return component
