# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re
import struct

from .bpasmregexs import *
from .bpasmrec import RecordFieldDef


# https://docs.python.org/2/library/struct.html

def convert_i8( param ):

    param = param.strip()
    value = int(param)

    if value < -128 or 127 < value:
        raise SyntaxError( "Expected -128 <= value <= 127, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<b', value))
    return list(b)

def convert_u8( param ):

    param = param.strip()
    value = int(param)

    if value < -128 or 127 < value:
        raise SyntaxError( "Expected -128 <= value <= 127, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<B', value))
    return list(b)

def convert_i16( param ):

    param = param.strip()
    value = int(param)

    if value < -32768 or 32767 < value:
        raise SyntaxError( "Expected -32768 <= value <= 32767, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<h', value))
    return list(b)

def convert_u16( param ):

    param = param.strip()
    value = int(param)

    if value < 0 or 65535 < value:
        raise SyntaxError( "Expected 0 <= value <= 65535, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<H', value))
    return list(b)

def convert_i32( param ):

    param = param.strip()
    value = int(param)

    if value < -2147483648 or 2147483647 < value:
        raise SyntaxError( "Expected -2147483648 <= value <= 2147483647, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<i', value))
    return list(b)

def convert_u32( param ):

    param = param.strip()
    value = int(param)

    if value < 0 or 4294967295 < value:
        raise SyntaxError( "Expected 0 <= value <= 4294967295, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<I', value))
    return list(b)

def convert_i64( param ):

    param = param.strip()
    value = int(param)

    if value < -9223372036854775808 or 9223372036854775807 < value:
        raise SyntaxError( "Expected -9223372036854775808 <= value <= 9223372036854775807, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<q', value))
    return list(b)

def convert_u64( param ):

    param = param.strip()
    value = int(param)

    if value < 0 or 18446744073709551615 < value:
        raise SyntaxError( "Expected 0 <= value <= 18446744073709551615, got '" + value + "' instead." )
        return None

    b = bytearray(struct.pack('<Q', value))
    return list(b)

def convert_f( param ):

    param = param.strip()
    value = float(param)

    b = bytearray(struct.pack('<f', value))
    return list(b)

def convert_d( param ):

    param = param.strip()
    value = float(param)

    b = bytearray(struct.pack('<d', value))
    return list(b)

# https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals
def convert_string_escape_sequences( value ):
    """
        \\a     ASCII Bell (BEL)
        \\b     ASCII Backspace (BS)
        \\f     ASCII Formfeed (FF)
        \\n     ASCII Linefeed (LF)
        \\r     ASCII Carriage Return (CR)
        \\t     ASCII Horizontal Tab (TAB)
        \\v     ASCII Vertical Tab (VT)
        \\ooo   Character with octal value ooo
        \\xhh   Character with hex value hh
    """
    value = value.replace('\\a', '\a')
    value = value.replace('\\b', '\b')
    value = value.replace('\\f', '\f')
    value = value.replace('\\n', '\n')
    value = value.replace('\\r', '\r')
    value = value.replace('\\t', '\t')
    value = value.replace('\\v', '\v')
    # TODO: replace octal and hex sequences
    return value

# Pascal-style string:
# 16-bit unsigned length of the string, followed by string content
def convert_ps( param ):

    value = param.strip()
    value = convert_string_escape_sequences( value )
    length = len(value)

    if 65535 < length:
        raise SyntaxError( "Expected string of length <= 65535, got length = " + value + " instead." )
        return None

    b = bytearray(struct.pack('<H', length))
    b.extend( bytearray( value, 'ascii' ) )
    return list(b)

# C-style string:
# string content terminated by NULL-byte
def convert_cs( param ):

    value = param.strip()
    value = convert_string_escape_sequences( value )
    length = len(value)

    b = bytearray( value, 'ascii' )
    b.extend([0])
    return list(b)

# UTF-8 string:
# 16-bit unsigned length of UTF-8 encoded string content in bytes, followed by the UTF-8 encoded string content
def convert_utf8s( param ):
    raise ValueError( "NOT IMPLEMENTED: 'utf8s' string type support" )

def flatten_data(data):
    # https://stackoverflow.com/a/952952
    # I found the syntax hard to understand until I realized you can think of it exactly like nested for loops.
    # for sublist in data: for item in sublist: yield item
    return [item for sublist in data for item in sublist]

def pad_with_zeros( expected_item_count, item_values ):

    actual_item_count = len( item_values )

    if actual_item_count > expected_item_count:
        raise SyntaxError( "Array declared to have " + str( expected_item_count ) + " elements, got " + str( actual_item_count ) + " instead." )

    if actual_item_count < expected_item_count:
        item_values = item_values + ['0'] * ( expected_item_count - actual_item_count )

    return item_values


#========================================================================
#
# Constant pool item parser
#
#========================================================================

def parse_cp_item(line):

    reg = re.match( reConstantPoolItemInteger, line, re.M|re.I)
    if reg:
        item_type = reg.group( 1 )
        item_count = reg.group( 2 ) or '1'
        item_name = reg.group( 3 )
        item_value = reg.group( 4 )
        # item_more_values = map( lambda s: s.strip(), reg.group( 5 ).split(',') )[1:]
        item_more_values = [ s.strip() for s in reg.group( 5 ).split(',') ][1:]
        item_values = [item_value] + item_more_values
        item_values = pad_with_zeros( int(item_count), item_values )

        record_field_data = [ item_type, int(item_count), item_name ]

        parsed_line_items = [ item_type, " ", item_name or '<>', " [ ", item_count, " ] = ", ", ".join( item_values ) ]
        parsed_line = "".join(parsed_line_items)
        code = []

        if item_type == 'i8':
            code = flatten_data( map( convert_i8, item_values ) )
        elif item_type == 'u8':
            code = flatten_data( map( convert_u8, item_values ) )
        elif item_type == 'i16':
            code = flatten_data( map( convert_i16, item_values ) )
        elif item_type == 'u16':
            code = flatten_data( map( convert_u16, item_values ) )
        elif item_type == 'i32':
            code = flatten_data( map( convert_i32, item_values ) )
        elif item_type == 'u32':
            code = flatten_data( map( convert_u32, item_values ) )
        elif item_type == 'i64':
            code = flatten_data( map( convert_i64, item_values ) )
        elif item_type == 'u64':
            code = flatten_data( map( convert_u64, item_values ) )

        return ( parsed_line, code, record_field_data )

    reg = re.match( reConstantPoolItemReal, line, re.M|re.I)
    if reg:
        item_type = reg.group( 1 )
        item_count = reg.group( 2 ) or '1'
        item_name = reg.group( 3 )
        item_value = reg.group( 4 )
        # item_more_values = map( lambda s: s.strip(), reg.group( 5 ).split(',') )[1:]
        item_more_values = [ s.strip() for s in reg.group( 5 ).split(',') ][1:]
        item_values = [item_value] + item_more_values
        item_values = pad_with_zeros( int(item_count), item_values )

        record_field_data = [ item_type, int(item_count), item_name ]

        parsed_line_items = [ item_type, " ", item_name or '<>', " [ ", item_count, " ] = ", ", ".join( item_values ) ]
        parsed_line = "".join(parsed_line_items)
        code = []

        if item_type == 'f':
            code = flatten_data( map( convert_f, item_values ) )
        elif item_type == 'd':
            code = flatten_data( map( convert_d, item_values ) )
        
        return ( parsed_line, code, record_field_data )

    reg = re.match( reConstantPoolItemStringLine, line, re.M|re.I)
    if reg:
        item_type = reg.group( 1 )
        item_name = reg.group( 2 )
        item_value = reg.group( 3 )

        parsed_line_items = [ item_type, " ", item_name or '<>', " [", item_value, "]" ]
        parsed_line = "".join(parsed_line_items)

        if item_type == 'ps':
            code = convert_ps( item_value )
        elif item_type == 'cs':
            code = convert_cs( item_value )
        elif item_type == 'utf8s':
            code = convert_utf8s( item_value )

        record_field_data = [ item_type, 1, item_name, len(code) ]

        return ( parsed_line, code, record_field_data )

    return None

def parse_cp_multiline_string( source ):
    # this one must NOT move source position to the next line
    # in order to simulate on-line source parsers
    line = source.current_line()
    reg = re.match( reConstantPoolItemStringMultiLineStart, line, re.M|re.I)
    if reg:
        # pick up string type and name from string start match
        item_type = reg.group( 1 )
        item_name = reg.group( 2 )
        item_value_lines = []

        got_end_of_string_marker = False
        string_start_line_number = source.current_line_number()

        # collect the rest of the multi-line string
        while not source.end_of_source():
            line = source.next_line()

            # we call source.skip_empty_lines() everywhere
            # which makes sense (nothing to inspect on empty line)
            # but also helps with not having to check for a number of edge situations
            # like, for example, calling re.match() on an empty string
            # here we have to do that because we collect ALL lines
            reg = line and re.match( reConstantPoolItemStringMultiLineEnd, line, re.M|re.I)
            if reg:
                # we're done
                got_end_of_string_marker = True
                break

            # this line is not end-of-string marker
            # pick it up exactly as is
            item_value_lines.append( line )

        if not got_end_of_string_marker:
            raise SyntaxError('Missing end-of-string line for multiline string definition starting @' + str(string_start_line_number) )

        item_value = '\n'.join( item_value_lines )

        parsed_line_items = [ item_type, " ", item_name or '<>', " [", item_value, "]" ]
        parsed_line = "".join(parsed_line_items)

        if item_type == 'ps':
            code = convert_ps( item_value )
        elif item_type == 'cs':
            code = convert_cs( item_value )
        elif item_type == 'utf8s':
            code = convert_utf8s( item_value )

        record_field_data = [ item_type, 1, item_name, len(code) ]

        return ( parsed_line, code, record_field_data )

    return None
