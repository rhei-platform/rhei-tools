# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re
import struct

from .bpasmregexs import *

# https://docs.python.org/3/library/struct.html

def signed_short_or_byte_param_to_byte_array( param ):

    param = param.strip()
    value = int(param)

    if value < -32768 or 32767 < value:
        raise SyntaxError( "Expected -32768 <= value <= 32767, got '" + value + "' instead." )

    format = '>h'
    if -128 <= value and value <= 127:
        format = '>b'

    b = bytearray(struct.pack(format, value))
    return list(b)

def signed_short_param_to_byte_array( param ):

    param = param.strip()
    value = int(param)

    if value < -32768 or 32767 < value:
        raise SyntaxError( "Expected -32768 <= value <= 32767, got '" + value + "' instead." )

    format = '>h'

    b = bytearray(struct.pack(format, value))
    return list(b)


def unsigned_short_or_byte_param_to_byte_array( param ):

    param = param.strip()
    value = int(param)

    if value < 0 or 65535 < value:
        raise SyntaxError( "Expected 0 <= value <= 65535, got '" + value + "' instead." )

    b = bytearray(struct.pack('>H', value))
    result = list(b)

    if 0 <= value and value <= 255:
        return result[1:]
    return result

def require_unsigned_short_byte_array( param_array ):

    if len( param_array ) > 2:
        raise SyntaxError( "Expected 0 <= value <= 65535, got " + str( len( param_array ) * 8 ) + "-bit value instead." )

    if len( param_array ) == 1:
        return [ 0, param_array[ 0 ] ]
    
    return param_array

def require_unsigned_byte_byte_array( param_array ):

    if len( param_array ) > 1:
        raise SyntaxError( "Expected 0 <= value <= 255, got " + str( len( param_array ) * 8 ) + "-bit value instead." )

    return param_array

def get_record_field_offset( records, record_name, record_field ):

    offset = 0

    if not record_name in records.keys():
        raise SyntaxError( "Unknown record name '" + record_name + "'" )
    
    rec = records[ record_name ]
    rec_field = rec.get_field( record_field )
    if not rec_field:
        raise SyntaxError( "Unknown record field name '" + record_name + "." + record_field + "'" )

    return unsigned_short_or_byte_param_to_byte_array( str(rec_field.offset) )

def decode_offset(param, records):
    reg = re.match( reIntegerValue, param, re.M|re.I)
    if reg:
        # param == integer literal
        return unsigned_short_or_byte_param_to_byte_array( param )
    else:
        reg = re.match( reRecordFieldReference, param, re.M|re.I)
        if reg:
            # param == possible record field reference
            record_name = reg.group( 1 )
            record_field = reg.group( 2 )
            return get_record_field_offset( records, record_name, record_field )
    
    return None

def get_record_size( records, record_name ):

    offset = 0

    if not record_name in records.keys():
        raise SyntaxError( "Unknown record name '" + record_name + "'" )
    
    rec = records[ record_name ]

    return unsigned_short_or_byte_param_to_byte_array( str(rec.size()) )

def decode_record_size(param, records):
    reg = re.match( reIntegerValue, param, re.M|re.I)
    if reg:
        # param == integer literal
        return unsigned_short_or_byte_param_to_byte_array( param )
    else:
        reg = re.match( reRecordNameReference, param, re.M|re.I)
        if reg:
            # param == possible record name reference
            record_name = reg.group( 1 )
            return get_record_size( records, record_name )
    
    return None

# find all lines NOT starting with 'def'
# ^(?!def|\.).*\n

# reNOP
def op_nop(line, records):
    reg = re.match( reNOP, line, re.M|re.I)
    if not re.match( reNOP, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_NOP"] )

# reINVALID
def op_invalid(line, records):
    if not re.match( reINVALID, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_INVALID"] )

# reEND
def op_end(line, records):
    if not re.match( reEND, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_END"] )
# reTERMINATE
def op_terminate(line, records):
    if not re.match( reTERMINATE, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_TERMINATE"] )

# reSWITCH
def op_switch(line, records):
    reg = re.match( reSWITCH, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_SWITCH"]

    param = require_unsigned_short_byte_array( decode_offset( reg.group( 1 ), records ) )
    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reJUMP
def op_jump(line, records):
    reg = re.match( reJUMP, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )

# reJUMP_IF_FALSE
def op_jump_if_false(line, records):
    reg = re.match( reJUMP_IF_FALSE, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_FALSE"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_LESS_EQUAL
def op_jump_if_less_equal(line, records):
    reg = re.match( reJUMP_IF_LESS_EQUAL, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_LESS_EQUAL"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_GREATER_EQUAL
def op_jump_if_greater_equal(line, records):
    reg = re.match( reJUMP_IF_GREATER_EQUAL, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_GREATER_EQUAL"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_LESS
def op_jump_if_less(line, records):
    reg = re.match( reJUMP_IF_LESS, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_LESS"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_GREATER
def op_jump_if_greater(line, records):
    reg = re.match( reJUMP_IF_GREATER, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_GREATER"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_EQUAL
def op_jump_if_equal(line, records):
    reg = re.match( reJUMP_IF_EQUAL, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_EQUAL"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_NOT_EQUAL
def op_jump_if_not_equal(line, records):
    reg = re.match( reJUMP_IF_NOT_EQUAL, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_NOT_EQUAL"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )
# reJUMP_IF_ZERO_2
def op_jump_if_zero_2(line, records):
    reg = re.match( reJUMP_IF_ZERO_2, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_JUMP_IF_ZERO_2"]
    code_label = None

    if reg.group( 1 ):
        # literal integer offset value
        param = signed_short_param_to_byte_array( reg.group( 1 ) )
    elif reg.group( 2 ):
        # code label to jump to
        code_label = reg.group( 2 ).strip()
        param = [ '0', '0' ]
    else:
        raise ValueError( "Expected offset or code label but got '" + line + "' instead" )

    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str, code_label )


# reCONST_I8
def op_const_i8(line, records):
    reg = re.match( reCONST_I8, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_PCONST_I8"]

    if reg.group( 1 ):

        op_str = ["OP_SCONST_I8"]

        param = decode_offset( reg.group( 1 ), records )
        if len(param) == 1:
            op_str = ["OP_BCONST_I8"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reCONST_I16
def op_const_i16(line, records):
    reg = re.match( reCONST_I16, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_PCONST_I16"]

    if reg.group( 1 ):

        op_str = ["OP_SCONST_I16"]

        param = decode_offset( reg.group( 1 ), records )
        if len(param) == 1:
            op_str = ["OP_BCONST_I16"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reCONST_I32
def op_const_i32(line, records):
    reg = re.match( reCONST_I32, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_PCONST_I32"]

    if reg.group( 1 ):

        op_str = ["OP_SCONST_I32"]

        param = decode_offset( reg.group( 1 ), records )
        if len(param) == 1:
            op_str = ["OP_BCONST_I32"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reCONST_I64
def op_const_i64(line, records):
    reg = re.match( reCONST_I64, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_PCONST_I64"]

    if reg.group( 1 ):

        op_str = ["OP_SCONST_I64"]

        param = decode_offset( reg.group( 1 ), records )
        if len(param) == 1:
            op_str = ["OP_BCONST_I64"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )

# reCONST_0
def op_const_0(line, records):
    if not re.match( reCONST_0, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CONST_0"] )
# reCONST_1
def op_const_1(line, records):
    if not re.match( reCONST_1, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CONST_1"] )
# reCONST_M1
def op_const_m1(line, records):
    if not re.match( reCONST_M1, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CONST_M1"] )

# rePUSH
def op_push(line, records):
    reg = re.match( rePUSH, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_SPUSH"]

    param = decode_offset( reg.group( 1 ), records )

    if len(param) == 1:
        op_str = ["OP_BPUSH"]
    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )

# reDUP
def op_dup(line, records):
    if not re.match( reDUP, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_DUP"] )
# reDUP_2
def op_dup_2(line, records):
    if not re.match( reDUP_2, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_DUP_2"] )

# reCLOAD_I8
def op_cload_i8(line, records):
    if not re.match( reCLOAD_I8, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CLOAD_I8"] )
# reCLOAD_I16
def op_cload_i16(line, records):
    if not re.match( reCLOAD_I16, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CLOAD_I16"] )
# reCLOAD_I32
def op_cload_i32(line, records):
    if not re.match( reCLOAD_I32, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CLOAD_I32"] )
# reCLOAD_I64
def op_cload_i64(line, records):
    if not re.match( reCLOAD_I64, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CLOAD_I64"] )

# reLOAD_I8
def op_load_i8(line, records):
    reg = re.match( reLOAD_I8, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_LOAD_I8"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("LOAD op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BLOAD_I8"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reLOAD_I16
def op_load_i16(line, records):
    reg = re.match( reLOAD_I16, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_LOAD_I16"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("LOAD op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BLOAD_I16"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reLOAD_I32
def op_load_i32(line, records):
    reg = re.match( reLOAD_I32, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_LOAD_I32"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("LOAD op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BLOAD_I32"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reLOAD_I64
def op_load_i64(line, records):
    reg = re.match( reLOAD_I64, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_LOAD_I64"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("LOAD op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BLOAD_I64"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
    return ( line.strip(), "," )

# reCSTORE_I8
def op_cstore_i8(line, records):
    if not re.match( reCSTORE_I8, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CSTORE_I8"] )
# reCSTORE_I16
def op_cstore_i16(line, records):
    if not re.match( reCSTORE_I16, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CSTORE_I16"] )
# reCSTORE_I32
def op_cstore_i32(line, records):
    if not re.match( reCSTORE_I32, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CSTORE_I32"] )
# reCSTORE_I64
def op_cstore_i64(line, records):
    if not re.match( reCSTORE_I64, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CSTORE_I64"] )

# reSTORE_I8
def op_store_i8(line, records):
    reg = re.match( reSTORE_I8, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_STORE_I8"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("STORE op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BSTORE_I8"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reSTORE_I16
def op_store_i16(line, records):
    reg = re.match( reSTORE_I16, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_STORE_I16"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("STORE op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BSTORE_I16"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reSTORE_I32
def op_store_i32(line, records):
    reg = re.match( reSTORE_I32, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_STORE_I32"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("STORE op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BSTORE_I32"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )
# reSTORE_I64
def op_store_i64(line, records):
    reg = re.match( reSTORE_I64, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_STORE_I64"]

    param = reg.group( 1 )
    if param:
        param = decode_offset( param, records )
        if len(param) > 1:
            raise SyntaxError("STORE op only supports offset parameter in range [0, 255]")
        op_str = ["OP_BSTORE_I64"]
        op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )


# reNEG_I
def op_neg_i(line, records):
    if not re.match( reNEG_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_NEG_I"] )
# reNEG_L
def op_neg_l(line, records):
    if not re.match( reNEG_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_NEG_L"] )
# reNEG_F
def op_neg_f(line, records):
    if not re.match( reNEG_F, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_NEG_F"] )
# reNEG_D
def op_neg_d(line, records):
    if not re.match( reNEG_D, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_NEG_D"] )

# reNOT
def op_not(line, records):
    if not re.match( reNOT, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_NOT"] )

# reBIT_INVERT_I
def op_bit_invert_i(line, records):
    if not re.match( reBIT_INVERT_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_INVERT_I"] )
# reBIT_INVERT_L
def op_bit_invert_l(line, records):
    if not re.match( reBIT_INVERT_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_INVERT_L"] )

# reMUL_I
def op_mul_i(line, records):
    if not re.match( reMUL_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_MUL_I"] )
# reMUL_L
def op_mul_l(line, records):
    if not re.match( reMUL_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_MUL_L"] )
# reMUL_F
def op_mul_f(line, records):
    if not re.match( reMUL_F, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_MUL_F"] )
# reMUL_D
def op_mul_d(line, records):
    if not re.match( reMUL_D, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_MUL_D"] )

# reDIV_I
def op_div_i(line, records):
    if not re.match( reDIV_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_DIV_I"] )
# reDIV_L
def op_div_l(line, records):
    if not re.match( reDIV_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_DIV_L"] )
# reDIV_F
def op_div_f(line, records):
    if not re.match( reDIV_F, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_DIV_F"] )
# reDIV_D
def op_div_d(line, records):
    if not re.match( reDIV_D, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_DIV_D"] )

# reMOD_I
def op_mod_i(line, records):
    if not re.match( reMOD_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_MOD_I"] )
# reMOD_L
def op_mod_l(line, records):
    if not re.match( reMOD_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_MOD_L"] )

# reADD_I
def op_add_i(line, records):
    if not re.match( reADD_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_ADD_I"] )
# reADD_L
def op_add_l(line, records):
    if not re.match( reADD_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_ADD_L"] )
# reADD_F
def op_add_f(line, records):
    if not re.match( reADD_F, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_ADD_F"] )
# reADD_D
def op_add_d(line, records):
    if not re.match( reADD_D, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_ADD_D"] )

# reSUB_I
def op_sub_i(line, records):
    if not re.match( reSUB_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SUB_I"] )
# reSUB_L
def op_sub_l(line, records):
    if not re.match( reSUB_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SUB_L"] )
# reSUB_F
def op_sub_f(line, records):
    if not re.match( reSUB_F, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SUB_F"] )
# reSUB_D
def op_sub_d(line, records):
    if not re.match( reSUB_D, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SUB_D"] )

# reSHIFT_LEFT_I
def op_shift_left_i(line, records):
    if not re.match( reSHIFT_LEFT_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SHIFT_LEFT_I"] )
# reSHIFT_LEFT_L
def op_shift_left_l(line, records):
    if not re.match( reSHIFT_LEFT_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SHIFT_LEFT_L"] )
# reSHIFT_RIGHT_I
def op_shift_right_i(line, records):
    if not re.match( reSHIFT_RIGHT_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SHIFT_RIGHT_I"] )
# reSHIFT_RIGHT_L
def op_shift_right_l(line, records):
    if not re.match( reSHIFT_RIGHT_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_SHIFT_RIGHT_L"] )
# reBIT_AND_I
def op_bit_and_i(line, records):
    if not re.match( reBIT_AND_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_AND_I"] )
# reBIT_AND_L
def op_bit_and_l(line, records):
    if not re.match( reBIT_AND_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_AND_L"] )
# reBIT_XOR_I
def op_bit_xor_i(line, records):
    if not re.match( reBIT_XOR_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_XOR_I"] )
# reBIT_XOR_L
def op_bit_xor_l(line, records):
    if not re.match( reBIT_XOR_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_XOR_L"] )
# reBIT_OR_I
def op_bit_or_i(line, records):
    if not re.match( reBIT_OR_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_OR_I"] )
# reBIT_OR_L
def op_bit_or_l(line, records):
    if not re.match( reBIT_OR_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_BIT_OR_L"] )

# reCMP_I
def op_cmp_i(line, records):
    if not re.match( reCMP_I, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CMP_I"] )
# reCMP_UI
def op_cmp_ui(line, records):
    if not re.match( reCMP_UI, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CMP_UI"] )
# reCMP_L
def op_cmp_l(line, records):
    if not re.match( reCMP_L, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CMP_L"] )
# reCMP_UL
def op_cmp_ul(line, records):
    if not re.match( reCMP_UL, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CMP_UL"] )
# reCMP_F
def op_cmp_f(line, records):
    if not re.match( reCMP_F, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CMP_F"] )
# reCMP_D
def op_cmp_d(line, records):
    if not re.match( reCMP_D, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_CMP_D"] )

# reAND
def op_and(line, records):
    if not re.match( reAND, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_AND"] )
# reOR
def op_or(line, records):
    if not re.match( reOR, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_OR"] )


# reREAD
def op_read(line, records):
    reg = re.match( reREAD, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_REQUEST_READ"]

    param1 = reg.group( 1 )
    param2 = reg.group( 2 )

    if param1 and param2:
        param1 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( param1 ) )
        param2 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( param2 ) )
        op_str = ["OP_BREQUEST_READ"]
        op_str += [ str(p1) for p1 in param1 ] + [ str(p2) for p2 in param2 ]

    return ( line.strip(), op_str )
# reWRITE
def op_write(line, records):
    reg = re.match( reWRITE, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_REQUEST_WRITE"]

    param1 = reg.group( 1 )
    param2 = reg.group( 2 )

    if param1 and param2:
        param1 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( param1 ) )
        param2 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( param2 ) )
        op_str = ["OP_BREQUEST_WRITE"]
        op_str += [ str(p1) for p1 in param1 ] + [ str(p2) for p2 in param2 ]

    return ( line.strip(), op_str )
# reCREATE
def op_create(line, records):
    reg = re.match( reCREATE, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_SCREATE_IP"]

    param1 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( reg.group( 1 ) ) )
    param2 = reg.group( 2 )

    if param2:
        param2 = require_unsigned_short_byte_array( decode_record_size( param2, records ) )
        op_str = ["OP_CREATE_IP"]
        op_str += [ str(p1) for p1 in param1 ] + [ str(p2) for p2 in param2 ]
    else:
        op_str += [ str(p1) for p1 in param1 ]

    return ( line.strip(), op_str )
# reDROP
def op_drop(line, records):
    reg = re.match( reDROP, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_DROP_IP"]

    param = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( reg.group( 1 ) ) )
    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )

# reYIELD
def op_yield(line, records):
    if not re.match( reYIELD, line, re.M|re.I):
        return None

    return ( line.strip(), ["OP_YIELD"] )

# reLENGTH
def op_length(line, records):
    reg = re.match( reLENGTH, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_IP_LENGTH"]

    param = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( reg.group( 1 ) ) )
    op_str += [ str(p) for p in param ]

    return ( line.strip(), op_str )

# reCOPY
def op_copy(line, records):
    reg = re.match( reCOPY, line, re.M|re.I)
    if not reg:
        return None

    op_str = ["OP_IP_COPY"]

    param1 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( reg.group( 1 ) ) )
    param2 = require_unsigned_byte_byte_array( unsigned_short_or_byte_param_to_byte_array( reg.group( 2 ) ) )
    op_str += [ str(p1) for p1 in param1 ] + [ str(p2) for p2 in param2 ]

    return ( line.strip(), op_str )


def parse_op(line, records):

    ops = [
        op_nop,
        op_invalid,
        op_end,
        op_terminate,

        op_switch,
        op_jump,
        op_jump_if_false,
        op_jump_if_less_equal,
        op_jump_if_greater_equal,
        op_jump_if_less,
        op_jump_if_greater,
        op_jump_if_equal,
        op_jump_if_not_equal,
        op_jump_if_zero_2,

        op_const_i8,
        op_const_i16,
        op_const_i32,
        op_const_i64,
        op_const_0,
        op_const_1,
        op_const_m1,

        op_push,
        op_dup,
        op_dup_2,

        op_cload_i8,
        op_cload_i16,
        op_cload_i32,
        op_cload_i64,
        op_load_i8,
        op_load_i16,
        op_load_i32,
        op_load_i64,

        op_cstore_i8,
        op_cstore_i16,
        op_cstore_i32,
        op_cstore_i64,
        op_store_i8,
        op_store_i16,
        op_store_i32,
        op_store_i64,

        op_neg_i,
        op_neg_l,
        op_neg_f,
        op_neg_d,
        op_not,
        op_bit_invert_i,
        op_bit_invert_l,
        op_mul_i,
        op_mul_l,
        op_mul_f,
        op_mul_d,
        op_div_i,
        op_div_l,
        op_div_f,
        op_div_d,
        op_mod_i,
        op_mod_l,
        op_add_i,
        op_add_l,
        op_add_f,
        op_add_d,
        op_sub_i,
        op_sub_l,
        op_sub_f,
        op_sub_d,

        op_shift_left_i,
        op_shift_left_l,
        op_shift_right_i,
        op_shift_right_l,
        op_bit_and_i,
        op_bit_and_l,
        op_bit_xor_i,
        op_bit_xor_l,
        op_bit_or_i,
        op_bit_or_l,

        op_cmp_i,
        op_cmp_ui,
        op_cmp_l,
        op_cmp_ul,
        op_cmp_f,
        op_cmp_d,
        op_and,
        op_or,

        op_read,
        op_write,
        op_create,
        op_drop,

        op_yield,

        op_length,
        op_copy,
    ]

    for op_fn in ops:
        op_items = op_fn( line, records )
        if op_items:
            # op_fn may return 2- or 3-item tuple:
            # op[ 0 ] -> parsed source line
            # op[ 1 ] -> array of compiled bytecodes (as strings)
            # op[ 2 ] -> if present: code label ref
            # these two lines make sure op_items is always 3-item tuple
            # so we can unpack it into 3 variables
            op_items = op_items + (None, None)
            op_items = op_items[0:3]
            return op_items
            # return {
            #     'source': op_items[0], # string
            #     'bytecode': op_items[1], # array of strings
            #     'label': op_items[2], # string or None
            # }

    return None

