# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re

from .bpasmregexs import *
from .bpasmops import parse_op
from .bpasmcpitems import parse_cp_item, parse_cp_multiline_string
from .bpasmcpitems import convert_i16
from .bpasmrec import RecordDef



#========================================================================
#
# Name
#
#========================================================================

def parse_name(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reName, line, re.M|re.I)
    if not reg:
        return None
    
    name = reg.group(1)
    source.next_line()

    return name


#========================================================================
#
# Doc section
#
#========================================================================

def parse_doc_section(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reDoc, line, re.M|re.I)
    if not reg:
        return None
    
    source.next_line()

    lines = []
    source.skip_empty_lines()
    while not source.end_of_source():
        line = source.current_line()

        if not re.search(r'^[ \t]',line):
            break

        lines.append( line )
        source.next_line()
        source.skip_empty_lines()

    return lines


#========================================================================
#
# Inputs
#
#========================================================================

def parse_inputs(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reInputs, line, re.M|re.I)
    if not reg:
        return None
    
    inputs = reg.group(1)
    source.next_line()

    return inputs


#========================================================================
#
# Outputs
#
#========================================================================

def parse_outputs(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reOutputs, line, re.M|re.I)
    if not reg:
        return None
    
    outputs = reg.group(1)
    source.next_line()

    return outputs


#========================================================================
#
# Stack
#
#========================================================================

def parse_stack(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reStack, line, re.M|re.I)
    if not reg:
        return None
    
    stack = reg.group(1)
    source.next_line()

    return stack


#========================================================================
#
# Packets
#
#========================================================================

def parse_packets(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( rePackets, line, re.M|re.I)
    if not reg:
        return None
    
    packets = reg.group(1)
    source.next_line()

    return packets




#========================================================================
#
# Constant pool
#
#========================================================================

def _parse_cp_items(source):
    source.skip_empty_lines()

    cp_items = []

    while not source.end_of_source():
        line = source.current_line()

        if not re.search(r'^[ \t]',line):
            break

        cp_item = parse_cp_item(line)

        if not cp_item:
            cp_item = parse_cp_multiline_string(source)

        if not cp_item:
            raise SyntaxError("expected cp_item @ " + str(source.current_line_number()) + ", got '" + line.splitlines()[0] + "' instead." )

        cp_items.append( cp_item )

        source.next_line()
        source.skip_empty_lines()

    return cp_items

def parse_constant_pool(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reConstantPool, line, re.M|re.I)
    if not reg:
        return None
    
    constant_pool_data = []

    source.next_line()

    cp_items = _parse_cp_items(source)
    cp_record_field_data = []

    for cp_item in cp_items:
        parsed_line, code, record_field_data = cp_item
        constant_pool_data.append( ( parsed_line, code ) )
        cp_record_field_data.append( record_field_data )

    record_def = RecordDef('$CONST')
    for cp_record_field in cp_record_field_data:
        record_def.add_field( cp_record_field )

    return ( constant_pool_data, record_def )


#========================================================================
#
# Setup/loop code
#
#========================================================================

class CodeLabels:
    def __init__(self):
        self.labels = {}
    def add_label(self, label_name, bytecode_offset):
        if label_name in self.labels.keys():
            raise ValueError("Label with name '" + label_name + "' already defined.")
        self.labels[ label_name ] = bytecode_offset
    def get_bytecode_offset(self, label_name):
        if not label_name in self.labels.keys():
            raise ValueError("Undefined label with name '" + label_name + "'")
        return self.labels[ label_name ]

class CodeLabelReference:
    def __init__(self, label_name, index, bytecode_offset):
        self.label_name = label_name # the label being referenced
        self.array_index = index # where the op referencing the label is in ops array
        self.jump_start_offset = bytecode_offset # first bytecode after the the op referencing the label

def _resolve_label_references( code_ops, label_refs, code_labels ):

    for ref in label_refs:

        label_offset = code_labels.get_bytecode_offset( ref.label_name )
        offset_param_value = label_offset - ref.jump_start_offset
        # print( offset_param_value )
        offset_param_value = convert_i16( str(offset_param_value) )

        op_items = code_ops[ ref.array_index ]
        op_items_code = op_items[ 1 ]
        # replace ['0','0'] placeholder with real jump offset
        # !!!NOTE!!!: we are flipping offset_param_value bytes since
        # convert_i16() returns value in little-endian order
        # and all immediate values in bytecode are big-endian
        op_items_code[ -1 ] = offset_param_value[ -2 ]
        op_items_code[ -2 ] = offset_param_value[ -1 ]



def _parse_code_ops(source, records):
    source.skip_empty_lines()

    code_ops = []

    # resolving label reference support
    code_labels = CodeLabels()
    label_refs = [] # of CodeLabelReference
    current_offset = 0

    while not source.end_of_source():
        line = source.current_line()

        if not re.search(r'^[ \t]',line):
            break
        
        labelReg = re.match( reCodeLabel, line )
        if labelReg:
            # code label
            label_name = labelReg.group( 1 )
            code_labels.add_label( label_name, current_offset )

        else:
            # an op
            op_items = parse_op(line, records)
            if not op_items:
                raise SyntaxError( "expected op @ " + str(source.current_line_number()) + ", got '" + line.splitlines()[0] + "' instead." )

            # op_items[ 0 ] -> parsed source line
            # op_items[ 1 ] -> array of compiled bytecodes (as strings)
            # op_items[ 2 ] -> code label ref or None
            current_offset += len( op_items[ 1 ] )

            label_name = op_items[ 2 ]
            if label_name:
                # this op references a code label
                # add it to our bookkeeping array
                label_refs.append( CodeLabelReference( label_name, len( code_ops ), current_offset ) )

            code_ops.append( op_items )

        source.next_line()
        source.skip_empty_lines()

    _resolve_label_references( code_ops, label_refs, code_labels )

    # code_ops: array of arrays each of which is:
    # code_ops[ idx ][ 0 ] -> parsed source line
    # code_ops[ idx ][ 1 ] -> array of compiled bytecodes (as strings)
    # code_ops[ idx ][ 2 ] -> code label ref or None
    return code_ops



def parse_setup_code(source, records):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reSetup, line, re.M|re.I)
    if not reg:
        return None

    source.next_line()

    return _parse_code_ops(source, records)


def parse_loop_code(source, records):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reLoop, line, re.M|re.I)
    if not reg:
        return False
    
    source.next_line()

    return _parse_code_ops(source, records)



