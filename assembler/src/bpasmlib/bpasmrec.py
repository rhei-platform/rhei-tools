# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re

from .bpasmregexs import *


types = {
    "i8": {
        "name": "i8",
        "size": 1,
    },
    "u8": {
        "name": "u8",
        "size": 1,
    },
    "i16": {
        "name": "i16",
        "size": 2,
    },
    "u16": {
        "name": "u16",
        "size": 2,
    },
    "i32": {
        "name": "i32",
        "size": 4,
    },
    "u32": {
        "name": "u32",
        "size": 4,
    },
    "i64": {
        "name": "i64",
        "size": 8,
    },
    "u64": {
        "name": "u64",
        "size": 8,
    },
    "f": {
        "name": "f",
        "size": 4,
    },
    "d": {
        "name": "d",
        "size": 8,
    },
}

class RecordFieldDef:

    def __init__(self):
        self.name = None
        self.type = None
        self.element_size = 0
        self.element_count = 0
        self.offset = 0
        self.size = 0

    def __str__(self):
        return self.type + \
        "[" + str(self.element_count) + "] " + \
        str(self.name or "<>") + \
        " @" + str(self.offset) + \
        " [" + str(self.size) + "]"

class FieldExistsError(TypeError):
    pass

class RecordDef:

    def __init__(self, name = None):
        self.name = name
        self.fields = []
        self.fields_by_name = {}
        self.current_offset = 0

    def __str__(self):
        str_val = "'" + str(self.name or "<>") + "' [" + str(self.size()) + "] :\n"
        for field in self.fields:
            str_val += str(field) + "\n"
        return str_val
    
    def size(self):
        return self.current_offset
    
    def get_field(self, name):
        if name in self.fields_by_name:
            return self.fields_by_name[name]
        return None

    def add_field(self, record_item):
        
        field_type = record_item[ 0 ]

        if field_type in types:
            # base type

            field_name = record_item[ 2 ]
            if field_name and field_name in self.fields_by_name:
                raise FieldExistsError(field_name)

            type_desc = types[ field_type ]

            field_def = RecordFieldDef()

            field_def.name = field_name
            field_def.element_count = record_item[ 1 ]
            field_def.type = type_desc["name"]
            field_def.element_size = type_desc["size"]
            field_def.size = field_def.element_count * field_def.element_size
            field_def.offset = self.current_offset
            self.current_offset += field_def.size

            self.fields.append( field_def )
            if field_name:
                self.fields_by_name[ field_name ] = field_def

        elif field_type == 'cs' or  field_type == 'ps':
            # 'cs': C-style string
            # 'ps': Pascal-style string

            field_name = record_item[ 2 ]
            if field_name and field_name in self.fields_by_name:
                raise FieldExistsError(field_name)

            field_def = RecordFieldDef()

            field_def.name = field_name
            field_def.element_count = record_item[ 1 ]
            field_def.type = field_type
            field_def.element_size = record_item[ 3 ]
            field_def.size = field_def.element_count * field_def.element_size
            field_def.offset = self.current_offset
            self.current_offset += field_def.size

            self.fields.append( field_def )
            if field_name:
                self.fields_by_name[ field_name ] = field_def

        elif field_type == 'utf8s':
            # UTF-8 string
            raise SyntaxError( "not supported: UTF-8 string -> " + str(record_item) )

        else:
            # a record
            raise SyntaxError( "not supported: a record -> " + str(record_item) )




def get_record_item(line):
    reg = re.match( reRecordItem, line, re.M|re.I )
    if not reg:
        return None
    
    item_type = reg.group( 1 ).strip()
    item_count = reg.group( 2 )
    if item_count:
        item_count = int(item_count)
    else:
        item_count = 1
    item_name = reg.group( 3 )

    return [item_type, item_count, item_name]



def get_record(lines, name = None):
    record_def = RecordDef(name)
    for line in lines:
        if isinstance( line, str ):
            record_item = get_record_item( line )
            if not record_item:
                raise SyntaxError("Expected record item and got '" + line + "'")
            record_def.add_field( record_item )
    return record_def


def parse_record(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reRecord, line, re.M|re.I)
    if not reg:
        return None
    
    record_name = reg.group( 1 )

    source.next_line()

    lines = []
    source.skip_empty_lines()
    while not source.end_of_source():
        line = source.current_line()

        if not re.search(r'^[ \t]',line):
            break

        lines.append( line )
        source.next_line()
        source.skip_empty_lines()

    rec = get_record(lines,record_name)

    return rec


def parse_data_section(source):
    source.skip_empty_lines()
    line = source.current_line()
    if line is None:
        return False
    reg = re.match( reData, line, re.M|re.I)
    if not reg:
        return None
    
    record_name = "$DATA"

    source.next_line()

    lines = []
    source.skip_empty_lines()
    while not source.end_of_source():
        line = source.current_line()

        if not re.search(r'^[ \t]',line):
            break

        lines.append( line )
        source.next_line()
        source.skip_empty_lines()

    rec = get_record(lines,record_name)

    return rec
