# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0


_reIndent = r'^[ \t]+'
_reIdentifier = r'[a-zA-Z][a-zA-Z_0-9]*'
_reIntegerType = r'i8|u8|i16|u16|i32|u32|i64|u64'
_reFloatType = r'f|d'
_reType = _reIntegerType + '|' + _reFloatType + '|r\.' + _reIdentifier
# ps - Pascal string, cs - C string, utf8s - UTF8 string
_reStringType = r'ps|cs|utf8s'
_reArrayDimension = r'\[\s*(\d+)\s*\]'
_reIntegerValue = r'[-+]?\s*\d+'
# TODO: make better float matcher
_reFloatValue = r'[-+]?\d*\.?\d+'

# # reg = re.match(_reStringLiteralGroup, ...)
# # string_value = reg.group(1) or reg.group(2)
# _reStringLiteralGroup = r'(?:' + r'"([^"]*)"' + r'|' + r"'([^']*)'" + r')'

_reCodeLabelIdentifier = r'_' + _reIdentifier
_reJumpTarget = r'(?:(' + _reIntegerValue + r')|(' + _reCodeLabelIdentifier + r'))'



#==============================================================
#
# General
#
#==============================================================

reLineComment = r'^\s*(?:[;#].*)?$'
reEolComment = r'(?:[;#].*)?$'


#==============================================================
#
# Network
#
#==============================================================

# \[ ident \]
_reContextGroup = r'\[\s*(' + _reIdentifier + r')\s*\]'
# \< ident \>
_reNativeFunctionGroup = r'<\s*(' + _reIdentifier + r')\s*>'
# \< ident [ , ident ] \>
_reNativeFunctionBufferGroup = r'<\s*(' + _reIdentifier + r')\s*' + \
    r'(?:,\s*(' + _reIdentifier + '))?' + r'\s*>'

_reNumericValue = r'\d+'
# --capacity-->
_reArrowWithCapacity = r'(?:-+\s*(' + _reNumericValue + ')\s*)?-+>'
# -->
_reArrow = r'-+>'

_reInputOutputGroup = r'(' + _reIdentifier + r')'


#
# Network connnection
#

_rePortGroup = r'(' + _reNumericValue + r')'

#     1        2            3              4        5
# \[ident\] [ : port ] --[capacity]--> [ port : ] \[ident\]
# optional group value -> None
reConnection = \
    r'^\s*' + _reContextGroup + r'(?:\s*:\s*' + _rePortGroup + r')?' + r'\s*' + \
    _reArrowWithCapacity + \
    r'\s*' + r'(?:' + _rePortGroup + r'\s*:\s*)?' + _reContextGroup + r'\s*' + \
    reEolComment

#
# Network input
#

#     1              2          3
# \[ident\] ---> [ port : ] \[ident\]
# optional group value -> None
reNetworkInput = \
    r'^\s*' + _reInputOutputGroup + r'\s*' + \
    _reArrow + \
    r'\s*' + r'(?:' + _rePortGroup + r'\s*:\s*)?' + _reContextGroup + r'\s*' + \
    reEolComment

#
# Network output
#

#      1        2             3
# \[ident\] [ : port ] --> \[ident\]
# optional group value -> None
reNetworkOutput = \
    r'^\s*' + _reContextGroup + r'(?:\s*:\s*' + _rePortGroup + r')?' + r'\s*' + \
    _reArrow + \
    r'\s*' + _reInputOutputGroup + r'\s*' + \
    reEolComment

#
# Network native input
#

#     1      2           3            4          5
# \<ident[,ident]\> --[capacity]--> [ port : ] \[ident\]
# optional group value -> None
reNetworkNativeInput = \
    r'^\s*' + _reNativeFunctionBufferGroup + r'\s*' + \
    _reArrowWithCapacity + \
    r'\s*' + r'(?:' + _rePortGroup + r'\s*:\s*)?' + _reContextGroup + r'\s*' + \
    reEolComment

#
# Network native output
#

#      1        2            3             4     5
# \[ident\] [ : port ] --[capacity]--> \<ident[,ident]\>
# optional group value -> None
reNetworkNativeOutput = \
    r'^\s*' + _reContextGroup + r'(?:\s*:\s*' + _rePortGroup + r')?' + r'\s*' + \
    _reArrowWithCapacity + \
    r'\s*' + _reNativeFunctionBufferGroup + r'\s*' + \
    reEolComment

#
# Native-to-native connection
#

#     1       2            3            4      5
# \<ident[,ident]\> --[capacity]--> \<ident[,ident]\>
# optional group value -> None
reNativeToNativeConnection = \
    r'\s*' + _reNativeFunctionBufferGroup + r'\s*' + \
    _reArrowWithCapacity + \
    r'\s*' + _reNativeFunctionBufferGroup + r'\s*' + \
    reEolComment

#
# Network context
#

_reComponent = r'(' + _reIdentifier + r')'
_rePath = r'((?:' + _reIdentifier + r'\.)*)'

#     1                    2              3
# \[ ident \]  <-----  [path.path.path.]ident
# optional group value -> empty string
reContext = r'^\s*' + _reContextGroup + r'\s*' + r'<-+' + r'\s*' + _rePath + _reComponent + r'\s*' + reEolComment

#
# Native function context
#

#      1                    2             3
# \< ident \>  <-----  [path.path.path.]ident
# optional group value -> empty string
reNativeFunctionContext = r'^\s*' + _reNativeFunctionGroup + r'\s*' + r'<-+' + r'\s*' + _rePath + _reComponent + r'\s*' + reEolComment

#
# Buffer definition
#
# \( ident \)
_reBufferGroup = r'{\s*(' + _reIdentifier + r')\s*}'
#     1                   2
# \( ident \)  <----- [extern]
reBufferStart = r'^\s*' + _reBufferGroup + r'\s*' + r'<-+' + '(\s*extern)?' + r'\s*' + reEolComment
# EOB  <-----
reBufferEnd = r'^\s*' + 'EOB' + r'\s*' + r'<-+' + r'\s*' + reEolComment


#==============================================================
#
# Component
#
#==============================================================

reName = r'^name:\s*(\w+)\s*' + reEolComment
reDoc = r'^doc:\s*' + reEolComment
reInputs = r'^inputs:\s*(\d+)\s*' + reEolComment
reOutputs = r'^outputs:\s*(\d+)\s*' + reEolComment
reData = r'^data:\s*' + reEolComment
reStack = r'^stack:\s*(\d+)\s*' + reEolComment
rePackets = r'^packets:\s*(\d+)\s*' + reEolComment

reConstantPool = r'^constant_pool:\s*' + reEolComment

reConstantPoolItemInteger = _reIndent + r'(' + _reIntegerType + ')' + \
    r'(?:' + _reArrayDimension + r')?' + \
    r'(?:\s+(' + _reIdentifier + r'))?' + \
    r'\s+(' + _reIntegerValue + r')' + \
    r'((?:\s*,\s*' + _reIntegerValue + r')*)' + \
    r'\s*' + reEolComment
reConstantPoolItemReal = _reIndent + r'(' + _reFloatType + ')' + \
    r'(?:' + _reArrayDimension + r')?' + \
    r'(?:\s+(' + _reIdentifier + r'))?' + \
    r'\s+(' + _reFloatValue + r')' + \
    r'((?:\s*,\s*' + _reFloatValue + r')*)' + \
    r'\s*' + reEolComment
reConstantPoolItemStringLine = _reIndent + r'(' + _reStringType + ')' + \
    r'(?:\s+(' + _reIdentifier + r'))?' + \
    r'\s+' + r'"(.*)' + r'$'
reConstantPoolItemStringMultiLineStart = _reIndent + r'(' + _reStringType + ')' + \
    r'(?:\s+(' + _reIdentifier + r'))?' + \
    r'\s+' + r'\'\'\'' + r'$'
reConstantPoolItemStringMultiLineEnd = r'^' + r'\'\'\'' + r'$'

reSetup = r'^setup:\s*' + reEolComment
reLoop = r'^loop:\s*' + reEolComment
reCodeLabel = _reIndent + r'(' + _reCodeLabelIdentifier + r')\s*:\s*' + reEolComment

reRecord = r'^record:\s*(' + _reIdentifier + ')\s*' + reEolComment
reRecordItem = _reIndent + r'(' + _reType + r')' + r'(?:' + _reArrayDimension + r')?' + r'(?:\s+(' + _reIdentifier + r'))?\s*' + reEolComment
reIntegerValue = r'^(\d+)$'
reRecordFieldReference = r'^\&(\$?' + _reIdentifier + r')\.(' + _reIdentifier + r')$'
reRecordNameReference = r'^\&(\$?' + _reIdentifier + r')$'



reNOP = _reIndent + r'NOP\s*' + reEolComment
reINVALID = _reIndent + r'INVALID\s*' + reEolComment

reEND = _reIndent + r'END\s*' + reEolComment
reTERMINATE = _reIndent + r'TERMINATE\s*' + reEolComment

reSWITCH = _reIndent + r'SWITCH\s+([^\s]+)\s*' + reEolComment
reJUMP = _reIndent + r'JUMP\s+' + _reJumpTarget + r'\s*' + reEolComment

reJUMP_IF_FALSE = _reIndent + r'JUMP_IF_FALSE\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_LESS_EQUAL = _reIndent + r'JUMP_IF_LESS_EQUAL\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_GREATER_EQUAL = _reIndent + r'JUMP_IF_GREATER_EQUAL\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_LESS = _reIndent + r'JUMP_IF_LESS\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_GREATER = _reIndent + r'JUMP_IF_GREATER\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_EQUAL = _reIndent + r'JUMP_IF_EQUAL\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_NOT_EQUAL = _reIndent + r'JUMP_IF_NOT_EQUAL\s+' + _reJumpTarget + r'\s*' + reEolComment
reJUMP_IF_ZERO_2 = _reIndent + r'JUMP_IF_ZERO_2\s+' + _reJumpTarget + r'\s*' + reEolComment


reCONST_I8 = _reIndent + r'CONST_I8(?:\s+([^\s]+))?\s*' + reEolComment
reCONST_I16 = _reIndent + r'CONST_I16(?:\s+([^\s]+))?\s*' + reEolComment
reCONST_I32 = _reIndent + r'CONST_I32(?:\s+([^\s]+))?\s*' + reEolComment
reCONST_I64 = _reIndent + r'CONST_I64(?:\s+([^\s]+))?\s*' + reEolComment

reCONST_0 = _reIndent + r'CONST_0\s*' + reEolComment
reCONST_1 = _reIndent + r'CONST_1\s*' + reEolComment
reCONST_M1 = _reIndent + r'CONST_M1\s*' + reEolComment

rePUSH = _reIndent + r'PUSH\s+([^\s]+)\s*' + reEolComment

reDUP = _reIndent + r'DUP\s*' + reEolComment
reDUP_2 = _reIndent + r'DUP_2\s*' + reEolComment
reCLOAD_I8 = _reIndent + r'CLOAD_I8\s*' + reEolComment
reCLOAD_I16 = _reIndent + r'CLOAD_I16\s*' + reEolComment
reCLOAD_I32 = _reIndent + r'CLOAD_I32\s*' + reEolComment
reCLOAD_I64 = _reIndent + r'CLOAD_I64\s*' + reEolComment

reLOAD_I8 = _reIndent + r'LOAD_I8(?:\s+([^\s]+))?\s*' + reEolComment
reLOAD_I16 = _reIndent + r'LOAD_I16(?:\s+([^\s]+))?\s*' + reEolComment
reLOAD_I32 = _reIndent + r'LOAD_I32(?:\s+([^\s]+))?\s*' + reEolComment
reLOAD_I64 = _reIndent + r'LOAD_I64(?:\s+([^\s]+))?\s*' + reEolComment

reCSTORE_I8 = _reIndent + r'CSTORE_I8\s*' + reEolComment
reCSTORE_I16 = _reIndent + r'CSTORE_I16\s*' + reEolComment
reCSTORE_I32 = _reIndent + r'CSTORE_I32\s*' + reEolComment
reCSTORE_I64 = _reIndent + r'CSTORE_I64\s*' + reEolComment

reSTORE_I8 = _reIndent + r'STORE_I8(?:\s+([^\s]+))?\s*' + reEolComment
reSTORE_I16 = _reIndent + r'STORE_I16(?:\s+([^\s]+))?\s*' + reEolComment
reSTORE_I32 = _reIndent + r'STORE_I32(?:\s+([^\s]+))?\s*' + reEolComment
reSTORE_I64 = _reIndent + r'STORE_I64(?:\s+([^\s]+))?\s*' + reEolComment


reNEG_I = _reIndent + r'NEG_I\s*' + reEolComment
reNEG_L = _reIndent + r'NEG_L\s*' + reEolComment
reNEG_F = _reIndent + r'NEG_F\s*' + reEolComment
reNEG_D = _reIndent + r'NEG_D\s*' + reEolComment

reNOT = _reIndent + r'NOT\s*' + reEolComment

reBIT_INVERT_I = _reIndent + r'BIT_INVERT_I\s*' + reEolComment
reBIT_INVERT_L = _reIndent + r'BIT_INVERT_L\s*' + reEolComment

reMUL_I = _reIndent + r'MUL_I\s*' + reEolComment
reMUL_L = _reIndent + r'MUL_L\s*' + reEolComment
reMUL_F = _reIndent + r'MUL_F\s*' + reEolComment
reMUL_D = _reIndent + r'MUL_D\s*' + reEolComment

reDIV_I = _reIndent + r'DIV_I\s*' + reEolComment
reDIV_L = _reIndent + r'DIV_L\s*' + reEolComment
reDIV_F = _reIndent + r'DIV_F\s*' + reEolComment
reDIV_D = _reIndent + r'DIV_D\s*' + reEolComment

reMOD_I = _reIndent + r'MOD_I\s*' + reEolComment
reMOD_L = _reIndent + r'MOD_L\s*' + reEolComment

reADD_I = _reIndent + r'ADD_I\s*' + reEolComment
reADD_L = _reIndent + r'ADD_L\s*' + reEolComment
reADD_F = _reIndent + r'ADD_F\s*' + reEolComment
reADD_D = _reIndent + r'ADD_D\s*' + reEolComment

reSUB_I = _reIndent + r'SUB_I\s*' + reEolComment
reSUB_L = _reIndent + r'SUB_L\s*' + reEolComment
reSUB_F = _reIndent + r'SUB_F\s*' + reEolComment
reSUB_D = _reIndent + r'SUB_D\s*' + reEolComment

reSHIFT_LEFT_I = _reIndent + r'SHIFT_LEFT_I\s*' + reEolComment
reSHIFT_LEFT_L = _reIndent + r'SHIFT_LEFT_L\s*' + reEolComment
reSHIFT_RIGHT_I = _reIndent + r'SHIFT_RIGHT_I\s*' + reEolComment
reSHIFT_RIGHT_L = _reIndent + r'SHIFT_RIGHT_L\s*' + reEolComment
reBIT_AND_I = _reIndent + r'BIT_AND_I\s*' + reEolComment
reBIT_AND_L = _reIndent + r'BIT_AND_L\s*' + reEolComment
reBIT_XOR_I = _reIndent + r'BIT_XOR_I\s*' + reEolComment
reBIT_XOR_L = _reIndent + r'BIT_XOR_L\s*' + reEolComment
reBIT_OR_I = _reIndent + r'BIT_OR_I\s*' + reEolComment
reBIT_OR_L = _reIndent + r'BIT_OR_L\s*' + reEolComment

reCMP_I = _reIndent + r'CMP_I\s*' + reEolComment
reCMP_UI = _reIndent + r'CMP_UI\s*' + reEolComment
reCMP_L = _reIndent + r'CMP_L\s*' + reEolComment
reCMP_UL = _reIndent + r'CMP_UL\s*' + reEolComment
reCMP_F = _reIndent + r'CMP_F\s*' + reEolComment
reCMP_D = _reIndent + r'CMP_D\s*' + reEolComment

reAND = _reIndent + r'AND\s*' + reEolComment
reOR = _reIndent + r'OR\s*' + reEolComment


reREAD = _reIndent + r'READ(?:\s+(\d+)\s*,\s*(\d+))?\s*' + reEolComment
reWRITE = _reIndent + r'WRITE(?:\s+(\d+)\s*,\s*(\d+))?\s*' + reEolComment
reCREATE = _reIndent + r'CREATE\s+(\d+)(?:\s*,\s*([^\s]+))?\s*' + reEolComment
reDROP = _reIndent + r'DROP\s+(\d+)\s*' + reEolComment

reYIELD = _reIndent + r'YIELD\s*' + reEolComment

reLENGTH = _reIndent + r'LENGTH\s+(\d+)\s*' + reEolComment
reCOPY = _reIndent + r'COPY\s+(\d+)\s*,\s*(\d+)\s*' + reEolComment
