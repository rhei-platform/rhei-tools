# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re

from .bpasmregexs import *

class BPAsmSource(object):
    def __init__(self,source, start_line_number=1):
        self.bpasm_source = []
        if isinstance(source, str):
            self.bpasm_source = [source]
        elif isinstance(source, list):
            self.bpasm_source = source

        self.number_of_source_lines = len(self.bpasm_source)
        self.current_source_line = 0
        self.start_line_number = start_line_number

    def current_line_number(self):
        return self.current_source_line + self.start_line_number

    def end_of_source(self):
        return self.current_source_line >= self.number_of_source_lines

    def current_line(self):
        if self.end_of_source():
            return None
        return self.bpasm_source[ self.current_source_line ]

    def next_line(self):
        self.current_source_line += 1
        return self.current_line()


    def skip_empty_lines(self):
        line = self.current_line()
        while not self.end_of_source():
            if not isinstance(line, str):
                line = self.next_line()
            elif re.match( reLineComment, line, re.M|re.I):
                line = self.next_line()
            else:
                # is string and is not a line comment
                break

class BPAsmSourceFile(BPAsmSource):
    def __init__(self,source):
        bpasm_source = source
        if isinstance(source, str):
            with open( source ) as bpasm_file:
                bpasm_source = bpasm_file.readlines()
        super(BPAsmSourceFile,self).__init__(bpasm_source)
