# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import os

#========================================================================
#
# Helper functions
#
#========================================================================

def get_component_header_name(component):
    return component.name + ".h"

def get_contex_index_name(ctx):
    return "ctx_" + ctx.name + "_idx"

#========================================================================
#
# Network to C source code compiler
#
#========================================================================

def generate_network_c_code( network, compiled_components ):
    source_output_top = \
    """
/************************************************
 *
 * BPScript net compiler
 *
 * !!! AUTO-GENERATED FILE !!!
 *
 ************************************************/
    """

#========================================================================
#
# Headers
#
#========================================================================

    source_output_headers = [
        '#include "vm/util/network_builder.h"',
        "",
        '#include "vm/core/information_packet.h"',
        '#include "vm/core/scheduler.h"',
        '#include "vm/interpreter/data_access.h"',
        '#include "vm/core/connection.h"',
        "",
        '#include "vm/debug/vm_debug.h"',
        "",
    ]



#========================================================================
#
# Add buffer definitions
#
#========================================================================
# template code for buffer declaration:
    """
        static uint8_t buffer_name[] = {
            0xAF, 0x45, ...
        };
    """
    source_output_buffer_definitions = [
        "/* config buffers passed as parameters to native functions used by this network */",
    ]

    for buffer_name in network.buffer_definitions:
        buffer_definition = network.buffer_definitions[ buffer_name ]

        if buffer_definition.is_external:
            source_output_buffer_definitions.append( "extern uint8_t " + buffer_name + "[];" )

        else:
            buffer_data_source = []
            for definition_line in buffer_definition.definition_data:
                buffer_data_source.append( "// " + definition_line.meta_source )
                buffer_data_source.append( ", ".join( map(str,definition_line.data) ) )

            # print( blah, blah, blah )
            source_output_buffer_definitions.append( "uint8_t " + buffer_name + "[] = {" )
            for buffer_data_source_line in buffer_data_source:
                source_output_buffer_definitions.append( "    " + buffer_data_source_line )
            source_output_buffer_definitions.append( "};" )


    source_output_buffer_definitions.append( "" )
    # print

#========================================================================
#
# Native context function declarations
#
#========================================================================

    source_output_native_function_declarations = [
        "/* native functions used by this network */",
    ]
    """
        extern void inputFn( FBNativeContextHandle handle );
        extern void outputFn( FBNativeContextHandle handle );
    """
    for ctx in network.native_contexts.values():
        source_output_native_function_declarations.append( "extern void " + ctx.name + "( FBNativeContextHandle handle );" )
        # print( "extern void " + ctx.name + "( FBNativeContextHandle handle );" )

    source_output_native_function_declarations.append( "" )
    # print

#========================================================================
#
# Network doc/description comment
#
#========================================================================

    source_output_network_comment = \
    """
    /*
        TODO: insert network structure here
        from .bpnet file

        OUT = - ( A + B + C )

        +sum1+
        A--|0   |   +sum2+
        B--|1  0|---|0   |   +neg+
        +----+   |   0|---|0 0|---OUT
        C-----------|1   |   +---+
                    +----+

    */
    """


#========================================================================
#
# Network function prologue
#
#========================================================================

    source_output_network_function_header = [
        "",
        "FBNetworkContext* create_network() {",
        "",
        "    FBNetworkBuilderData nbdata;",
        "    fbNetworkBuilder_init( &nbdata );",
    ]

    source_output_generated = []
    source_output_generated_includes = []

    source_output_generated.append( "" )
    # print


#========================================================================
#
# Components declaration and fetch
#
#========================================================================

    """
    #include "tests/vm/samples/summator.h"
    #include "tests/vm/samples/negator.h"
    """
    """
        FBComponent* sumamtor = fbSamples_get_summator();
        FBComponent* negator = fbSamples_get_negator();
    """
    for key in compiled_components.keys():
        component, generated_fn_name, generated_source = compiled_components[ key ]
        # print( '#include "' + get_component_header_name( component ) + '"' )
        source_output_generated_includes.append( '#include "' + get_component_header_name( component ) + '"' )
        # print( "FBComponent* " + component.name + " = " + generated_fn_name + "();" )
        source_output_generated.append( "    " + "FBComponent* " + component.name + " = " + generated_fn_name + "();" )


    source_output_generated_includes.append( "" )
    source_output_generated.append( "" )
    # print


#========================================================================
#
# Create component instance contexts
#
#========================================================================

    """
        index_t ctxSum1idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        index_t ctxSum2idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );
    """
    context_indexes = {}

    for ctx in network.contexts.values():
        component_file = ctx.file_location()
        component, generated_fn_name, generated_source = compiled_components[ component_file ]
        context_index_name = get_contex_index_name(ctx)
        # print( "index_t " + context_index_name + " = fbNetworkBuilder_addComponentInstance( &nbdata, " + component.name + " );" )
        source_output_generated.append( "    " + "index_t " + context_index_name + " = fbNetworkBuilder_addComponentInstance( &nbdata, " + component.name + " );" )
        context_indexes[ ctx.name ] = context_index_name


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Add connections
#
#========================================================================

    """
        fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum1idx, 0, ctxSum2idx, 0, 1 );
        fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum2idx, 0, ctxNegidx, 0, 1 );
    """
    for conn in network.connections:
        source_context = network.contexts[ conn.source_port.context_name ]
        source_context_idx = context_indexes[ source_context.name ]
        source_port = conn.source_port.context_port

        sink_context = network.contexts[ conn.sink_port.context_name ]
        sink_context_idx = context_indexes[ sink_context.name ]
        sink_port = conn.sink_port.context_port

        capacity = conn.capacity

        # print( "fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, " + \ )
        #     source_context_idx + ", " + str(source_port) + ", " + \
        #     sink_context_idx + ", " + str(sink_port) + ", " + \
        #     str(capacity) + " );"
        source_output_generated.append( "    " + "fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, " + \
            source_context_idx + ", " + str(source_port) + ", " + \
            sink_context_idx + ", " + str(sink_port) + ", " + \
            str(capacity) + " );" )


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Add network inputs
#
#========================================================================

    """
        fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 0 );
        fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 1 );
        fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum2idx, 1 );
    """
    for inp in network.inputs.keys():
        network_input = network.inputs[ inp ]

        sink_context = network.contexts[ network_input.context_name ]
        sink_context_idx = context_indexes[ sink_context.name ]
        sink_port = network_input.context_port

        # print( "fbNetworkBuilder_setNetworkInput( &nbdata, " + sink_context_idx + ", " + str(sink_port) + ");" )
        source_output_generated.append( "    " + "fbNetworkBuilder_setNetworkInput( &nbdata, " + sink_context_idx + ", " + str(sink_port) + ");" )


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Add network outputs
#
#========================================================================

    """
        fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );
    """
    for out in network.outputs.keys():
        network_output = network.outputs[ out ]

        source_context = network.contexts[ network_output.context_name ]
        source_context_idx = context_indexes[ source_context.name ]
        source_port = network_output.context_port

        # print( "fbNetworkBuilder_setNetworkOutput( &nbdata, " + source_context_idx + ", " + str(source_port) + ");" )
        source_output_generated.append( "    " + "fbNetworkBuilder_setNetworkOutput( &nbdata, " + source_context_idx + ", " + str(source_port) + ");" )


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Add native inputs
#
#========================================================================


    """
        fbNetworkBuilder_setNativeInput( &nbdata, inputFn, NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInIdx, ctxIdx, port, 1 );
    """
    inpVarIdx = 0
    for inp in network.native_inputs.keys():
        native_input = network.native_inputs[ inp ]

        sink_context_name = native_input.port.context_name
        sink_context_idx = context_indexes[ sink_context_name ]
        sink_port = native_input.port.context_port

        capacity = native_input.capacity

        native_function_name = native_input.native_context.name
        buffer_definition_name = native_input.buffer_name()
        buffer_definition_size = "sizeof( " + buffer_definition_name + " )" if buffer_definition_name != "NULL" else "0"

        inputIndexVar = "nativeInIdx_" + str(inpVarIdx)
        inpVarIdx += 1

        source_output_generated.append( "    " + "index_t " + inputIndexVar + " = " + \
            "fbNetworkBuilder_setNativeInput( &nbdata, " + \
            native_function_name + ", " + \
            buffer_definition_name + ", " + buffer_definition_size + " );" )

        source_output_generated.append( "    " + "fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, " + \
            inputIndexVar + ", " + \
            sink_context_idx + ", " + str(sink_port) + ", " + \
            # capacity == 1
            str(capacity) + \
            # "1" + \
            " );" )


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Add native outputs
#
#========================================================================

    """
        fbNetworkBuilder_setNativeOutput( &nbdata, outputFn, NULL, 0 );
        fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxIdx, port, nativeOutIdx, 1 );
    """
    outVarIdx = 0
    for out in network.native_outputs.keys():
        native_output = network.native_outputs[ out ]

        source_context_name = native_output.port.context_name
        source_context_idx = context_indexes[ source_context_name ]
        source_port = native_output.port.context_port

        capacity = native_input.capacity

        native_function_name = native_output.native_context.name
        buffer_definition_name = native_output.buffer_name()
        buffer_definition_size = "sizeof( " + buffer_definition_name + " )" if buffer_definition_name != "NULL" else "0"

        outputIndexVar = "nativeOutIdx_" + str(outVarIdx)
        outVarIdx += 1

        source_output_generated.append( "    " + "index_t " + outputIndexVar + " = " + \
            "fbNetworkBuilder_setNativeOutput( &nbdata, " + \
            native_function_name + ", " + \
            buffer_definition_name + ", " + buffer_definition_size + " );" )

        source_output_generated.append( "    " + "fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, " + \
            source_context_idx + ", " + str(source_port) + ", " + \
            outputIndexVar + ", " + \
            # capacity == 1
            str(capacity) + \
            # "1" + \
            " );" )


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Add native-to-native connections
#
#========================================================================

    """
        index_t nativeInIdx = fbNetworkBuilder_setNativeInput( &nbdata, inputFn, config_buffer_in, sizeof(config_buffer_in) );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, outputFn, config_buffer_out, sizeof(config_buffer_out) );
        fbNetworkBuilder_addConnection_Ntv2Ntv( &nbdata, nativeInIdx, nativeOutIdx, capacity );
    """
    n2nCnxIdx = 0
    for n2n_cnx in network.n2n_connections:

        source_native_context = n2n_cnx.source_native_context
        source_buffer_name = n2n_cnx.source_buffer_name()
        source_buffer_size = "sizeof( " + source_buffer_name + " )" if source_buffer_name != "NULL" else "0"

        connection_capacity = n2n_cnx.capacity

        sink_native_context = n2n_cnx.sink_native_context
        sink_buffer_name = n2n_cnx.sink_buffer_name()
        sink_buffer_size = "sizeof( " + sink_buffer_name + " )" if sink_buffer_name != "NULL" else "0"

        inputIndexVar = "nativeInIdx_" + str(n2nCnxIdx)
        outputIndexVar = "nativeOutIdx_" + str(n2nCnxIdx)
        n2nCnxIdx += 1

        source_output_generated.append( "    " + "index_t " + inputIndexVar + " = " + \
            "fbNetworkBuilder_setNativeInput( &nbdata, " + \
            source_native_context + ", " + \
            source_buffer_name + ", " + source_buffer_size + " );" )

        source_output_generated.append( "    " + "index_t " + outputIndexVar + " = " + \
            "fbNetworkBuilder_setNativeOutput( &nbdata, " + \
            sink_native_context + ", " + \
            sink_buffer_name + ", " + sink_buffer_size + " );" )

        source_output_generated.append( "    " + "fbNetworkBuilder_addConnection_Ntv2Ntv( &nbdata, " + \
            inputIndexVar + ", " + \
            outputIndexVar + ", " + \
            # capacity == 1
            str(connection_capacity) + \
            # "1" + \
            " );" )


    source_output_generated.append( "" )
    # print


#========================================================================
#
# Network function epilogue
#
#========================================================================

    source_output_network_function_footer = [
        "",
        "    FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );",
        "    fbNetworkBuilder_free( &nbdata );",
        "",
        "    // DEBUG INFO",
        "    // fbDebug_printNetworkContext( networkContext );",
        "",
        "    return networkContext;",
        "}",
    ]


#========================================================================
#
# Package all up and ship it out
#
#========================================================================

    generated_source = \
        [ source_output_top ] + \
        source_output_headers + \
        source_output_generated_includes + \
        source_output_native_function_declarations + \
        source_output_buffer_definitions + \
        [ source_output_network_comment ] + \
        source_output_network_function_header + \
        source_output_generated + \
        source_output_network_function_footer

    return generated_source
