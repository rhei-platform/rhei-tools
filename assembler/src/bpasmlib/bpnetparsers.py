# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re
import os

from .bpasmregexs import reContext, reNativeFunctionContext, reConnection, reNetworkInput, reNetworkOutput, reNetworkNativeInput, reNetworkNativeOutput, reNativeToNativeConnection
from .bpasmregexs import reBufferStart, reBufferEnd
from .bpasmcpitems import parse_cp_item, parse_cp_multiline_string
from .bpasmsource import BPAsmSource


#========================================================================
#
# Network definition elements classes
#
#========================================================================

class Context:
    def __init__(self, name, package, file_name):
        self.name = name
        self.package = package.replace( '.', '/' )
        self.file_name = file_name + '.bpasm'
    def file_location(self):
        return os.path.normpath( os.path.join( self.package, self.file_name ) )
    def __str__(self):
        return \
            self.name + " <- " + self.package + self.file_name

class NativeContext:
    def __init__(self, name, package, file_name):
        self.name = name
        self.package = package.replace( '.', '/' )
        self.file_name = file_name + '.c'
    def file_location(self):
        return os.path.normpath( os.path.join( self.package, self.file_name ) )
    def __str__(self):
        return \
            self.name + " <- " + self.file_location()

class Port(object):
    def __init__(self, context_name, context_port):
        self.context_name = context_name
        self.context_port = int(context_port)

class InputPort(Port):
    def __init__(self, context_name, context_port):
        super(InputPort,self).__init__(context_name,context_port)
    def is_input(self):
        return True
    def is_output(self):
        return False
    def __str__(self):
        return str(self.context_port) + ":" + self.context_name

class OutputPort(Port):
    def __init__(self, context_name, context_port):
        super(OutputPort,self).__init__(context_name,context_port)
    def is_input(self):
        return False
    def is_output(self):
        return True
    def __str__(self):
        return self.context_name + ":" + str(self.context_port)


class NativeInputOutput(object):
    def __init__(self, native_context, buffer_definition_name, connection_capacity, port):
        self.native_context = native_context
        self.buffer_definition_name = buffer_definition_name
        self.capacity = int(connection_capacity)
        self.port = port
    def buffer_name(self):
        return self.buffer_definition_name or 'NULL'
    def __str__(self):
        if self.port.is_input():
            return \
                "<"+self.native_context.name+","+self.buffer_name()+">" + \
                " --["+str(self.capacity)+"]--> "+ \
                str(self.port)
        if self.port.is_output():
            return \
                str(self.port) + \
                " --["+str(self.capacity)+"]--> "+ \
                "<"+self.native_context.name+","+self.buffer_name()+">"
        return "unknown port type: " + str(self.port)

class Connection:
    def __init__(self, source_port, connection_capacity, sink_port):
        self.source_port = source_port
        self.capacity = int(connection_capacity)
        self.sink_port = sink_port
    def __str__(self):
        return \
            str(self.source_port) + \
            " --["+str(self.capacity)+"]--> "+ \
            str(self.sink_port)

class N2NConnection:
    def __init__(self, source_native_context, source_buffer, connection_capacity, sink_native_context, sink_buffer):
        self.source_native_context = source_native_context
        self.source_buffer = source_buffer
        self.capacity = int(connection_capacity)
        self.sink_native_context = sink_native_context
        self.sink_buffer = sink_buffer
    def source_buffer_name(self):
        return self.source_buffer or 'NULL'
    def sink_buffer_name(self):
        return self.sink_buffer or 'NULL'
    def __str__(self):
        return \
            "<"+self.source_native_context.name+","+self.source_buffer_name()+">" + \
            " --["+str(self.capacity)+"]--> "+ \
            "<"+self.sink_native_context.name+","+self.sink_buffer_name()+">"


#========================================================================
#
# Network class
#
#========================================================================

class Network:
    def __init__(self):
        self.contexts = {}
        self.native_contexts = {}
        self.connections = []
        self.inputs = {}
        self.outputs = {}
        self.native_inputs = {}
        self.native_outputs = {}
        self.buffer_definitions = {}
        self.n2n_connections = []

    def add_context(self, context_name, component_package, component_file_name):
        if context_name in self.contexts:
            raise SyntaxError("Context '" + context_name + "' is already defined")

        context = Context( context_name, component_package, component_file_name )
        self.contexts[ context_name ] = context

    def add_native_context(self, native_context_name, native_function_package, native_function_file_name):
        if native_context_name in self.native_contexts:
            raise SyntaxError("Native context '" + native_context_name + "' is already defined")

        context = NativeContext( native_context_name, native_function_package, native_function_file_name )
        self.native_contexts[ native_context_name ] = context

    def add_connection(self, source_context, source_context_port, sink_context, sink_context_port, connection_capacity):
        if int(connection_capacity) <= 0:
            raise ValueError("Connection capacity must be >= 1 ")

        sourceport = OutputPort( source_context, source_context_port )
        sinkport = InputPort( sink_context, sink_context_port )
        connection = Connection( sourceport, connection_capacity, sinkport )
        if not source_context in self.contexts:
            raise SyntaxError("Connection '" + str(connection) + "' references undefined context '" + source_context + "'")
        if not sink_context in self.contexts:
            raise SyntaxError("Connection '" + str(connection) + "' references undefined context '" + sink_context + "'")

        self.connections.append( connection )

    def add_input(self, name, sink_context, sink_context_port):
        if name in self.inputs:
            raise SyntaxError("Network input '" + name + "' is already defined")

        input_port = InputPort( sink_context, sink_context_port )
        if not sink_context in self.contexts:
            raise SyntaxError("Port '" + str(input_port) + "' references undefined context '" + sink_context + "'")

        self.inputs[ name ] = input_port

    def add_output(self, name, source_context, source_context_port):
        if name in self.outputs:
            raise SyntaxError("Network output '" + name + "' is already defined")

        output_port = OutputPort( source_context, source_context_port )
        if not source_context in self.contexts:
            raise SyntaxError("Port '" + str(output_port) + "' references undefined context '" + source_context + "'")

        self.outputs[ name ] = output_port

    @staticmethod
    def native_io_key( native_context_name, port ):
        return native_context_name + ':' + port.context_name + ':' + str(port.context_port)

    def add_native_input(self, native_context_name, buffer_definition_name, connection_capacity, sink_context, sink_context_port):
        if int(connection_capacity) <= 0:
            raise ValueError("Connection capacity must be >= 1 ")
        if not native_context_name in self.native_contexts:
            raise SyntaxError("Network native context '" + native_context_name + "' is undefined")
        if buffer_definition_name and not buffer_definition_name in self.buffer_definitions:
            raise SyntaxError("Bufer definition reference '" + buffer_definition_name + "' is undefined")

        input_port = InputPort( sink_context, sink_context_port )
        if not sink_context in self.contexts:
            raise SyntaxError("Port '" + str(input_port) + "' references undefined context '" + sink_context + "'")

        nio_key = self.native_io_key( native_context_name, input_port )
        if nio_key in self.native_inputs:
            raise SyntaxError("Native context '" + native_context_name + "' is already connected to a port")

        native_context = self.native_contexts[ native_context_name ]
        self.native_inputs[ nio_key ] = NativeInputOutput( native_context, buffer_definition_name, connection_capacity, input_port )

    def add_native_output(self, native_context_name, buffer_definition_name, connection_capacity, source_context, source_context_port ):
        if int(connection_capacity) <= 0:
            raise ValueError("Connection capacity must be >= 1 ")
        if not native_context_name in self.native_contexts:
            raise SyntaxError("Network native context '" + native_context_name + "' is undefined")
        if buffer_definition_name and not buffer_definition_name in self.buffer_definitions:
            raise SyntaxError("Bufer definition reference '" + buffer_definition_name + "' is undefined")

        output_port = OutputPort( source_context, source_context_port )
        if not source_context in self.contexts:
            raise SyntaxError("Port '" + str(output_port) + "' references undefined context '" + source_context + "'")

        nio_key = self.native_io_key( native_context_name, output_port )
        if nio_key in self.native_outputs:
            raise SyntaxError("Native context '" + native_context_name + "' is already connected to a port")

        native_context = self.native_contexts[ native_context_name ]
        self.native_outputs[nio_key] = NativeInputOutput( native_context, buffer_definition_name, connection_capacity, output_port )

    def add_native_to_native_connection( self, source_native_context_name, source_buffer_definition_name, connection_capacity, sink_native_context_name, sink_buffer_definition_name ):
        if int(connection_capacity) <= 0:
            raise ValueError("Connection capacity must be >= 1 ")
        if not source_native_context_name in self.native_contexts:
            raise SyntaxError("Network native context '" + source_native_context_name + "' is undefined")
        if not sink_native_context_name in self.native_contexts:
            raise SyntaxError("Network native context '" + sink_native_context_name + "' is undefined")
        if source_buffer_definition_name and not source_buffer_definition_name in self.buffer_definitions:
            raise SyntaxError("Bufer definition reference '" + source_buffer_definition_name + "' is undefined")
        if sink_buffer_definition_name and not sink_buffer_definition_name in self.buffer_definitions:
            raise SyntaxError("Bufer definition reference '" + sink_buffer_definition_name + "' is undefined")

        n2n_connection = N2NConnection( source_native_context_name, source_buffer_definition_name, connection_capacity, sink_native_context_name, sink_buffer_definition_name )
        self.n2n_connections.append( n2n_connection )

    def add_buffer_definition(self, buffer_definition):
        if buffer_definition.name in self.buffer_definitions:
            raise SyntaxError("Buffer with name '" + buffer_definition.name + "' is already defined")

        self.buffer_definitions[ buffer_definition.name ] = buffer_definition


#========================================================================
#
# Buffer definition
#
#========================================================================

class BufferDefinitionLine:
    def __init__(self, meta_source, data):
        self.meta_source = meta_source
        self.data = data

class BufferDefinition:
    def __init__(self, buffer_name, buffer_definition_data, is_external = False):
        self.name = buffer_name
        # [ [ 'src', [0,1,2,...] ], ... ]
        self.definition_data = buffer_definition_data
        self.is_external = is_external

    @staticmethod
    def parse_buffer_items(buffer_name, source):

        source.skip_empty_lines()

        buffer_items = []

        while not source.end_of_source():
            line = source.current_line()

            buffer_item = parse_cp_item(line)

            if not buffer_item:
                buffer_item = parse_cp_multiline_string(source)

            if not buffer_item:
                raise SyntaxError("expected buffer_item @ " + str(source.current_line_number()) + ", got '" + line.splitlines()[0] + "' instead." )

            meta_line, code, record_field_data = buffer_item
            buffer_items.append( BufferDefinitionLine( meta_line, code ) )

            source.next_line()
            source.skip_empty_lines()

        return BufferDefinition( buffer_name, buffer_items )


class BufferDefinitionParser:
    def __init__(self, source):
        self.source = source

    def parse(self):
        self.source.skip_empty_lines()

        line = self.source.current_line()
        if line is None:
            return None

        reg = re.match( reBufferStart, line, re.M|re.I)
        if not reg:
            return None

        buffer_name = reg.group( 1 )
        is_extern = reg.group( 2 )
        buffer_definition_data = []

        if is_extern:
            return BufferDefinition( buffer_name, buffer_definition_data, is_external=True )

        buffer_definition_source = self._collect_buffer_definition_source()

        return BufferDefinition.parse_buffer_items( buffer_name, buffer_definition_source )


    def _collect_buffer_definition_source(self):

        buffer_start_line_number = self.source.current_line_number()

        self.source.next_line()
        start_line_number = self.source.current_line_number()

        buffer_source_lines = []

        got_buffer_end_line = False

        while not self.source.end_of_source():
            line = self.source.current_line()

            # check for end of buffer definition mark
            reg = re.match( reBufferEnd, line, re.I | re.M )
            if reg:
                got_buffer_end_line = True
                break

            buffer_source_lines.append( line )

            self.source.next_line()

        if not got_buffer_end_line:
            raise SyntaxError('Missing end-of-buffer line for buffer definition starting @' + str(buffer_start_line_number) )

        return BPAsmSource( buffer_source_lines, start_line_number )


#========================================================================
#
# Network source line parser
#
#========================================================================

def parse_network_element( source, network ):

    buffer_definition = BufferDefinitionParser( source ).parse()
    if buffer_definition:
        network.add_buffer_definition( buffer_definition )
        return

    line = source.current_line()

    reg = re.match( reContext, line, re.I | re.M )
    if reg:
        # context definition
        context_name = reg.group( 1 )
        component_package = reg.group( 2 )
        component_file_name = reg.group( 3 )

        network.add_context( context_name, component_package, component_file_name )
        return

    reg = re.match( reNativeFunctionContext, line, re.I | re.M )
    if reg:
        # native function definition
        native_context_name = reg.group( 1 )
        native_function_package = reg.group( 2 )
        native_function_file_name = reg.group( 3 )

        network.add_native_context( native_context_name, native_function_package, native_function_file_name )
        return

    reg = re.match( reConnection, line, re.I | re.M )
    if reg:
        # connection definition
        source_context = reg.group( 1 )
        source_context_port = reg.group( 2 ) or '0'
        connection_capacity = reg.group( 3 ) or '1'
        sink_context_port = reg.group( 4 ) or '0'
        sink_context = reg.group( 5 )

        network.add_connection( source_context, source_context_port, sink_context, sink_context_port, connection_capacity )
        return

    reg = re.match( reNetworkInput, line, re.I | re.M )
    if reg:
        # network input definition
        network_input_name = reg.group( 1 )
        sink_context_port = reg.group( 2 ) or '0'
        sink_context = reg.group( 3 )

        network.add_input( network_input_name, sink_context, sink_context_port )
        return

    reg = re.match( reNetworkOutput, line, re.I | re.M )
    if reg:
        # network output definition
        source_context = reg.group( 1 )
        source_context_port = reg.group( 2 ) or '0'
        network_output_name = reg.group( 3 )

        network.add_output( network_output_name, source_context, source_context_port )
        return

    reg = re.match( reNetworkNativeInput, line, re.I | re.M )
    if reg:
        # network native input definition
        native_context_name = reg.group( 1 )
        buffer_definition_name = reg.group( 2 ) or ''
        connection_capacity = reg.group( 3 ) or '1'
        sink_context_port = reg.group( 4 ) or '0'
        sink_context = reg.group( 5 )

        network.add_native_input( native_context_name, buffer_definition_name, connection_capacity, sink_context, sink_context_port )
        return

    reg = re.match( reNetworkNativeOutput, line, re.I | re.M )
    if reg:
        # network native output definition
        source_context = reg.group( 1 )
        source_context_port = reg.group( 2 ) or '0'
        connection_capacity = reg.group( 3 ) or '1'
        native_context_name = reg.group( 4 )
        buffer_definition_name = reg.group( 5 ) or ''

        network.add_native_output( native_context_name, buffer_definition_name, connection_capacity, source_context, source_context_port )
        return

    reg = re.match( reNativeToNativeConnection, line, re.I | re.M )
    if reg:
        # native to native connection definition
        source_native_context_name = reg.group( 1 )
        source_buffer_definition_name = reg.group( 2 ) or ''
        connection_capacity = reg.group( 3 ) or '1'
        sink_native_context_name = reg.group( 4 )
        sink_buffer_definition_name = reg.group( 5 ) or ''

        network.add_native_to_native_connection( source_native_context_name, source_buffer_definition_name, connection_capacity, sink_native_context_name, sink_buffer_definition_name )
        return

    raise SyntaxError("Couldn't parse line '" + line.strip() + "'")


#========================================================================
#
# Parse network source
#
#========================================================================

def parse_network( source ):

    network = Network()

    source.skip_empty_lines()

    while not source.end_of_source():

        parse_network_element( source, network )

        source.next_line()
        source.skip_empty_lines()
    
    return network
