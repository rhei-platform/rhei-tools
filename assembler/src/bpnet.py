# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import re
import sys
import os

import argparse

from shutil import copyfile

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmccode import generate_c_code
from bpasmlib.bpnetccode import *
from bpasmlib.bpnetparsers import parse_network

from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile


parser = argparse.ArgumentParser(description='BPNet compiler.')
parser.add_argument('file_name', type=str, help='network source file to compile')
parser.add_argument('-cp', '--component-path', type=str, help='path to folder containing component definitions')
parser.add_argument('-of', '--output-folder', type=str, help='path to folder that all output files will be placed to')
parser.add_argument('-mn', '--main-file', type=str, help='path to C source file that contains main() function')
parser.add_argument('--simple-cli', action='store_true', help='build CLI app using simple busy loop main function')

args = parser.parse_args()

component_folder_path = args.component_path if args.component_path else "./"
component_folder_path = os.path.normpath( component_folder_path )
output_folder_path = args.output_folder if args.output_folder else "./"
output_folder_path = os.path.normpath( output_folder_path )
c_main_file_name = args.main_file if args.main_file else None
build_simple_cli = args.simple_cli
bpnet_file_name = args.file_name

if not build_simple_cli and not c_main_file_name:
    print('Pleas specify main file using -mn option or use --simple-cli')
    exit(1)

# TODO: make these arguments
vm_source_folder = '../../rhei-vm'
vm_lib_folder = '../../rhei-vm/build'


bpnet_file_name = os.path.normpath( bpnet_file_name )
if not os.path.isfile( bpnet_file_name ):
    print( "file: '", bpnet_file_name, "' is not a file" )
    sys.exit(1)

network_file_prefix = os.path.splitext( os.path.basename( bpnet_file_name ) )[0]
network_file_location = os.path.dirname( bpnet_file_name )

network_main_runner_c_file = "%s_main.c"%network_file_prefix
if c_main_file_name:
    network_main_runner_c_file = c_main_file_name

network_c_source_file = "%s_network.c"%network_file_prefix

app_name = os.path.splitext( network_main_runner_c_file )[0]

source = BPAsmSourceFile(bpnet_file_name)

#========================================================================
#
# Parse network definition source
#
#========================================================================

network = parse_network( source )


#========================================================================
#
# Print network information
#
#========================================================================

print( "---------------------------------------------------------" )
print( "Network info:" )
print( "---------------------------------------------------------" )

for ctx in network.contexts.values():
    print( "context:", ctx )
    print( "component file:", os.path.join( component_folder_path, ctx.package, ctx.file_name ) )

for ctx in network.native_contexts.values():
    print( "native context:", ctx )
    print( "native function definition file:", os.path.join( component_folder_path, ctx.package, ctx.file_name ) )

for inp in network.inputs.keys():
    print( "input:", inp, "->", network.inputs[inp] )

for out in network.outputs.keys():
    print( "output:", network.outputs[out], "->", out )

for inp in network.native_inputs.keys():
    print( "native input:", network.native_inputs[inp] )

for out in network.native_outputs.keys():
    print( "native output:", network.native_outputs[out] )

for conn in network.connections:
    print( "connection:", conn )

print( "---------------------------------------------------------" )
print( "" )



if not os.path.exists( output_folder_path ):
    os.makedirs( output_folder_path )


generated_c_source_files = []
generatet_h_source_files = []

#========================================================================
#
# Compile components to C source code
#
#========================================================================

print( "Compilling components..." )

compiled_components = {}

for ctx in network.contexts.values():
    component_file = ctx.file_location()
    if not component_file in compiled_components:
        print( "component: ", component_file )
        component_file_path = os.path.normpath( os.path.join( component_folder_path, component_file ) )

        source = BPAsmSourceFile( component_file_path )
        component = parse_component( source )
        generated_fn_name, generated_source = generate_c_code( component )

        compiled_components[ component_file ] = ( component, generated_fn_name, generated_source )

print( "Writing C-source to output..." )

for component_file in compiled_components.keys():
    component, _, generated_source = compiled_components[ component_file ]
    component_header_name = get_component_header_name(component)
    output_file_name = os.path.join( output_folder_path, component_header_name )
    print( "component: ", component.name, "->", output_file_name )
    with open( output_file_name, 'w' ) as header_file:
        header_file.write( generated_source )
    generatet_h_source_files.append( component_header_name )

print( "" )


#========================================================================
#
# Collect any native function source files
#
#========================================================================

print( "Collecting native function source files..." )

for ctx in network.native_contexts.values():
    context_file = ctx.file_location()
    print( "Copying: ", context_file )
    source_file_name = os.path.join( component_folder_path, context_file )
    native_functions_c_file = ctx.file_name
    output_file_name = os.path.join( output_folder_path, native_functions_c_file )
    print( source_file_name, '->', output_file_name )
    copyfile( source_file_name, output_file_name )
    generated_c_source_files.append( native_functions_c_file )

print( "" )


#========================================================================
#
# Compile network to C source code
#
#========================================================================

print( "Compilling network..." )

generated_source = generate_network_c_code( network, compiled_components )

print( "Writing C-source to output..." )

output_file_name = os.path.join( output_folder_path, network_c_source_file )
print( output_file_name )
with open( output_file_name, 'w' ) as source_file:
    source_file.write( "\n".join(generated_source) )
generated_c_source_files.append( network_c_source_file )

print( "" )


#========================================================================
#
# Copy/create network main runner file
#
#========================================================================

from bptools.bpnettools import BuildFileGenerator, write_simple_cli_runner

print( "Copying network main runner file..." )

# TODO replace network_file_location with network_main_runner_c_file_location or something
output_file_name = os.path.join( output_folder_path, network_main_runner_c_file )
if build_simple_cli:
    print( 'Writing', output_file_name )
    write_simple_cli_runner( output_file_name )
else:
    source_file_name = os.path.join( network_file_location, network_main_runner_c_file )
    print( source_file_name, '->', output_file_name )
    copyfile( source_file_name, output_file_name )

generated_c_source_files.append( network_main_runner_c_file )

print( "" )


#========================================================================
#
# Generate build files to compile and link the network runner code
#
#========================================================================

print( "Generating build files..." )
bfg = BuildFileGenerator( output_folder_path, generated_c_source_files, network_file_prefix, app_name, vm_source_folder, vm_lib_folder )
bfg.generate_makefile()
bfg.generate_scons()

print( "Done." )
