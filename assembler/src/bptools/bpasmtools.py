# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import struct

# typedef enum _FBVMInstruction {

_FBVMInstruction = [
    'OP_NOP',
    'OP_INVALID',

    'OP_END',
    'OP_TERMINATE',

    'OP_SWITCH',
    'OP_JUMP',

    'OP_JUMP_IF_FALSE',
    'OP_JUMP_IF_LESS_EQUAL',
    'OP_JUMP_IF_GREATER_EQUAL',
    'OP_JUMP_IF_LESS',
    'OP_JUMP_IF_GREATER',
    'OP_JUMP_IF_EQUAL',
    'OP_JUMP_IF_NOT_EQUAL',
   
    'OP_JUMP_IF_ZERO_2',

    'OP_BCONST_I8',
    'OP_BCONST_I16',
    'OP_BCONST_I32',
    'OP_BCONST_I64',

    'OP_SCONST_I8',
    'OP_SCONST_I16',
    'OP_SCONST_I32',
    'OP_SCONST_I64',

    'OP_PCONST_I8',
    'OP_PCONST_I16',
    'OP_PCONST_I32',
    'OP_PCONST_I64',

    'OP_CONST_0',
    'OP_CONST_1',
    'OP_CONST_M1',

    'OP_BPUSH',
    'OP_SPUSH',

    'OP_DUP',
    'OP_DUP_2',

    'OP_CLOAD_I8',
    'OP_CLOAD_I16',
    'OP_CLOAD_I32',
    'OP_CLOAD_I64',

    'OP_LOAD_I8',
    'OP_LOAD_I16',
    'OP_LOAD_I32',
    'OP_LOAD_I64',

    'OP_BLOAD_I8',
    'OP_BLOAD_I16',
    'OP_BLOAD_I32',
    'OP_BLOAD_I64',

    'OP_CSTORE_I8',
    'OP_CSTORE_I16',
    'OP_CSTORE_I32',
    'OP_CSTORE_I64',

    'OP_STORE_I8',
    'OP_STORE_I16',
    'OP_STORE_I32',
    'OP_STORE_I64',

    'OP_BSTORE_I8',
    'OP_BSTORE_I16',
    'OP_BSTORE_I32',
    'OP_BSTORE_I64',

    'OP_NEG_I',
    'OP_NEG_L',
    'OP_NEG_F',
    'OP_NEG_D',

    'OP_NOT',

    'OP_BIT_INVERT_I',
    'OP_BIT_INVERT_L',

    'OP_MUL_I',
    'OP_MUL_L',
    'OP_MUL_F',
    'OP_MUL_D',

    'OP_DIV_I',
    'OP_DIV_L',
    'OP_DIV_F',
    'OP_DIV_D',

    'OP_MOD_I',
    'OP_MOD_L',

    'OP_ADD_I',
    'OP_ADD_L',
    'OP_ADD_F',
    'OP_ADD_D',

    'OP_SUB_I',
    'OP_SUB_L',
    'OP_SUB_F',
    'OP_SUB_D',

    'OP_SHIFT_LEFT_I',
    'OP_SHIFT_LEFT_L',
    'OP_SHIFT_RIGHT_I',
    'OP_SHIFT_RIGHT_L',
    'OP_BIT_AND_I',
    'OP_BIT_AND_L',
    'OP_BIT_XOR_I',
    'OP_BIT_XOR_L',
    'OP_BIT_OR_I',
    'OP_BIT_OR_L',

    'OP_CMP_I',
    'OP_CMP_UI',
    'OP_CMP_L',
    'OP_CMP_UL',
    'OP_CMP_F',
    'OP_CMP_D',

    'OP_AND',
    'OP_OR',

    'OP_BREQUEST_READ',
    'OP_REQUEST_READ',
    'OP_BREQUEST_WRITE',
    'OP_REQUEST_WRITE',
    'OP_CREATE_IP',
    'OP_SCREATE_IP',
    'OP_DROP_IP',

    'OP_YIELD',

    'OP_IP_LENGTH',
    'OP_IP_COPY',
]
# } FBVMInstruction;

FBVMInstruction = dict( zip( _FBVMInstruction, range( 0, len(_FBVMInstruction) ) ) )


def op_to_int_array( op_array ):
    return [ FBVMInstruction[ op_array[0] ] ] + list( map( int, op_array[1:] ) )

magic = "Rhei"
major_version = 1
minor_version = 0


# https://docs.python.org/3/library/struct.html#format-characters

def generate_binary( component ):

    # component.name
    component_name = bytes( component.name, 'ascii' )
    component_name_size = len( component.name )
    # print( 'component.name', component_name_size, component_name )

    # component.inputs
    input_count = int( component.inputs )
    # print( 'component.inputs', component.inputs )

    # component.outputs
    output_count = int( component.outputs )
    # print( 'component.outputs', component.outputs )

    # component.stack
    component_stack_size = int( component.stack )
    # print( 'component.stack', component.stack )

    # component.packets
    information_packet_count = int( component.packets )
    # print( 'component.packets', component.packets )

    # component.data
    component_data_size = component.data.size()
    # print( 'component.data', component.data.size() )

    # component.constant_pool_data
    # print( 'component.constant_pool_data' )
    cpdata = []
    for cpd in component.constant_pool_data:
        _, data = cpd
        cpdata += data
    constant_pool_size = len( cpdata )
    # print( constant_pool_size )
    # print( cpdata )

    # component.setup_code
    # print( 'component.setup_code' )
    setupcode = []
    for op in component.setup_code:
        _, data, _ = op
        setupcode += op_to_int_array( data )
    setup_code_size = len( setupcode )
    # print( setup_code_size )
    # print( setupcode )

    # component.loop_code
    # print( 'component.loop_code' )
    loopcode = []
    for op in component.loop_code:
        _, data, _ = op
        loopcode += op_to_int_array( data )
    loop_code_size = len( loopcode )
    # print( loop_code_size )
    # print( loopcode )


#     // payload[] - right after component_name_size
    payload = []

#     // @0
    name_offset = 0
#     u1      component_name[ component_name_size ];
    payload += list( component_name )

#     // @constant_pool_offset
    constant_pool_offset = name_offset + component_name_size
#     u1      constant_pool[ constant_pool_size ];
    payload += cpdata

#     // @setup_code_offset
    setup_code_offset = constant_pool_offset + constant_pool_size
#     u1      setup_code[ setup_code_size ];
    payload += setupcode

#     // @loop_code_offset
    loop_code_offset = setup_code_offset + setup_code_size
#     u1      loop_code[ loop_code_size ];
    payload += loopcode

#     // @(loop_code_offset + loop_code_size)
    # debug_data_offset = loop_code_offset + loop_code_size
#     debug_info  debug_data;
    # payload += debug_data

# Binary {
#     u4      magic;
#     u2      major_version;
#     u2      minor_version;

    bin = bytearray( bytes( magic, 'ascii' ) )
    bin.extend( struct.pack( '<H', major_version ) )
    bin.extend( struct.pack( '<H', minor_version ) )

#     u4      constant_pool_size;
#     u4      constant_pool_offset;

    bin.extend( struct.pack( '<I', constant_pool_size ) )
    bin.extend( struct.pack( '<I', constant_pool_offset ) )

#     u2      setup_code_size;
#     u2      setup_code_offset;
#     u2      loop_code_size;
#     u2      loop_code_offset;

    bin.extend( struct.pack( '<H', setup_code_size ) )
    bin.extend( struct.pack( '<H', setup_code_offset ) )
    bin.extend( struct.pack( '<H', loop_code_size ) )
    bin.extend( struct.pack( '<H', loop_code_offset ) )

#     u4      component_data_size;
#     u4      component_stack_size;
#     u2      information_packet_count;
#     u2      input_count;
#     u2      output_count;

    bin.extend( struct.pack( '<I', component_data_size ) )
    bin.extend( struct.pack( '<I', component_stack_size ) )
    bin.extend( struct.pack( '<H', information_packet_count ) )
    bin.extend( struct.pack( '<H', input_count ) )
    bin.extend( struct.pack( '<H', output_count ) )

#     u2      component_name_size;

    bin.extend( struct.pack( '<H', component_name_size ) )

# }

    bin.extend( bytes( payload ) )

    return bin
