# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import os

from string import Template

#========================================================================
#
# Makefile to build app
#
#========================================================================

_makefile_template = """
######################################################
# Automatically-generated file
######################################################

RM := rm -rf

VM_SRC_PATH := $vm_source_root/src
VM_LIB_PATH := $vm_lib_root

VM_INCLUDES := \\
-I$(VM_SRC_PATH) \\
-I$(VM_SRC_PATH)/core \\
-I$(VM_SRC_PATH)/debug \\
-I$(VM_SRC_PATH)/interpreter \\
-I$(VM_SRC_PATH)/lib \\
-I$(VM_SRC_PATH)/meta \\
-I$(VM_SRC_PATH)/util \\
-I$(VM_SRC_PATH)/contrib \\
-I. \\


VM_LIBS := \\
-lbpsvm \\


SYSTEM_LIBS := \\
-lm \\


TARGET := $app_name
OBJS := \\
$objs


all: $(TARGET)

# Build executable
$(TARGET): $(OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC Linker'
	gcc -o $@ $(OBJS) $(VM_LIBS) $(SYSTEM_LIBS) -L$(VM_LIB_PATH)
	@echo 'Finished building target: $@'
	@echo ' '
	$(MAKE) --no-print-directory -f $makefile_file_name post-build

%.o: %.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -c "$<" -o "$@" $(VM_INCLUDES)
	@echo 'Finished building: $<'
	@echo ' '

# Other Targets
clean:
	-$(RM) $(OBJS) $(TARGET)
	-@echo ' '

post-build:
	-@echo 'Performing post-build steps'
	-@echo ' '

.PHONY: all clean dependents
.SECONDARY: post-build

-include ../makefile.targets
"""

#========================================================================
#
# Sconstruct file to build app
#
#========================================================================

_scons_template = """
######################################################
# Automatically-generated file
######################################################

######################################################
#
# VM stuff
#
######################################################
vm_source_root = '$vm_source_root'
vm_include_root = vm_source_root + '/src'
vm_lib_root = '$vm_lib_root'
vm_library_name = 'bpsvm'
system_libs = [
    'm',
]

vm_include_paths = [
    Dir(vm_include_root+''),
    Dir(vm_include_root+'/core'),
    Dir(vm_include_root+'/debug'),
    Dir(vm_include_root+'/interpreter'),
    Dir(vm_include_root+'/lib'),
    Dir(vm_include_root+'/meta'),
    Dir(vm_include_root+'/util'),

    Dir(vm_include_root+'/contrib'),
]

######################################################
#
# App stuff
#
######################################################
app_sources = [
    $sources
]

app_target_name = '$app_name'

######################################################
#
# Build
#
######################################################
env = Environment()

env.Program(
    app_target_name,
    app_sources,
    CPPPATH = vm_include_paths,
    LIBS=[ vm_library_name ] + system_libs,
    LIBPATH=vm_lib_root
)
"""

#========================================================================
#
# Simple CLI VM runner source
#
#========================================================================

_simple_cli_vm_runner = """
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/scheduler.h"
#include "vm/debug/vm_debug.h"


extern FBNetworkContext *create_network();

int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    FBNetworkContext *networkContext = create_network();


    //---------------------------------------------------------------
    // Run the network
    //---------------------------------------------------------------
    do {
        // DEBUG INFO
        // uncomment this line to see network context state printouts
        // for each iteration
        // fbDebug_printNetworkContextState( networkContext );
    }
    while( fbScheduler_mainLoop( networkContext ) );

    fbNetwork_destroy( networkContext );

    return 0;
}
"""

#========================================================================
#
# Utility functions
#
#========================================================================

def write_simple_cli_runner( file_path ):
    with open( file_path, 'w' ) as source_file:
        source_file.write( _simple_cli_vm_runner )


#========================================================================
#
# The generator
#
#========================================================================

class BuildFileGenerator:
    def __init__(self, output_folder_path, generated_c_source_files, network_file_prefix, app_name, vm_source_folder, vm_lib_folder):
        self.output_folder_path = output_folder_path
        self.generated_c_source_files = list(set(generated_c_source_files)) # remove duplicates
        self.network_file_prefix = network_file_prefix
        self.app_name = app_name
        self.vm_source_folder = vm_source_folder
        self.vm_lib_folder = vm_lib_folder

    def generate_makefile( self ):

        app_objs = []
        for c_source in self.generated_c_source_files:
            basename = os.path.splitext( c_source )[0]
            app_objs.append( basename + '.o' )

        makefile_file_name = "%s.Makefile"%self.network_file_prefix

        template_values = {
            'vm_source_root' : self.vm_source_folder,
            'vm_lib_root' : self.vm_lib_folder,
            'app_name' : self.app_name,
            'objs' : ' \\\n    '.join(app_objs),
            'makefile_file_name' : makefile_file_name
        }
        scons_source = Template( _makefile_template ).safe_substitute( template_values )

        output_build_file_name = os.path.join( self.output_folder_path, makefile_file_name )
        print( output_build_file_name )
        with open( output_build_file_name, 'w' ) as build_file:
            build_file.write( scons_source )


    def generate_scons( self ):

        app_sources = []
        for c_source in self.generated_c_source_files:
            app_sources.append( '"' + c_source + '"' )

        template_values = {
            'vm_source_root' : self.vm_source_folder,
            'vm_lib_root' : self.vm_lib_folder,
            'app_name' : self.app_name,
            'sources' : ',\n    '.join(app_sources)
        }
        scons_source = Template( _scons_template ).safe_substitute( template_values )

        output_build_file_name = os.path.join( self.output_folder_path, "%s.SConstruct"%self.network_file_prefix )
        print( output_build_file_name )
        with open( output_build_file_name, 'w' ) as build_file:
            build_file.write( scons_source )
