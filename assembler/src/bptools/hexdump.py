#!/usr/bin/python
#
# HEX DUMP PYTHON SCRIPT - RUBY DEVICES 2017
#
# Usage:
# hexdump.py <file_to_dump>
#
import sys
import os.path
import binascii

def _check_file_provided():
  # This method ensures a valid file was provided to the invoked script ##
  if (len(sys.argv) < 2):
    print( "" )
    print( "Error - No file was provided" )
    print( "" )
    print( "Correct Usage:" )
    print( "python hexdump.py <file_to_dump>" )
    print( "" )
    sys.exit(0)
  if not os.path.isfile(sys.argv[1]):
    print( "" )
    print( "Error - The file provided does not exist" )
    print( "" )
    sys.exit(0)
  
def _read_bytes(filename, chunksize=8192):
  # This method returns the bytes of a provided file ##
  try:
    with open(filename, "rb") as f:
      while True:
        chunk = f.read(chunksize)
        if chunk:
          for b in chunk:
            yield b
        else:
          break
  except IOError:
    print( "" )
    print( "Error - The file provided does not exist" )
    print( "" )
    sys.exit(0)
        
def _is_character_printable(s):
  ## This method returns true if a byte is a printable ascii character ##
  return (s >= 32) and (s < 127)
#   return all((ord(c) < 127) and (ord(c) >= 32) for c in s)

def print_hexdump_header():
  print( "Offset 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F" )
  print( "" )

def _print_headers():
  ## This method prints the headers at the top of our hex dump ##
  print( "" )
  print( "#### HEX DUMP PYTHON SCRIPT - RUBY DEVICES 2017 ####" )
  print( "" )
  print_hexdump_header()

def hexdump( sequence ):
    memory_address = 0
    ascii_string = ""

    ## Loop through the given sequence while printing the address, hex and ascii output ##
    for byte in sequence:
        ascii_string = ascii_string + _validate_byte_as_printable(byte)
        if memory_address%16 == 0:
            print( format(memory_address, '06X'), end=' ' )
            # print(byte.encode('hex')),
            print( format(byte, '02X'), end=' ' )
        elif memory_address%16 == 15:
            # print(byte.encode('hex')),
            print( format(byte, '02X'), end=' ' )
            print( ascii_string )
            ascii_string = ""
        else:
            # print(byte.encode('hex')),
            print( format(byte, '02X'), end=' ' )
        memory_address = memory_address + 1
    
    print( "" )


def _validate_byte_as_printable(byte):
  ## Check if byte is a printable ascii character. If not replace with a '.' character ##
  if _is_character_printable(byte):
    return chr(byte)
  else:
    return '.'

if __name__ == "__main__":
    ## main ##
    _check_file_provided()
    _print_headers()

    ## Loop through the given file while printing the address, hex and ascii output ##
    hexdump( _read_bytes(sys.argv[1]) )
