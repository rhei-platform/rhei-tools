## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import os

_CFG_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )

TEST_COMPONENTS_SOURCES = os.path.normpath( os.path.join( _CFG_LOCATION, "sources/components" ) )
TEST_NETWORKS_SOURCES = os.path.normpath( os.path.join( _CFG_LOCATION, "sources/networks" ) )
