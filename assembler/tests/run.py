## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import sys
import os

_FILE_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )
_BPASM_LOCATION = os.path.normpath( os.path.join( _FILE_LOCATION, "../src" ) )
# _BPASM_LOCATION = os.path.normpath( os.path.join( _FILE_LOCATION, "../src/bpasmlib" ) )
sys.path.append( _BPASM_LOCATION )

# pip install pytest

import unittest

# import all test modules
import units.testAsmBasics
import units.testAsmMetaData
import units.testAsmOps
import units.testAsmJumpLabels
import units.testAsmRecordOffsets
import units.testAsmConstantPoolData
import units.testAsmDataSection
import units.testNetBasics
import units.testNetComments
import units.testNetContexts
import units.testNetInputsOutputs
import units.testNetConnections
import units.testNetBufferDefinition


# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# add tests from modules to the test suite
suite.addTests( loader.loadTestsFromModule( units.testAsmBasics ) )
suite.addTests( loader.loadTestsFromModule( units.testAsmMetaData ) )
suite.addTests( loader.loadTestsFromModule( units.testAsmOps ) )
suite.addTests( loader.loadTestsFromModule( units.testAsmJumpLabels ) )
suite.addTests( loader.loadTestsFromModule( units.testAsmRecordOffsets ) )
suite.addTests( loader.loadTestsFromModule( units.testAsmConstantPoolData ) )
suite.addTests( loader.loadTestsFromModule( units.testAsmDataSection ) )
suite.addTests( loader.loadTestsFromModule( units.testNetBasics ) )
suite.addTests( loader.loadTestsFromModule( units.testNetComments ) )
suite.addTests( loader.loadTestsFromModule( units.testNetContexts ) )
suite.addTests( loader.loadTestsFromModule( units.testNetInputsOutputs ) )
suite.addTests( loader.loadTestsFromModule( units.testNetConnections ) )
suite.addTests( loader.loadTestsFromModule( units.testNetBufferDefinition ) )

# initialize a runner and run our test suite with it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )
