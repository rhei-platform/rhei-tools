#include "vm/util/network_builder.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/debug/vm_debug.h"

#include "vm/native.h"

#include <stdio.h>


extern uint8_t value_A[];
extern uint8_t value_B[];
extern uint8_t buffer_one[];
extern int32_t fnOutputValue;

extern FBNetworkContext *create_network();

int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    printf("BPScript VM sample: add buffers\n");
    printf("OUT = A + B\n");

    FBNetworkContext *networkContext = create_network();


    //---------------------------------------------------------------
    // Run the network
    //---------------------------------------------------------------
    while( fbScheduler_mainLoop( networkContext ) ) {
        // the network will run until component terminates
        // by reaching OP_TERMINATE opcode at which point
        // scheduler will terminate both input connections
        // and close all ports on them and then close
        // component's output port.
        // output connection with result in it will still
        // be alive and our portResult will still be open

        // DEBUG INFO
        // fbDebug_printNetworkContextState( networkContext );
    }

    fbNetwork_destroy( networkContext );

    printf("A = %d\n", fbData_get_int32( value_A ));
    printf("B = %d\n", fbData_get_int32( value_B ));
    printf("fnOutputValue = %d\n", fnOutputValue);

    return 0;
}
