## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile


class TestSmoke(unittest.TestCase):

    def setUp(self):
        pass

    def test_allops(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_COMPONENTS_SOURCES, "allops.bpasm" ) )
        source = BPAsmSourceFile( source_file )

        component = parse_component(source)

        self.assertIsNotNone( component )

    def test_constant_pool(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_COMPONENTS_SOURCES, "constant_pool.bpasm" ) )
        source = BPAsmSourceFile( source_file )

        component = parse_component(source)

        self.assertIsNotNone( component )

    def test_jumps(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_COMPONENTS_SOURCES, "jumps.bpasm" ) )
        source = BPAsmSourceFile( source_file )

        component = parse_component(source)

        self.assertIsNotNone( component )

    def test_labels_and_jumps(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_COMPONENTS_SOURCES, "labels_and_jumps.bpasm" ) )
        source = BPAsmSourceFile( source_file )

        component = parse_component(source)

        self.assertIsNotNone( component )

    def test_offsets(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_COMPONENTS_SOURCES, "offsets.bpasm" ) )
        source = BPAsmSourceFile( source_file )

        component = parse_component(source)

        self.assertIsNotNone( component )

    def tearDown(self):
        pass


class TestSectionsOptinalRerquired(unittest.TestCase):

    def setUp(self):
        pass

    def test_optional_record(self):
        source = BPAsmSource([
            # "record: SampleRecord",
            # "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])
        component = parse_component(source)
        self.assertIsNotNone( component )

    def test_optional_doc(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            # "doc:",
            # "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])
        component = parse_component(source)
        self.assertIsNotNone( component )

    def test_required_name(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            # "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_inputs(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            # "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_outputs(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            # "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_stack(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            # "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_packets(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            # "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_data(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            # "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_constant_pool(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            # "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_setup(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            # "setup:",
            # "   end",
            "loop:",
            "   end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_loop(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            # "loop:",
            # "   end",
            "# the last (essentially empty) line",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_end_of_source_handling(self):

        #
        # for parse_loop_code()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_setup_code()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_constant_pool()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_data_section()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_packets()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_stack()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_outputs()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_inputs()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_doc()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_name()
        #
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_record()
        #
        source = BPAsmSource([
            ""
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

        #
        # for parse_record(), when empty file
        #
        source = BPAsmSource([
        ])
        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_setup_code(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            # "   end",
            "loop:",
            "   end",
            "# the last (essentially empty) line",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def test_required_loop_code(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "name: test",
            "doc:",
            "   doc line 1",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            # "   end",
            "# the last (essentially empty) line",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )

    def tearDown(self):
        pass

