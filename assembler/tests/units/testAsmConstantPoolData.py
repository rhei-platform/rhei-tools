## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

from . import tools

class TestConstantPoolData(unittest.TestCase):

    def setUp(self):
        pass


    def test_constant_pool(self):
        source = BPAsmSource([
            "name: constant_pool_sample",
            "inputs: 1",
            "outputs: 1",
            "stack: 1",
            "packets: 1",
            "data:",
            "constant_pool:",
            "    i8 55",
            "    i8[3] -55",
            "    i8[3] 55, 45",
            "    i8[3] 55, 45, 66",
            "    i8 length8 120",
            "    i8[2] values8 120",
            "",
            "    i16 55",
            "    i16[3] -55",
            "    i16[3] 55, 45",
            "    i16[3] 55, 45, 66",
            "    i16 length16 128",
            "    i16[2] values16 128",
            "",
            "    i32 55",
            "    i32[3] -55",
            "    i32[3] 55, 45",
            "    i32[3] 55, 45, 66",
            "    i32 length32 128",
            "    i32[2] values32 128",
            "",
            "    i64 55",
            "    i64[3] -55",
            "    i64[3] 55, 45",
            "    i64[3] 55, 45, 66",
            "    i64 length64 128",
            "    i64[2] values64 128",
            "",
            "    f 55.2",
            "    f[3] 55.2",
            "    f[3] 55.2, 45.65",
            "    f[3] 55.2, 45.65, 66",
            "    f lengthf 128.33",
            "    f[2] valuesf 128.33",
            "",
            "    ps \"sample string\"",
            "    ps named \"sample string with an identifier",
            "    cs \"null terminated string",
            "    cs \"\"quoted' string",
            "    cs \"escape sequence chars \\a\\v\\b\\f\\t\\n\\r string",
            "    cs '''",
            "multi-line",
            "    string",
            "        literal",
            "'''",
            "",
            "setup:",
            "    end",
            "loop:",
            "    end",
        ])
        component = parse_component(source)


        # i8 55
        _, cp_data = component.constant_pool_data[ 0 ]
        self.assertEqual( 1, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data ) )

        # i8[3] -55
        _, cp_data = component.constant_pool_data[ 1 ]
        self.assertEqual( 3, len( cp_data ) )
        self.assertEqual( -55, tools.signed_LE_value_from_byte_array( cp_data[0:1] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[1:2] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[2:3] ) )

        # i8[3] 55, 45
        _, cp_data = component.constant_pool_data[ 2 ]
        self.assertEqual( 3, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:1] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[1:2] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[2:3] ) )

        # i8[3] 55, 45, 66
        _, cp_data = component.constant_pool_data[ 3 ]
        self.assertEqual( 3, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:1] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[1:2] ) )
        self.assertEqual( 66, tools.signed_LE_value_from_byte_array( cp_data[2:3] ) )

        # i8 length16 120
        _, cp_data = component.constant_pool_data[ 4 ]
        self.assertEqual( 1, len( cp_data ) )
        self.assertEqual( 120, tools.signed_LE_value_from_byte_array( cp_data[0:1] ) )

        # i8[2] values16 120
        _, cp_data = component.constant_pool_data[ 5 ]
        self.assertEqual( 2, len( cp_data ) )
        self.assertEqual( 120, tools.signed_LE_value_from_byte_array( cp_data[0:1] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[1:2] ) )


        # i16 55
        _, cp_data = component.constant_pool_data[ 6 ]
        self.assertEqual( 2, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data ) )

        # i16[3] -55
        _, cp_data = component.constant_pool_data[ 7 ]
        self.assertEqual( 6, len( cp_data ) )
        self.assertEqual( -55, tools.signed_LE_value_from_byte_array( cp_data[0:2] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[2:4] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[4:6] ) )

        # i16[3] 55, 45
        _, cp_data = component.constant_pool_data[ 8 ]
        self.assertEqual( 6, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:2] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[2:4] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[4:6] ) )

        # i16[3] 55, 45, 66
        _, cp_data = component.constant_pool_data[ 9 ]
        self.assertEqual( 6, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:2] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[2:4] ) )
        self.assertEqual( 66, tools.signed_LE_value_from_byte_array( cp_data[4:6] ) )

        # i16 length16 128
        _, cp_data = component.constant_pool_data[ 10 ]
        self.assertEqual( 2, len( cp_data ) )
        self.assertEqual( 128, tools.signed_LE_value_from_byte_array( cp_data[0:2] ) )

        # i16[2] values16 128
        _, cp_data = component.constant_pool_data[ 11 ]
        self.assertEqual( 4, len( cp_data ) )
        self.assertEqual( 128, tools.signed_LE_value_from_byte_array( cp_data[0:2] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[2:4] ) )


        # i32 55
        _, cp_data = component.constant_pool_data[ 12 ]
        self.assertEqual( 4, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data ) )

        # i32[3] -55
        _, cp_data = component.constant_pool_data[ 13 ]
        self.assertEqual( 12, len( cp_data ) )
        self.assertEqual( -55, tools.signed_LE_value_from_byte_array( cp_data[0:4] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[4:8] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[8:12] ) )

        # i32[3] 55, 45
        _, cp_data = component.constant_pool_data[ 14 ]
        self.assertEqual( 12, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:4] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[4:8] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[8:12] ) )

        # i32[3] 55, 45, 66
        _, cp_data = component.constant_pool_data[ 15 ]
        self.assertEqual( 12, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:4] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[4:8] ) )
        self.assertEqual( 66, tools.signed_LE_value_from_byte_array( cp_data[8:12] ) )

        # i32 length32 128
        _, cp_data = component.constant_pool_data[ 16 ]
        self.assertEqual( 4, len( cp_data ) )
        self.assertEqual( 128, tools.signed_LE_value_from_byte_array( cp_data[0:4] ) )

        # i32[2] values32 128
        _, cp_data = component.constant_pool_data[ 17 ]
        self.assertEqual( 8, len( cp_data ) )
        self.assertEqual( 128, tools.signed_LE_value_from_byte_array( cp_data[0:4] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[4:8] ) )


        # i64 55
        _, cp_data = component.constant_pool_data[ 18 ]
        self.assertEqual( 8, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data ) )

        # i64[3] -55
        _, cp_data = component.constant_pool_data[ 19 ]
        self.assertEqual( 24, len( cp_data ) )
        self.assertEqual( -55, tools.signed_LE_value_from_byte_array( cp_data[0:8] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[8:16] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[16:24] ) )

        # i64[3] 55, 45
        _, cp_data = component.constant_pool_data[ 20 ]
        self.assertEqual( 24, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:8] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[8:16] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[16:24] ) )

        # i64[3] 55, 45, 66
        _, cp_data = component.constant_pool_data[ 21 ]
        self.assertEqual( 24, len( cp_data ) )
        self.assertEqual( 55, tools.signed_LE_value_from_byte_array( cp_data[0:8] ) )
        self.assertEqual( 45, tools.signed_LE_value_from_byte_array( cp_data[8:16] ) )
        self.assertEqual( 66, tools.signed_LE_value_from_byte_array( cp_data[16:24] ) )

        # i64 length64 128
        _, cp_data = component.constant_pool_data[ 22 ]
        self.assertEqual( 8, len( cp_data ) )
        self.assertEqual( 128, tools.signed_LE_value_from_byte_array( cp_data[0:8] ) )

        # i64[2] values64 128
        _, cp_data = component.constant_pool_data[ 23 ]
        self.assertEqual( 16, len( cp_data ) )
        self.assertEqual( 128, tools.signed_LE_value_from_byte_array( cp_data[0:8] ) )
        self.assertEqual( 0, tools.signed_LE_value_from_byte_array( cp_data[8:16] ) )


        # f 55.2
        _, cp_data = component.constant_pool_data[ 24 ]
        self.assertEqual( 4, len( cp_data ) )
        self.assertAlmostEqual( 55.2, tools.real_LE_value_from_byte_array( cp_data ), 5 )

        # f[3] 55.2
        _, cp_data = component.constant_pool_data[ 25 ]
        self.assertEqual( 12, len( cp_data ) )
        self.assertAlmostEqual( 55.2, tools.real_LE_value_from_byte_array( cp_data[0:4] ), 5 )
        self.assertAlmostEqual( 0, tools.real_LE_value_from_byte_array( cp_data[4:8] ), 5 )
        self.assertAlmostEqual( 0, tools.real_LE_value_from_byte_array( cp_data[8:12] ), 5 )

        # f[3] 55.2, 45.65
        _, cp_data = component.constant_pool_data[ 26 ]
        self.assertEqual( 12, len( cp_data ) )
        self.assertAlmostEqual( 55.2, tools.real_LE_value_from_byte_array( cp_data[0:4] ), 5 )
        self.assertAlmostEqual( 45.65, tools.real_LE_value_from_byte_array( cp_data[4:8] ), 5 )
        self.assertAlmostEqual( 0, tools.real_LE_value_from_byte_array( cp_data[8:12] ), 5 )

        # f[3] 55.2, 45.65, 66
        _, cp_data = component.constant_pool_data[ 27 ]
        self.assertEqual( 12, len( cp_data ) )
        self.assertAlmostEqual( 55.2, tools.real_LE_value_from_byte_array( cp_data[0:4] ), 5 )
        self.assertAlmostEqual( 45.65, tools.real_LE_value_from_byte_array( cp_data[4:8] ), 5 )
        self.assertAlmostEqual( 66.0, tools.real_LE_value_from_byte_array( cp_data[8:12] ), 5 )

        # f lengthf 128.33
        _, cp_data = component.constant_pool_data[ 28 ]
        self.assertEqual( 4, len( cp_data ) )
        self.assertAlmostEqual( 128.33, tools.real_LE_value_from_byte_array( cp_data ), 5 )

        # f[2] valuesf 128.33
        _, cp_data = component.constant_pool_data[ 29 ]
        self.assertEqual( 8, len( cp_data ) )
        self.assertAlmostEqual( 128.33, tools.real_LE_value_from_byte_array( cp_data[0:4] ), 5 )
        self.assertAlmostEqual( 0, tools.real_LE_value_from_byte_array( cp_data[4:8] ), 5 )


        # ps "sample string"
        _, cp_data = component.constant_pool_data[ 30 ]
        self.assertEqual( 2 + len( "sample string\"" ), len( cp_data ) )
        self.assertEqual( len( "sample string\"" ), tools.unsigned_LE_value_from_byte_array( cp_data[0:2] ) )
        self.assertEqual( "sample string\"", tools.ascii_string_from_byte_array( cp_data[2:] ) )

        # ps named "sample string with an identifier"
        _, cp_data = component.constant_pool_data[ 31 ]
        self.assertEqual( 2 + len( "sample string with an identifier" ), len( cp_data ) )
        self.assertEqual( len( "sample string with an identifier" ), tools.unsigned_LE_value_from_byte_array( cp_data[0:2] ) )
        self.assertEqual( "sample string with an identifier", tools.ascii_string_from_byte_array( cp_data[2:] ) )

        # cs "null terminated string"
        _, cp_data = component.constant_pool_data[ 32 ]
        self.assertEqual( 1 + len( "null terminated string" ), len( cp_data ) )
        self.assertEqual( 0, cp_data[-1:][0] )
        self.assertEqual( "null terminated string", tools.ascii_string_from_byte_array( cp_data[0:-1] ) )

        # cs "double 'quoted' string"
        _, cp_data = component.constant_pool_data[ 33 ]
        self.assertEqual( "\"quoted' string", tools.ascii_string_from_byte_array( cp_data[0:-1] ) )

        # cs "double 'quoted' string"
        _, cp_data = component.constant_pool_data[ 34 ]
        self.assertEqual( "escape sequence chars \a\v\b\f\t\n\r string", tools.ascii_string_from_byte_array( cp_data[0:-1] ) )

        #     cs '''
        # multi-line
        #     string
        #         literal
        # '''
        _, cp_data = component.constant_pool_data[ 35 ]
        # notice how there's NO starting and ending newline in expected_value
        expected_value = """multi-line
    string
        literal"""
        self.assertEqual( expected_value, tools.ascii_string_from_byte_array( cp_data[0:-1] ) )


    def test_multiline_string_errors(self):
        source = BPAsmSource([
            "name: constant_pool_sample",
            "inputs: 1",
            "outputs: 1",
            "stack: 1",
            "packets: 1",
            "data:",
            "constant_pool:",
            "    cs '''",
            "multi-line",
            "    string",
            "        literal",
            # "'''",
            "",
            "setup:",
            "    end",
            "loop:",
            "    end",
        ])

        with self.assertRaises(SyntaxError):
            parse_component( source )


    def tearDown(self):
        pass

