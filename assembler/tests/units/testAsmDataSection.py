## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

from . import tools

class TestDataSection(unittest.TestCase):

    def setUp(self):
        pass


    def test_jump(self):
        source = BPAsmSource([
            "name: constant_pool_sample",
            "inputs: 1",
            "outputs: 1",
            "stack: 1",
            "packets: 1",
            "data:",
            "    i8 value8",
            "    i8[3] array8",
            "",
            "    i16 value16",
            "    i16[3] array16",
            "",
            "    i32 value32",
            "    i32[3] array32",
            "",
            "    i64 value64",
            "    i64[3] array64",
            "",
            "    f valuef",
            "    f[3] arrayf",
            "",
            "    d valued",
            "    d[3] arrayd",
            "",
            "    u8 sentinel",
            "",
            "constant_pool:",
            "setup:",
            "    end",
            "loop:",
            "    end",
        ])
        component = parse_component(source)
        data_section = component.data


        # i8 value8
        data_f = data_section.get_field( 'value8' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 1, data_f.element_size )
        self.assertEqual( 1, data_f.element_count )
        self.assertEqual( 0, data_f.offset )

        # i8[3] array8
        data_f = data_section.get_field( 'array8' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 1, data_f.element_size )
        self.assertEqual( 3, data_f.element_count )
        self.assertEqual( 1, data_f.offset )


        # i16 value16
        data_f = data_section.get_field( 'value16' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 2, data_f.element_size )
        self.assertEqual( 1, data_f.element_count )
        self.assertEqual( 4, data_f.offset )

        # i16[3] array16
        data_f = data_section.get_field( 'array16' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 2, data_f.element_size )
        self.assertEqual( 3, data_f.element_count )
        self.assertEqual( 6, data_f.offset )


        # i32 value32
        data_f = data_section.get_field( 'value32' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 4, data_f.element_size )
        self.assertEqual( 1, data_f.element_count )
        self.assertEqual( 12, data_f.offset )

        # i32[3] array32
        data_f = data_section.get_field( 'array32' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 4, data_f.element_size )
        self.assertEqual( 3, data_f.element_count )
        self.assertEqual( 16, data_f.offset )


        # i64 value64
        data_f = data_section.get_field( 'value64' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 8, data_f.element_size )
        self.assertEqual( 1, data_f.element_count )
        self.assertEqual( 28, data_f.offset )

        # i64[3] array64
        data_f = data_section.get_field( 'array64' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 8, data_f.element_size )
        self.assertEqual( 3, data_f.element_count )
        self.assertEqual( 36, data_f.offset )


        # f valuef
        data_f = data_section.get_field( 'valuef' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 4, data_f.element_size )
        self.assertEqual( 1, data_f.element_count )
        self.assertEqual( 60, data_f.offset )

        # f[3] arrayf
        data_f = data_section.get_field( 'arrayf' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 4, data_f.element_size )
        self.assertEqual( 3, data_f.element_count )
        self.assertEqual( 64, data_f.offset )


        # d valued
        data_f = data_section.get_field( 'valued' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 8, data_f.element_size )
        self.assertEqual( 1, data_f.element_count )
        self.assertEqual( 76, data_f.offset )

        # d[3] arrayd
        data_f = data_section.get_field( 'arrayd' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 8, data_f.element_size )
        self.assertEqual( 3, data_f.element_count )
        self.assertEqual( 84, data_f.offset )

        # u8 sentinel
        data_f = data_section.get_field( 'sentinel' )
        self.assertIsNotNone( data_f )
        self.assertEqual( 108, data_f.offset )


    def tearDown(self):
        pass

