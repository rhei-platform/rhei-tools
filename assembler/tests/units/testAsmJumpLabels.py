## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

from . import tools

class TestJumpLabels(unittest.TestCase):

    def setUp(self):
        pass


    def test_jump(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump _label_1_0",
            "    push 24",
            "    _label_2_0:",
            "    push 42",
            "    end",
            "    _label_1_0:",
            "    jump _label_2_0",
            "    end",
        ])
        component = parse_component(source)

        # jump _label_1_0
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 5, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump _label_2_0
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_false(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_false _label_1_8",
            "    push 24",
            "    jump _label_3_8",
            "    _label_2_8:",
            "    push 42",
            "    _label_3_8:",
            "    end",
            "    _label_1_8:",
            "    jump_if_false _label_2_8",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_false _label_1_8
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_false _label_2_8
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_less_equal(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_less_equal _label_1_1",
            "    push 24",
            "    jump _label_3_1",
            "    _label_2_1:",
            "    push 42",
            "    _label_3_1:",
            "    end",
            "    _label_1_1:",
            "    jump_if_less_equal _label_2_1",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_less_equal _label_1_1
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_less_equal _label_2_1
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_greater_equal(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_greater_equal _label_1_2",
            "    push 24",
            "    jump _label_3_2",
            "    _label_2_2:",
            "    push 42",
            "    _label_3_2:",
            "    end",
            "    _label_1_2:",
            "    jump_if_greater_equal _label_2_2",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_greater_equal _label_1_2
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_greater_equal _label_2_2
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_less(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_less _label_1_3",
            "    push 24",
            "    jump _label_3_3",
            "    _label_2_3:",
            "    push 42",
            "    _label_3_3:",
            "    end",
            "    _label_1_3:",
            "    jump_if_less _label_2_3",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_less _label_1_3
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_less _label_2_3
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_greater(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_greater _label_1_4",
            "    push 24",
            "    jump _label_3_4",
            "    _label_2_4:",
            "    push 42",
            "    _label_3_4:",
            "    end",
            "    _label_1_4:",
            "    jump_if_greater _label_2_4",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_greater _label_1_4
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_greater _label_2_4
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_equal(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_equal _label_1_5",
            "    push 24",
            "    jump _label_3_5",
            "    _label_2_5:",
            "    push 42",
            "    _label_3_5:",
            "    end",
            "    _label_1_5:",
            "    jump_if_equal _label_2_5",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_equal _label_1_5
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_equal _label_2_5
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_not_equal(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_not_equal _label_1_6",
            "    push 24",
            "    jump _label_3_6",
            "    _label_2_6:",
            "    push 42",
            "    _label_3_6:",
            "    end",
            "    _label_1_6:",
            "    jump_if_not_equal _label_2_6",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_not_equal _label_1_6
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_not_equal _label_2_6
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_jump_if_zero_2(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_zero_2 _label_1_7",
            "    push 24",
            "    jump _label_3_7",
            "    _label_2_7:",
            "    push 42",
            "    _label_3_7:",
            "    end",
            "    _label_1_7:",
            "    jump_if_zero_2 _label_2_7",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_zero_2 _label_1_7
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_zero_2 _label_2_7
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def test_label_indent(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "    jump_if_zero_2 _label_1_7",
            "    push 24",
            "    jump _label_3_7",
            " _label_2_7:",
            "    push 42",
            "  _label_3_7:",
            "    end",
            "      _label_1_7:",
            "    jump_if_zero_2 _label_2_7",
            "    end",
        ])
        component = parse_component(source)

        # jump_if_zero_2 _label_1_7
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # jump_if_zero_2 _label_2_7
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( -6, tools.signed_BE_value_from_byte_array( op[1:] ) )


    def tearDown(self):
        pass

