## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile


class TestMetaData(unittest.TestCase):

    def setUp(self):
        pass

    def test_allops(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 fieldA",
            "",
            "record: AnotherRecord",
            "    i32 fieldB",
            "name: test_name",
            "doc:",
            "   doc line 1",
            "   doc line 2",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   end",
        ])
        component = parse_component(source)

        self.assertEqual( "test_name", component.name )
        self.assertEqual( 2, len( component.doc ) )
        self.assertEqual( "   doc line 1", component.doc[0] )
        self.assertEqual( "   doc line 2", component.doc[1] )
        self.assertEqual( '2', component.inputs )
        self.assertEqual( '1', component.outputs )
        self.assertEqual( '16', component.stack )
        self.assertEqual( '4', component.packets )

        self.assertEqual( 4, len( component.records ) )
        self.assertIn( 'SampleRecord', component.records )
        self.assertIn( 'AnotherRecord', component.records )
        self.assertIn( '$DATA', component.records )
        self.assertIn( '$CONST', component.records )

    def tearDown(self):
        pass

