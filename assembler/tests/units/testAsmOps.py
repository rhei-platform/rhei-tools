## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

from . import tools

class TestOps(unittest.TestCase):

    def setUp(self):
        pass


    def test_misc(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   nop",
            "   invalid",
            "   end",
            "   terminate",
            "   yield",
        ])
        component = parse_component(source)

        # NOP
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_NOP', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # INVALID
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_INVALID', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # END
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_END', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # TERMINATE
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_TERMINATE', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # YIELD
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_YIELD', op[ 0 ] )
        self.assertEqual( 1, len( op ) )


    def test_switch_jump(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   switch 42",
            "   jump 42",
            "   jump_if_false 42",
            "   jump_if_less_equal 42",
            "   jump_if_greater_equal 42",
            "   jump_if_less 42",
            "   jump_if_greater 42",
            "   jump_if_equal 42",
            "   jump_if_not_equal 42",
            "   jump_if_zero_2 42",
            "   switch 321",
            "   jump 321",
            "   jump_if_false 321",
            "   jump_if_less_equal 321",
            "   jump_if_greater_equal 321",
            "   jump_if_less 321",
            "   jump_if_greater 321",
            "   jump_if_equal 321",
            "   jump_if_not_equal 321",
            "   jump_if_zero_2 321",
        ])
        component = parse_component(source)

        #
        # BYTE size offset
        #
        # SWITCH
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_SWITCH', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_JUMP', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_FALSE
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_JUMP_IF_FALSE', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_LESS_EQUAL
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_JUMP_IF_LESS_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_GREATER_EQUAL
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_JUMP_IF_GREATER_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_LESS
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_JUMP_IF_LESS', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_GREATER
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_JUMP_IF_GREATER', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_EQUAL
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_JUMP_IF_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_NOT_EQUAL
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 'OP_JUMP_IF_NOT_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_ZERO_2
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 'OP_JUMP_IF_ZERO_2', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 42, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        #
        # SHORT size offset
        #
        # SWITCH
        _, op, _ = component.loop_code[ 10 ]
        self.assertEqual( 'OP_SWITCH', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP
        _, op, _ = component.loop_code[ 11 ]
        self.assertEqual( 'OP_JUMP', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_FALSE
        _, op, _ = component.loop_code[ 12 ]
        self.assertEqual( 'OP_JUMP_IF_FALSE', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_LESS_EQUAL
        _, op, _ = component.loop_code[ 13 ]
        self.assertEqual( 'OP_JUMP_IF_LESS_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_GREATER_EQUAL
        _, op, _ = component.loop_code[ 14 ]
        self.assertEqual( 'OP_JUMP_IF_GREATER_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_LESS
        _, op, _ = component.loop_code[ 15 ]
        self.assertEqual( 'OP_JUMP_IF_LESS', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_GREATER
        _, op, _ = component.loop_code[ 16 ]
        self.assertEqual( 'OP_JUMP_IF_GREATER', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_EQUAL
        _, op, _ = component.loop_code[ 17 ]
        self.assertEqual( 'OP_JUMP_IF_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_NOT_EQUAL
        _, op, _ = component.loop_code[ 18 ]
        self.assertEqual( 'OP_JUMP_IF_NOT_EQUAL', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # JUMP_IF_ZERO_2
        _, op, _ = component.loop_code[ 19 ]
        self.assertEqual( 'OP_JUMP_IF_ZERO_2', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 321, tools.unsigned_BE_value_from_byte_array( op[1:] ) )


    def test_const_i(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   const_i8",
            "   const_i16",
            "   const_i32",
            "   const_i64",
            "   const_i8 44",
            "   const_i16 33",
            "   const_i32 22",
            "   const_i64 11",
            "   const_i8 344",
            "   const_i16 333",
            "   const_i32 322",
            "   const_i64 311",
        ])
        component = parse_component(source)

        # CONST_I8
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_PCONST_I8', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CONST_I16
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_PCONST_I16', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CONST_I32
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_PCONST_I32', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CONST_I64
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_PCONST_I64', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CONST_I8
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_BCONST_I8', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 44, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I16
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_BCONST_I16', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 33, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I32
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_BCONST_I32', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 22, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I64
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_BCONST_I64', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 11, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I8
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 'OP_SCONST_I8', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 344, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I16
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 'OP_SCONST_I16', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 333, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I32
        _, op, _ = component.loop_code[ 10 ]
        self.assertEqual( 'OP_SCONST_I32', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 322, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # CONST_I64
        _, op, _ = component.loop_code[ 11 ]
        self.assertEqual( 'OP_SCONST_I64', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 311, tools.unsigned_BE_value_from_byte_array( op[1:] ) )


    def test_const(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   const_0",
            "   const_1",
            "   const_m1",
        ])
        component = parse_component(source)

        # CONST_0
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_CONST_0', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CONST_1
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_CONST_1', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CONST_M1
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_CONST_M1', op[ 0 ] )
        self.assertEqual( 1, len( op ) )


    def test_push_dup(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   push 55",
            "   push 355",
            "   dup",
            "   dup_2",
        ])
        component = parse_component(source)

        # BPUSH
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_BPUSH', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 55, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # SPUSH
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_SPUSH', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 355, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # DUP
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_DUP', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # DUP_2
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_DUP_2', op[ 0 ] )
        self.assertEqual( 1, len( op ) )


    def test_load(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   cload_i8",
            "   cload_i16",
            "   cload_i32",
            "   cload_i64",
            "   load_i8",
            "   load_i16",
            "   load_i32",
            "   load_i64",
            "   load_i8 22",
            "   load_i16 33",
            "   load_i32 44",
            "   load_i64 55",
        ])
        component = parse_component(source)

        # CLOAD_I8
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_CLOAD_I8', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CLOAD_I16
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_CLOAD_I16', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CLOAD_I32
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_CLOAD_I32', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CLOAD_I64
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_CLOAD_I64', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # LOAD_I8
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_LOAD_I8', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # LOAD_I16
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_LOAD_I16', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # LOAD_I32
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_LOAD_I32', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # LOAD_I64
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_LOAD_I64', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BLOAD_I8
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 'OP_BLOAD_I8', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 22, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # BLOAD_I16
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 'OP_BLOAD_I16', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 33, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # BLOAD_I32
        _, op, _ = component.loop_code[ 10 ]
        self.assertEqual( 'OP_BLOAD_I32', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 44, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # BLOAD_I64
        _, op, _ = component.loop_code[ 11 ]
        self.assertEqual( 'OP_BLOAD_I64', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 55, tools.unsigned_BE_value_from_byte_array( op[1:] ) )


    def test_store(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   cstore_i8",
            "   cstore_i16",
            "   cstore_i32",
            "   cstore_i64",
            "   store_i8",
            "   store_i16",
            "   store_i32",
            "   store_i64",
            "   store_i8 22",
            "   store_i16 33",
            "   store_i32 44",
            "   store_i64 55",
        ])
        component = parse_component(source)

        # CSTORE_I8
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_CSTORE_I8', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CSTORE_I16
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_CSTORE_I16', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CSTORE_I32
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_CSTORE_I32', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CSTORE_I64
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_CSTORE_I64', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # STORE_I8
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_STORE_I8', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # STORE_I16
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_STORE_I16', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # STORE_I32
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_STORE_I32', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # STORE_I64
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_STORE_I64', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BSTORE_I8
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 'OP_BSTORE_I8', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 22, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # BSTORE_I16
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 'OP_BSTORE_I16', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 33, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # BSTORE_I32
        _, op, _ = component.loop_code[ 10 ]
        self.assertEqual( 'OP_BSTORE_I32', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 44, tools.unsigned_BE_value_from_byte_array( op[1:] ) )

        # BSTORE_I64
        _, op, _ = component.loop_code[ 11 ]
        self.assertEqual( 'OP_BSTORE_I64', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 55, tools.unsigned_BE_value_from_byte_array( op[1:] ) )


    def test_math(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   neg_i",
            "   neg_l",
            "   neg_f",
            "   neg_d",
            "   not",
            "   bit_invert_i",
            "   bit_invert_l",
            "   mul_i",
            "   mul_l",
            "   mul_f",
            "   mul_d",
            "   div_i",
            "   div_l",
            "   div_f",
            "   div_d",
            "   mod_i",
            "   mod_l",
            "   add_i",
            "   add_l",
            "   add_f",
            "   add_d",
            "   sub_i",
            "   sub_l",
            "   sub_f",
            "   sub_d",
        ])
        component = parse_component(source)

        # NEG_I
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_NEG_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # NEG_L
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_NEG_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # NEG_F
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_NEG_F', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # NEG_D
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_NEG_D', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # NOT
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_NOT', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_INVERT_I
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_BIT_INVERT_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_INVERT_L
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_BIT_INVERT_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # MUL_I
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_MUL_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # MUL_L
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 'OP_MUL_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # MUL_F
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 'OP_MUL_F', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # MUL_D
        _, op, _ = component.loop_code[ 10 ]
        self.assertEqual( 'OP_MUL_D', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # DIV_I
        _, op, _ = component.loop_code[ 11 ]
        self.assertEqual( 'OP_DIV_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # DIV_L
        _, op, _ = component.loop_code[ 12 ]
        self.assertEqual( 'OP_DIV_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # DIV_F
        _, op, _ = component.loop_code[ 13 ]
        self.assertEqual( 'OP_DIV_F', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # DIV_D
        _, op, _ = component.loop_code[ 14 ]
        self.assertEqual( 'OP_DIV_D', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # MOD_I
        _, op, _ = component.loop_code[ 15 ]
        self.assertEqual( 'OP_MOD_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # MOD_L
        _, op, _ = component.loop_code[ 16 ]
        self.assertEqual( 'OP_MOD_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # ADD_I
        _, op, _ = component.loop_code[ 17 ]
        self.assertEqual( 'OP_ADD_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # ADD_L
        _, op, _ = component.loop_code[ 18 ]
        self.assertEqual( 'OP_ADD_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # ADD_F
        _, op, _ = component.loop_code[ 19 ]
        self.assertEqual( 'OP_ADD_F', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # ADD_D
        _, op, _ = component.loop_code[ 20 ]
        self.assertEqual( 'OP_ADD_D', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SUB_I
        _, op, _ = component.loop_code[ 21 ]
        self.assertEqual( 'OP_SUB_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SUB_L
        _, op, _ = component.loop_code[ 22 ]
        self.assertEqual( 'OP_SUB_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SUB_F
        _, op, _ = component.loop_code[ 23 ]
        self.assertEqual( 'OP_SUB_F', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SUB_D
        _, op, _ = component.loop_code[ 24 ]
        self.assertEqual( 'OP_SUB_D', op[ 0 ] )
        self.assertEqual( 1, len( op ) )


    def test_bit_logic(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   shift_left_i",
            "   shift_left_l",
            "   shift_right_i",
            "   shift_right_l",
            "   bit_and_i",
            "   bit_and_l",
            "   bit_xor_i",
            "   bit_xor_l",
            "   bit_or_i",
            "   bit_or_l",
        ])
        component = parse_component(source)

        # SHIFT_LEFT_I
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_SHIFT_LEFT_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SHIFT_LEFT_L
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_SHIFT_LEFT_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SHIFT_RIGHT_I
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_SHIFT_RIGHT_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # SHIFT_RIGHT_L
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_SHIFT_RIGHT_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_AND_I
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_BIT_AND_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_AND_L
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_BIT_AND_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_XOR_I
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_BIT_XOR_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_XOR_L
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_BIT_XOR_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_OR_I
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 'OP_BIT_OR_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # BIT_OR_L
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 'OP_BIT_OR_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )


    def test_logic(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   cmp_i",
            "   cmp_ui",
            "   cmp_l",
            "   cmp_ul",
            "   cmp_f",
            "   cmp_d",
            "   and",
            "   or",
        ])
        component = parse_component(source)

        # CMP_I
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_CMP_I', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CMP_UI
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_CMP_UI', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CMP_L
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_CMP_L', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CMP_UL
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_CMP_UL', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CMP_F
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_CMP_F', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # CMP_D
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_CMP_D', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # AND
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_AND', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # OR
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_OR', op[ 0 ] )
        self.assertEqual( 1, len( op ) )


    def test_rw(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   read",
            "   read 2, 3",
            "   write",
            "   write 4, 5",
            "   create 44",
            "   create 55, 44",
            "   create 55, 277",
            "   drop 5",
        ])
        component = parse_component(source)

        # READ
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_REQUEST_READ', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # READ
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_BREQUEST_READ', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 2, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )
        self.assertEqual( 3, tools.unsigned_BE_value_from_byte_array( op[2:3] ) )

        # WRITE
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 'OP_REQUEST_WRITE', op[ 0 ] )
        self.assertEqual( 1, len( op ) )

        # WRITE
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 'OP_BREQUEST_WRITE', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 4, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )
        self.assertEqual( 5, tools.unsigned_BE_value_from_byte_array( op[2:3] ) )

        # CREATE
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 'OP_SCREATE_IP', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 44, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )

        # CREATE
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 'OP_CREATE_IP', op[ 0 ] )
        self.assertEqual( 4, len( op ) )
        self.assertEqual( 55, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )
        self.assertEqual( 44, tools.unsigned_BE_value_from_byte_array( op[2:4] ) )

        # CREATE
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 'OP_CREATE_IP', op[ 0 ] )
        self.assertEqual( 4, len( op ) )
        self.assertEqual( 55, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )
        self.assertEqual( 277, tools.unsigned_BE_value_from_byte_array( op[2:4] ) )

        # DROP
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 'OP_DROP_IP', op[ 0 ] )
        self.assertEqual( 2, len( op ) )


    def test_ip(self):
        source = BPAsmSource([
            "name: test",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "constant_pool:",
            "setup:",
            "   end",
            "loop:",
            "   length 2",
            "   copy 2, 1",
        ])
        component = parse_component(source)

        # LENGTH
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 'OP_IP_LENGTH', op[ 0 ] )
        self.assertEqual( 2, len( op ) )
        self.assertEqual( 2, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )

        # COPY
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 'OP_IP_COPY', op[ 0 ] )
        self.assertEqual( 3, len( op ) )
        self.assertEqual( 2, tools.unsigned_BE_value_from_byte_array( op[1:2] ) )
        self.assertEqual( 1, tools.unsigned_BE_value_from_byte_array( op[2:3] ) )


    def tearDown(self):
        pass

