## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpasmcomponent import parse_component
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

from . import tools

class TestRecordOffsets(unittest.TestCase):

    def setUp(self):
        pass


    def test_jump(self):
        source = BPAsmSource([
            "record: SampleRecord",
            "    u16 size",
            "    u8[256] name",
            "    ; default offset, # of pairs in the table",
            "    i32[2] switch_table ;243, 4",
            "    ; key-value pairs",
            "    i32[2] ;2, 234",
            "    i32[2] ;3, 33",
            "    i32[2] ;4, 45",
            "    i32[2] ;5, 7",
            "    f[2] grades",
            "",
            "record: AnotherRecord",
            "    i32 length",
            "    f lat",
            "    f lon",
            "    u8[256] name",
            "    f[2] velocity",
            "",
            "name: offsets",
            "inputs: 2",
            "outputs: 1",
            "stack: 16",
            "packets: 4",
            "data:",
            "    i32 value",
            "    i32 second",
            "constant_pool:",
            "    u8 1",
            "    i32 that_one -42",
            "    f float_value 34.0",
            "    ps \"character string literal\"",
            "",
            "setup:",
            "    end",
            "",
            "loop:",
            "    switch &SampleRecord.switch_table",
            "",
            "    const_i8 &SampleRecord.size",
            "    const_i16 &SampleRecord.grades",
            "    const_i32 &SampleRecord.grades",
            "    const_i64 &AnotherRecord.lon",
            "    const_i64 &$CONST.that_one",
            "",
            "    push &SampleRecord.size",
            "    push &SampleRecord.grades",
            "    push &AnotherRecord.lon",
            "    push &$DATA.second",
            "    push &$CONST.float_value",
            "",
            "    load_i8 &SampleRecord.size",
            "    load_i16 &AnotherRecord.lon",
            "    load_i32 &AnotherRecord.lat",
            "    load_i64 &AnotherRecord.length",
            "",
            "    store_i8 &SampleRecord.size",
            "    store_i16 &AnotherRecord.lon",
            "    store_i32 &AnotherRecord.lat",
            "    store_i64 &AnotherRecord.length",
            "",
            "    create 55, &SampleRecord",
            "    create 55, &AnotherRecord",
            "",
        ])
        component = parse_component(source)

        # switch &SampleRecord.switch_table
        _, op, _ = component.loop_code[ 0 ]
        self.assertEqual( 258, tools.signed_BE_value_from_byte_array( op[1:] ) )


        # const_i8 &SampleRecord.size
        _, op, _ = component.loop_code[ 1 ]
        self.assertEqual( 0, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # const_i16 &SampleRecord.grades
        _, op, _ = component.loop_code[ 2 ]
        self.assertEqual( 298, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # const_i32 &SampleRecord.grades
        _, op, _ = component.loop_code[ 3 ]
        self.assertEqual( 298, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # const_i64 &AnotherRecord.lon
        _, op, _ = component.loop_code[ 4 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # const_i64 &$CONST.that_one
        _, op, _ = component.loop_code[ 5 ]
        self.assertEqual( 1, tools.signed_BE_value_from_byte_array( op[1:] ) )


        # push &SampleRecord.size
        _, op, _ = component.loop_code[ 6 ]
        self.assertEqual( 0, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # push &SampleRecord.grades
        _, op, _ = component.loop_code[ 7 ]
        self.assertEqual( 298, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # push &AnotherRecord.lon
        _, op, _ = component.loop_code[ 8 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # push &$DATA.second
        _, op, _ = component.loop_code[ 9 ]
        self.assertEqual( 4, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # push &$CONST.float_value
        _, op, _ = component.loop_code[ 10 ]
        self.assertEqual( 5, tools.signed_BE_value_from_byte_array( op[1:] ) )


        # load_i8 &SampleRecord.size
        _, op, _ = component.loop_code[ 11 ]
        self.assertEqual( 0, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # load_i16 &AnotherRecord.lon
        _, op, _ = component.loop_code[ 12 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # load_i32 &AnotherRecord.lat
        _, op, _ = component.loop_code[ 13 ]
        self.assertEqual( 4, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # load_i64 &AnotherRecord.length
        _, op, _ = component.loop_code[ 14 ]
        self.assertEqual( 0, tools.signed_BE_value_from_byte_array( op[1:] ) )


        # store_i8 &SampleRecord.size
        _, op, _ = component.loop_code[ 15 ]
        self.assertEqual( 0, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # store_i16 &AnotherRecord.lon
        _, op, _ = component.loop_code[ 16 ]
        self.assertEqual( 8, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # store_i32 &AnotherRecord.lat
        _, op, _ = component.loop_code[ 17 ]
        self.assertEqual( 4, tools.signed_BE_value_from_byte_array( op[1:] ) )

        # store_i64 &AnotherRecord.length
        _, op, _ = component.loop_code[ 18 ]
        self.assertEqual( 0, tools.signed_BE_value_from_byte_array( op[1:] ) )


        # create &SampleRecord
        _, op, _ = component.loop_code[ 19 ]
        self.assertEqual( 306, tools.signed_BE_value_from_byte_array( op[2:] ) )

        # create &AnotherRecord
        _, op, _ = component.loop_code[ 20 ]
        self.assertEqual( 276, tools.signed_BE_value_from_byte_array( op[2:] ) )


    def tearDown(self):
        pass

