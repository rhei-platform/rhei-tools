## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpnetparsers import parse_network
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

class Smoke(unittest.TestCase):

    def setUp(self):
        pass

    def test_sample(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_NETWORKS_SOURCES, "sumneg.bpnet" ) )
        source = BPAsmSourceFile( source_file )

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 3, len( network.contexts ) )
        self.assertEqual( 3, len( network.inputs ) )
        self.assertEqual( 1, len( network.outputs ) )
        self.assertEqual( 2, len( network.connections ) )

    def test_no_lines(self):
        source = BPAsmSource([
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.contexts ) )
        self.assertEqual( 0, len( network.connections ) )
        self.assertEqual( 0, len( network.inputs ) )
        self.assertEqual( 0, len( network.outputs ) )
        self.assertEqual( 0, len( network.native_contexts ) )
        self.assertEqual( 0, len( network.native_inputs ) )
        self.assertEqual( 0, len( network.native_outputs ) )
        self.assertEqual( 0, len( network.buffer_definitions ) )

    def test_empty_lines(self):
        source = BPAsmSource([
            "",
            "",
            "",
            "",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.contexts ) )
        self.assertEqual( 0, len( network.connections ) )
        self.assertEqual( 0, len( network.inputs ) )
        self.assertEqual( 0, len( network.outputs ) )
        self.assertEqual( 0, len( network.native_contexts ) )
        self.assertEqual( 0, len( network.native_inputs ) )
        self.assertEqual( 0, len( network.native_outputs ) )
        self.assertEqual( 0, len( network.buffer_definitions ) )

    def tearDown(self):
        pass

