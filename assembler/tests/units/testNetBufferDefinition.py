## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpnetparsers import parse_network
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

class TestBufferDefinition(unittest.TestCase):

    def setUp(self):
        pass

    def test_sample(self):
        source_file = os.path.normpath( os.path.join( cfg.TEST_NETWORKS_SOURCES, "buffers.bpnet" ) )
        source = BPAsmSourceFile( source_file )

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 2, len( network.buffer_definitions ) )
        self.assertIn( 'buffer1', network.buffer_definitions )
        self.assertIn( 'buffer2', network.buffer_definitions )


    def test_extern(self):
        source = BPAsmSource([
            "{ buffer1 } <-- extern",
            "[ctx1] <-- some.package.component",
            "",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 1, len( network.buffer_definitions ) )
        self.assertIn( 'buffer1', network.buffer_definitions )
        buffer_definition = network.buffer_definitions['buffer1']
        self.assertEqual( 'buffer1', buffer_definition.name )
        self.assertTrue( buffer_definition.is_external )


        source = BPAsmSource([
            "{ buffer1 } <-- extern",
            "",
            "[ctx1] <-- some.package.component",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 1, len( network.buffer_definitions ) )
        self.assertIn( 'buffer1', network.buffer_definitions )
        buffer_definition = network.buffer_definitions['buffer1']
        self.assertEqual( 'buffer1', buffer_definition.name )
        self.assertTrue( buffer_definition.is_external )


    def test_extern_errors(self):

        source = BPAsmSource([
            "{ buffer1 } <-- extern",
            "EOB <-",
            "",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "{ buffer1 } <-- extern",
            "{ buffer1 } <-- extern",
            "",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


    def test_definitions(self):
        source = BPAsmSource([
            "{ buffer1 } <-",
            " u8 12",
            " u32 1246",
            "EOB <-",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 1, len( network.buffer_definitions ) )
        self.assertIn( 'buffer1', network.buffer_definitions )
        buffer_definition = network.buffer_definitions['buffer1']
        self.assertEqual( 'buffer1', buffer_definition.name )
        self.assertFalse( buffer_definition.is_external )
        self.assertEqual( 2, len( buffer_definition.definition_data ) )
        # len( u8 ) == 1
        self.assertEqual( 1, len( buffer_definition.definition_data[0].data ) )
        # len( u32 ) == 4
        self.assertEqual( 4, len( buffer_definition.definition_data[1].data ) )


        source = BPAsmSource([
            "{ buffer1 } <-",
            "EOB <-",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 1, len( network.buffer_definitions ) )
        self.assertIn( 'buffer1', network.buffer_definitions )
        buffer_definition = network.buffer_definitions['buffer1']
        self.assertFalse( buffer_definition.is_external )
        self.assertEqual( 0, len( buffer_definition.definition_data ) )


    def test_definition_errors(self):

        source = BPAsmSource([
            "{ buffer1 } <-",
            "u8 12",
            "EOB <-",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "{ buffer1 } <-",
            "[ctx1] <-- some.package.component",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "{ buffer1 } <-",
            "",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "{ buffer1 } <-",
            "EOB <-",
            "{ buffer1 } <-",
            "EOB <-",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


    def tearDown(self):
        pass

