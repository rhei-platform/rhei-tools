## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpnetparsers import parse_network
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile

class TestComments(unittest.TestCase):

    def setUp(self):
        pass

    def test_line_comments(self):
        source = BPAsmSource([
            "; comment",
            "",
            ";; another comment",
            "; ;",
            "# different comment",
            "",
            "## and so on",
            "# #",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.contexts ) )
        self.assertEqual( 0, len( network.connections ) )
        self.assertEqual( 0, len( network.inputs ) )
        self.assertEqual( 0, len( network.outputs ) )
        self.assertEqual( 0, len( network.native_contexts ) )
        self.assertEqual( 0, len( network.native_inputs ) )
        self.assertEqual( 0, len( network.native_outputs ) )

    def test_inline_comments(self):
        source = BPAsmSource([
            "{ buffer1 } <-- extern ; comment",
            "{ buffer2 } <-- extern # comment",
            "{ buffer3 } <-- ; comment",
            " u8 10 ; comment",
            " u8 2 # comment",
            "EOB <-- ; comment",
            "{ buffer4 } <-- # comment",
            "EOB <-- # comment",
            "[ sum1 ] <-- tests.math.adder  ; comment",
            "[ sum2 ] <-- tests.math.adder  # comment",
            "[ neg ]  <-- tests.math.negator #comment",
            "< inputFn > <-- tests.system.readline ; comment",
            "< outputFn > <-- tests.system.printline # comment",
            "",
            "",
            "A --> 0 : [sum1] ; comment",
            "B --> 1 : [sum1] # comment",
            "< inputFn > --> 0 : [sum1] ; comment",
            "< inputFn > --> 1 : [sum1] # comment",
            "[sum1] : 0 --> 1 : [sum2] ; comment",
            "C --> 0 : [sum2] ;comment",
            "< inputFn > --> 0 : [sum2] #comment",
            "[sum2] : 0 --> [neg] ; comment",
            "[neg] --> OUT ; comment",
            "[neg] --> <outputFn> # comment",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 3, len( network.contexts ) )
        self.assertEqual( 2, len( network.connections ) )
        self.assertEqual( 3, len( network.inputs ) )
        self.assertEqual( 1, len( network.outputs ) )
        self.assertEqual( 2, len( network.native_contexts ) )
        self.assertEqual( 3, len( network.native_inputs ) )
        self.assertEqual( 1, len( network.native_outputs ) )
        self.assertEqual( 4, len( network.buffer_definitions ) )

    def tearDown(self):
        pass
