## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpnetparsers import parse_network, InputPort, OutputPort
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile


class TestConnections(unittest.TestCase):

    def setUp(self):
        pass

    def test_connections(self):
        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "[ sum2 ] <-- tests.math.adder",
            "",
            "A -> 0 : [ sum1 ]",
            "B -> 1 : [ sum1 ]",
            "[ sum1 ] -> C",
            "D -> [ sum2 ]",
            "E -> [ sum2 ]",
            "[ sum2 ] -> F",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.connections ) )


        source = BPAsmSource([
            "[ ctx1 ] <-- tests.component1",
            "[ ctx2 ] <-- tests.component2",
            "[ ctx3 ] <-- tests.component3",
            "[ ctx4 ] <-- tests.component4",
            "",
            "[ ctx1 ] : 0 -> 0 : [ ctx2 ]",
            "[ ctx1 ] : 1 ----> 1 : [ ctx2 ]",
            "[ ctx1 ] : 2 -- 4 --> 2 : [ ctx2 ]",
            "[ ctx2 ] -> [ ctx3 ]",
            "[ ctx2 ] -----> [ ctx3 ]",
            "[ ctx2 ] --- 9 --> [ ctx3 ]",
            "[ ctx3 ]     -> 1 : [ ctx4 ]",
            "[ ctx3 ] : 1 ->     [ ctx4 ]",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 8, len( network.connections ) )

        connection = network.connections[0]
        self.assertEqual( "ctx1", connection.source_port.context_name )
        self.assertEqual( 0, connection.source_port.context_port )
        self.assertEqual( 1, connection.capacity )
        self.assertEqual( "ctx2", connection.sink_port.context_name )
        self.assertEqual( 0, connection.sink_port.context_port )

        connection = network.connections[1]
        self.assertEqual( "ctx1", connection.source_port.context_name )
        self.assertEqual( 1, connection.source_port.context_port )
        self.assertEqual( 1, connection.capacity )
        self.assertEqual( "ctx2", connection.sink_port.context_name )
        self.assertEqual( 1, connection.sink_port.context_port )

        connection = network.connections[2]
        self.assertEqual( "ctx1", connection.source_port.context_name )
        self.assertEqual( 2, connection.source_port.context_port )
        self.assertEqual( 4, connection.capacity )
        self.assertEqual( "ctx2", connection.sink_port.context_name )
        self.assertEqual( 2, connection.sink_port.context_port )

        connection = network.connections[3]
        self.assertEqual( "ctx2", connection.source_port.context_name )
        self.assertEqual( 0, connection.source_port.context_port )
        self.assertEqual( 1, connection.capacity )
        self.assertEqual( "ctx3", connection.sink_port.context_name )
        self.assertEqual( 0, connection.sink_port.context_port )

        connection = network.connections[4]
        self.assertEqual( "ctx2", connection.source_port.context_name )
        self.assertEqual( 0, connection.source_port.context_port )
        self.assertEqual( 1, connection.capacity )
        self.assertEqual( "ctx3", connection.sink_port.context_name )
        self.assertEqual( 0, connection.sink_port.context_port )

        connection = network.connections[5]
        self.assertEqual( "ctx2", connection.source_port.context_name )
        self.assertEqual( 0, connection.source_port.context_port )
        self.assertEqual( 9, connection.capacity )
        self.assertEqual( "ctx3", connection.sink_port.context_name )
        self.assertEqual( 0, connection.sink_port.context_port )

        connection = network.connections[6]
        self.assertEqual( "ctx3", connection.source_port.context_name )
        self.assertEqual( 0, connection.source_port.context_port )
        self.assertEqual( 1, connection.capacity )
        self.assertEqual( "ctx4", connection.sink_port.context_name )
        self.assertEqual( 1, connection.sink_port.context_port )

        connection = network.connections[7]
        self.assertEqual( "ctx3", connection.source_port.context_name )
        self.assertEqual( 1, connection.source_port.context_port )
        self.assertEqual( 1, connection.capacity )
        self.assertEqual( "ctx4", connection.sink_port.context_name )
        self.assertEqual( 0, connection.sink_port.context_port )


    def test_n2n_connections(self):

        source = BPAsmSource([
            "< nativeFnA > <-- tests.sys.in",
            "< nativeFnB > <-- tests.sys.out",
            "{ buffer1 } <-- extern",
            "{ buffer2 } <-- extern",
            "",
            "< nativeFnA, buffer1 > --> < nativeFnB, buffer2 >",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.connections ) )
        self.assertEqual( 1, len( network.n2n_connections ) )

        n2n_connection = network.n2n_connections[0]
        self.assertEqual( "nativeFnA", n2n_connection.source_native_context )
        self.assertEqual( "buffer1", n2n_connection.source_buffer_name() )
        self.assertEqual( 1, n2n_connection.capacity )
        self.assertEqual( "nativeFnB", n2n_connection.sink_native_context )
        self.assertEqual( "buffer2", n2n_connection.sink_buffer_name() )


        source = BPAsmSource([
            "< nativeFnA > <-- tests.sys.in",
            "< nativeFnB > <-- tests.sys.out",
            "",
            "< nativeFnA > --> < nativeFnB >",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.connections ) )
        self.assertEqual( 1, len( network.n2n_connections ) )

        n2n_connection = network.n2n_connections[0]
        self.assertEqual( "nativeFnA", n2n_connection.source_native_context )
        self.assertEqual( "NULL", n2n_connection.source_buffer_name() )
        self.assertEqual( 1, n2n_connection.capacity )
        self.assertEqual( "nativeFnB", n2n_connection.sink_native_context )
        self.assertEqual( "NULL", n2n_connection.sink_buffer_name() )


        source = BPAsmSource([
            "< nativeFnA > <-- tests.sys.in",
            "< nativeFnB > <-- tests.sys.out",
            "",
            "< nativeFnA > -- 4 --> < nativeFnB >",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 0, len( network.connections ) )
        self.assertEqual( 1, len( network.n2n_connections ) )

        n2n_connection = network.n2n_connections[0]
        self.assertEqual( "nativeFnA", n2n_connection.source_native_context )
        self.assertEqual( "NULL", n2n_connection.source_buffer_name() )
        self.assertEqual( 4, n2n_connection.capacity )
        self.assertEqual( "nativeFnB", n2n_connection.sink_native_context )
        self.assertEqual( "NULL", n2n_connection.sink_buffer_name() )


    def test_connection_errors(self):
        source = BPAsmSource([
            "[ ctx1 ] <-- tests.component1",
            "[ ctx2 ] <-- tests.component2",
            "",
            "[ ctx1 ] -- 0 --> [ ctx2 ]",
        ])

        with self.assertRaises(ValueError):
            parse_network( source )


        source = BPAsmSource([
            "[ ctx1 ] <-- tests.component1",
            "[ ctx2 ] <-- tests.component2",
            "",
            "[ ctx1 ] -- -5 --> [ ctx2 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ ctx1 ] <-- tests.component1",
            "",
            "[ ctx1 ] -> [ ctx2 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ ctx2 ] <-- tests.component1",
            "",
            "[ ctx1 ] -> [ ctx2 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


    def tearDown(self):
        pass
