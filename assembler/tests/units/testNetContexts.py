## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpnetparsers import parse_network
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile


class TestContexts(unittest.TestCase):

    def setUp(self):
        pass

    def test_contexts(self):
        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "[ sum2 ] <-- tests.math.adder",
            "[ neg ]  <-- tests.math.negator",
            "< inputFn > <-- tests.system.readline",
            "< outputFn > <-- tests.system.printline",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 3, len( network.contexts ) )
        self.assertEqual( 2, len( network.native_contexts ) )

        self.assertIn( "sum1", network.contexts )
        self.assertEqual( "sum1", network.contexts['sum1'].name )
        self.assertEqual( "tests/math/", network.contexts['sum1'].package )
        self.assertEqual( "adder.bpasm", network.contexts['sum1'].file_name )
        self.assertEqual( "tests/math/adder.bpasm", network.contexts['sum1'].file_location() )

        self.assertIn( "sum2", network.contexts )
        self.assertEqual( "sum2", network.contexts['sum2'].name )
        self.assertEqual( "tests/math/", network.contexts['sum2'].package )
        self.assertEqual( "adder.bpasm", network.contexts['sum2'].file_name )
        self.assertEqual( "tests/math/adder.bpasm", network.contexts['sum2'].file_location() )

        self.assertIn( "neg", network.contexts )
        self.assertEqual( "neg", network.contexts['neg'].name )
        self.assertEqual( "tests/math/", network.contexts['neg'].package )
        self.assertEqual( "negator.bpasm", network.contexts['neg'].file_name )
        self.assertEqual( "tests/math/negator.bpasm", network.contexts['neg'].file_location() )

        self.assertIn( "inputFn", network.native_contexts )
        self.assertEqual( "inputFn", network.native_contexts['inputFn'].name )
        self.assertEqual( "tests/system/", network.native_contexts['inputFn'].package )
        self.assertEqual( "readline.c", network.native_contexts['inputFn'].file_name )
        self.assertEqual( "tests/system/readline.c", network.native_contexts['inputFn'].file_location() )

        self.assertIn( "outputFn", network.native_contexts )
        self.assertEqual( "outputFn", network.native_contexts['outputFn'].name )
        self.assertEqual( "tests/system/", network.native_contexts['outputFn'].package )
        self.assertEqual( "printline.c", network.native_contexts['outputFn'].file_name )
        self.assertEqual( "tests/system/printline.c", network.native_contexts['outputFn'].file_location() )

    def test_context_errors(self):
        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "[ sum1 ] <-- tests.math.adder",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "[ sum1 ] <-- tests.math.other_adder",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "< inputFn > <-- tests.system.readline",
            "< inputFn > <-- tests.system.readline",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "< inputFn > <-- tests.system.readline",
            "< inputFn > <-- tests.system.other_readline",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )

    def tearDown(self):
        pass

