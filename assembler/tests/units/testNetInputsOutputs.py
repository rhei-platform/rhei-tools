## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: Apache-2.0

import config as cfg

import os
import unittest

from bpasmlib.bpnetparsers import parse_network, InputPort, OutputPort
from bpasmlib.bpasmsource import BPAsmSource, BPAsmSourceFile


class TestInputsOutputs(unittest.TestCase):

    def setUp(self):
        pass

    def test_inputs(self):
        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "[ sum2 ] <-- tests.math.adder",
            "< inputFn > <-- tests.system.readline",
            "",
            "A -> 0 : [ sum1 ]",
            "B ----> 1 : [ sum1 ]",
            "C -> [ sum2 ]",
            "< inputFn > -> 0 : [ sum1 ]",
            "< inputFn > ---> 1 : [ sum1 ]",
            "< inputFn > -> [ sum2 ]",
            "< inputFn > --- 4 --> 1 : [ sum2 ]",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 3, len( network.inputs ) )
        self.assertEqual( 4, len( network.native_inputs ) )


        self.assertIn( "A", network.inputs )
        self.assertEqual( "sum1", network.inputs['A'].context_name )
        self.assertEqual( 0, network.inputs['A'].context_port )
        self.assertTrue( network.inputs['A'].is_input() )
        self.assertFalse( network.inputs['A'].is_output() )

        self.assertIn( "B", network.inputs )
        self.assertEqual( "sum1", network.inputs['B'].context_name )
        self.assertEqual( 1, network.inputs['B'].context_port )
        self.assertTrue( network.inputs['B'].is_input() )
        self.assertFalse( network.inputs['B'].is_output() )

        self.assertIn( "C", network.inputs )
        self.assertEqual( "sum2", network.inputs['C'].context_name )
        self.assertEqual( 0, network.inputs['C'].context_port )
        self.assertTrue( network.inputs['C'].is_input() )
        self.assertFalse( network.inputs['C'].is_output() )


        native_input_key = network.native_io_key( "inputFn", InputPort( 'sum1', '0' ) )
        self.assertIn( native_input_key, network.native_inputs )
        self.assertEqual( "inputFn", network.native_inputs[ native_input_key ].native_context.name )
        self.assertEqual( 1, network.native_inputs[ native_input_key ].capacity )
        self.assertEqual( "sum1", network.native_inputs[ native_input_key ].port.context_name )
        self.assertEqual( 0, network.native_inputs[ native_input_key ].port.context_port )

        native_input_key = network.native_io_key( "inputFn", InputPort( 'sum1', '1' ) )
        self.assertIn( native_input_key, network.native_inputs )
        self.assertEqual( "inputFn", network.native_inputs[ native_input_key ].native_context.name )
        self.assertEqual( 1, network.native_inputs[ native_input_key ].capacity )
        self.assertEqual( "sum1", network.native_inputs[ native_input_key ].port.context_name )
        self.assertEqual( 1, network.native_inputs[ native_input_key ].port.context_port )

        native_input_key = network.native_io_key( "inputFn", InputPort( 'sum2', '0' ) )
        self.assertIn( native_input_key, network.native_inputs )
        self.assertEqual( "inputFn", network.native_inputs[ native_input_key ].native_context.name )
        self.assertEqual( 1, network.native_inputs[ native_input_key ].capacity )
        self.assertEqual( "sum2", network.native_inputs[ native_input_key ].port.context_name )
        self.assertEqual( 0, network.native_inputs[ native_input_key ].port.context_port )

        native_input_key = network.native_io_key( "inputFn", InputPort( 'sum2', '1' ) )
        self.assertIn( native_input_key, network.native_inputs )
        self.assertEqual( "inputFn", network.native_inputs[ native_input_key ].native_context.name )
        self.assertEqual( 4, network.native_inputs[ native_input_key ].capacity )
        self.assertEqual( "sum2", network.native_inputs[ native_input_key ].port.context_name )
        self.assertEqual( 1, network.native_inputs[ native_input_key ].port.context_port )

    def test_input_errors(self):
        source = BPAsmSource([
            "A -> 0 : [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "",
            "< inputFn > -> 0 : [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "< inputFn > <-- tests.system.readline",
            "",
            "< inputFn > -> 0 : [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "",
            "A -> 0 : [ sum1 ]",
            "A -> 0 : [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "",
            "A -> [ sum1 ]",
            "A -> [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "< inputFn > <-- tests.system.readline",
            "",
            "< inputFn > -> 0 : [ sum1 ]",
            "< inputFn > -> 0 : [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "< inputFn > <-- tests.system.readline",
            "",
            "< inputFn > -> [ sum1 ]",
            "< inputFn > -> [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "< inputFn > <-- tests.system.readline",
            "",
            "< inputFn > -- 0 --> [ sum1 ]",
        ])

        with self.assertRaises(ValueError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "< inputFn > <-- tests.system.readline",
            "",
            "< inputFn > -- -9 --> [ sum1 ]",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


    def test_outputs(self):
        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "[ sum2 ] <-- tests.math.adder",
            "< outputFn > <-- tests.system.printline",
            "",
            "[ sum1 ] : 0 -> A",
            "[ sum1 ] : 1 -----> B",
            "[ sum2 ] -> C",
            "[ sum1 ] : 0 -> < outputFn >",
            "[ sum1 ] : 1 -----> < outputFn >",
            "[ sum2 ] -> < outputFn >",
            "[ sum2 ] : 1 --- 4 --> < outputFn >",
        ])

        network = parse_network( source )

        self.assertIsNotNone( network )
        self.assertEqual( 3, len( network.outputs ) )
        self.assertEqual( 4, len( network.native_outputs ) )


        self.assertIn( "A", network.outputs )
        self.assertEqual( "sum1", network.outputs['A'].context_name )
        self.assertEqual( 0, network.outputs['A'].context_port )
        self.assertTrue( network.outputs['A'].is_output() )
        self.assertFalse( network.outputs['A'].is_input() )

        self.assertIn( "B", network.outputs )
        self.assertEqual( "sum1", network.outputs['B'].context_name )
        self.assertEqual( 1, network.outputs['B'].context_port )
        self.assertTrue( network.outputs['B'].is_output() )
        self.assertFalse( network.outputs['B'].is_input() )

        self.assertIn( "C", network.outputs )
        self.assertEqual( "sum2", network.outputs['C'].context_name )
        self.assertEqual( 0, network.outputs['C'].context_port )
        self.assertTrue( network.outputs['C'].is_output() )
        self.assertFalse( network.outputs['C'].is_input() )


        native_input_key = network.native_io_key( "outputFn", OutputPort( 'sum1', '0' ) )
        self.assertIn( native_input_key, network.native_outputs )
        self.assertEqual( "outputFn", network.native_outputs[ native_input_key ].native_context.name )
        self.assertEqual( 1, network.native_outputs[ native_input_key ].capacity )
        self.assertEqual( "sum1", network.native_outputs[ native_input_key ].port.context_name )
        self.assertEqual( 0, network.native_outputs[ native_input_key ].port.context_port )

        native_input_key = network.native_io_key( "outputFn", OutputPort( 'sum1', '1' ) )
        self.assertIn( native_input_key, network.native_outputs )
        self.assertEqual( "outputFn", network.native_outputs[ native_input_key ].native_context.name )
        self.assertEqual( 1, network.native_outputs[ native_input_key ].capacity )
        self.assertEqual( "sum1", network.native_outputs[ native_input_key ].port.context_name )
        self.assertEqual( 1, network.native_outputs[ native_input_key ].port.context_port )

        native_input_key = network.native_io_key( "outputFn", OutputPort( 'sum2', '0' ) )
        self.assertIn( native_input_key, network.native_outputs )
        self.assertEqual( "outputFn", network.native_outputs[ native_input_key ].native_context.name )
        self.assertEqual( 1, network.native_outputs[ native_input_key ].capacity )
        self.assertEqual( "sum2", network.native_outputs[ native_input_key ].port.context_name )
        self.assertEqual( 0, network.native_outputs[ native_input_key ].port.context_port )

        native_input_key = network.native_io_key( "outputFn", OutputPort( 'sum2', '1' ) )
        self.assertIn( native_input_key, network.native_outputs )
        self.assertEqual( "outputFn", network.native_outputs[ native_input_key ].native_context.name )
        self.assertEqual( 4, network.native_outputs[ native_input_key ].capacity )
        self.assertEqual( "sum2", network.native_outputs[ native_input_key ].port.context_name )
        self.assertEqual( 1, network.native_outputs[ native_input_key ].port.context_port )

    def test_output_errors(self):
        source = BPAsmSource([
            "[ sum1 ] : 0 -> A",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "",
            "[ sum1 ] : 0 -> < outputFn >",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "< outputFn > <-- tests.system.printline",
            "",
            "[ sum1 ] : 0 -> < outputFn >",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "",
            "[ sum1 ] : 0 -> A",
            "[ sum1 ] : 0 -> A",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "",
            "[ sum1 ] -> A",
            "[ sum1 ] -> A",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "< outputFn > <-- tests.system.printline",
            "",
            "[ sum1 ] : 0 -> < outputFn >",
            "[ sum1 ] : 0 -> < outputFn >",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


        source = BPAsmSource([
            "[ sum1 ] <-- tests.math.adder",
            "< outputFn > <-- tests.system.printline",
            "",
            "[ sum1 ] -> < outputFn >",
            "[ sum1 ] -> < outputFn >",
        ])

        with self.assertRaises(SyntaxError):
            parse_network( source )


    def tearDown(self):
        pass
