# Copyright 2019 Ravendyne Inc.
# SPDX-License-Identifier: Apache-2.0

import struct
import array

# https://docs.python.org/2/library/struct.html#format-characters

def unsigned_BE_value_from_byte_array( byte_list ):
    a = array.array( 'B', map( int, byte_list ) )
    b = bytearray( a )

    if len( byte_list ) == 1:
        return struct.unpack( '>B', b )[0]

    if len( byte_list ) == 2:
        return struct.unpack( '>H', b )[0]

    if len( byte_list ) == 4:
        return struct.unpack( '>I', b )[0]

    if len( byte_list ) == 8:
        return struct.unpack( '>Q', b )[0]

    raise ValueError( "Expected 8-, 16-, 32- or 64-bit unsigned value, got " + str( len( byte_list ) * 8 ) + "-bit one instead." )

def signed_BE_value_from_byte_array( byte_list ):
    a = array.array( 'B', map( int, byte_list ) )
    b = bytearray( a )

    if len( byte_list ) == 1:
        return struct.unpack( '>b', b )[0]

    if len( byte_list ) == 2:
        return struct.unpack( '>h', b )[0]

    if len( byte_list ) == 4:
        return struct.unpack( '>i', b )[0]

    if len( byte_list ) == 8:
        return struct.unpack( '>q', b )[0]

    raise ValueError( "Expected 8-, 16-, 32- or 64-bit signed value, got " + str( len( byte_list ) * 8 ) + "-bit one instead." )

def unsigned_LE_value_from_byte_array( byte_list ):
    a = array.array( 'B', map( int, byte_list ) )
    b = bytearray( a )

    if len( byte_list ) == 1:
        return struct.unpack( '<B', b )[0]

    if len( byte_list ) == 2:
        return struct.unpack( '<H', b )[0]

    if len( byte_list ) == 4:
        return struct.unpack( '<I', b )[0]

    if len( byte_list ) == 8:
        return struct.unpack( '<Q', b )[0]

    raise ValueError( "Expected 8-, 16-, 32- or 64-bit unsigned value, got " + str( len( byte_list ) * 8 ) + "-bit one instead." )

def signed_LE_value_from_byte_array( byte_list ):
    a = array.array( 'B', map( int, byte_list ) )
    b = bytearray( a )

    if len( byte_list ) == 1:
        return struct.unpack( '<b', b )[0]

    if len( byte_list ) == 2:
        return struct.unpack( '<h', b )[0]

    if len( byte_list ) == 4:
        return struct.unpack( '<i', b )[0]

    if len( byte_list ) == 8:
        return struct.unpack( '<q', b )[0]

    raise ValueError( "Expected 8-, 16-, 32- or 64-bit signed value, got " + str( len( byte_list ) * 8 ) + "-bit one instead." )

def real_LE_value_from_byte_array( byte_list ):
    a = array.array( 'B', map( int, byte_list ) )
    b = bytearray( a )

    if len( byte_list ) == 4:
        return struct.unpack( '<f', b )[0]

    if len( byte_list ) == 8:
        return struct.unpack( '<d', b )[0]

    raise ValueError( "Expected 8-, 16-, 32- or 64-bit signed value, got " + str( len( byte_list ) * 8 ) + "-bit one instead." )

def ascii_string_from_byte_array( byte_list ):
    a = array.array( 'B', map( int, byte_list ) )
    b = bytearray( a )

    return b.decode('ascii')
