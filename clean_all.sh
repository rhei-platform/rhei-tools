#!/bin/bash

rm -rf build
find . -name "*.o" -delete
find . -name "*.pyc" -delete
